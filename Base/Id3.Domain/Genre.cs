﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Genre.cs" company="Tripple S">
// 2011  
// </copyright>
// <summary>
//   Defines the Genre type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Id3.Domain
{
  /// <summary>
  /// Data class for Genre
  /// </summary>
  public class Genre
  {
    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Genre"/> class.
    /// </summary>
    /// <param name="id">
    /// Id for genre
    /// </param>
    /// <param name="text">
    /// Text for genre
    /// </param>
    public Genre(int id, string text)
    {
      this.Id = id;
      this.Text = text;
    }

    #endregion

    /// <summary>
    /// Unique numeric identifier in range 0-255
    /// </summary>
    public int Id { get; private set; }

    /// <summary>
    /// Description for Genre
    /// </summary>
    public string Text { get; private set; }
  }
}
