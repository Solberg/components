﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Id3.Domain
{
    public class Id3BaseTag
    {

        /// <summary>
        /// Gets Album.
        /// </summary>
        public string Album { get; set; }

        /// <summary>
        /// Gets Artist.
        /// </summary>
        public string Artist { get; set; }

        /// <summary>
        /// Gets Comment.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets FileName.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets FullPath.
        /// </summary>
        public string FullPath { get; set; }

        /// <summary>
        /// Gets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets TrackNo.
        /// </summary>
        public byte TrackNo { get; set; }

        /// <summary>
        /// Gets Year.
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Gets or sets Genre.
        /// </summary>
        public byte Genre { get; set; }

        public Id3BaseTag()
        {
        }

    public Id3BaseTag(
      byte trackNo, 
      string artist, 
      string album, 
      string title, 
      string year, 
      string comment, 
      string filename, 
      string fullPath,
      byte genre)
    {
      this.TrackNo = trackNo;
      this.Artist = artist.TrimEnd();
      this.Album = album.TrimEnd();
      this.Title = title.TrimEnd();
      this.Year = year;
      this.Comment = comment.TrimEnd();
      this.FileName = filename;
      this.FullPath = fullPath;
      this.Genre = genre;
    }

    }
}
