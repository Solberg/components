﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Id3Tag.cs" company="Tripple S">
// 2011  
// </copyright>
// <summary>
//   Definition for an Id3 tag
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Id3.Domain
{
  using ID3;

    /// <summary>
  /// Definition for an Id3 tag
  /// </summary>
  public class Id3Tag : Id3BaseTag
  {
    #region Constants and Fields

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Id3Tag"/> class.
    /// </summary>
    /// <param name="trackNo">
    /// The track no.
    /// </param>
    /// <param name="artist">
    /// The artist.
    /// </param>
    /// <param name="album">
    /// The album.
    /// </param>
    /// <param name="title">
    /// The title.
    /// </param>
    /// <param name="year">
    /// The year.
    /// </param>
    /// <param name="comment">
    /// The comment.
    /// </param>
    /// <param name="filename">
    /// The filename.
    /// </param>
    /// <param name="fullPath">
    /// The full path.
    /// </param>
    /// <param name="genre">
    /// Mp3 genre
    /// </param>
    /// <param name="id3Info">
    /// All id3info for file.
    /// </param>
    public Id3Tag(
      byte trackNo, 
      string artist, 
      string album, 
      string title, 
      string year, 
      string comment, 
      string filename, 
      string fullPath,
      byte genre,
      ID3Info id3Info) : base(trackNo, artist, album, title, year, comment, filename, fullPath, genre)
    {
      this.Id3Info = id3Info;
    }

    #endregion

    #region Properties
    /// <summary>
    /// Do the file have a Id3V1 tag.
    /// </summary>
    public bool Id3V1
    {
      get
      {
        return Id3Info.ID3v1Info.HaveTag;
      }
    }

    /// <summary>
    /// Do the file have a Id3V1 tag.
    /// </summary>
    public bool Id3V2
    {
      get
      {
        return Id3Info.ID3v2Info.HaveTag;
      }
    }

    /// <summary>
    /// All id3 info for the file
    /// </summary>
    public ID3Info Id3Info { get; private set; }

    #endregion
  }
}