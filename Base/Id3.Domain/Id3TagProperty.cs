﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Id3.Domain
{
  public enum Id3TagProperty
  {
    Title,
    Artist,
    Year,
    Comment,
    Album,
    Genre
  }
}
