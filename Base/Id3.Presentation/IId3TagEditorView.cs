﻿using System;
using ID3;
namespace Id3.Presentation
{
    public interface IId3TagEditorView
    {
        // MP3 Struct
        /// <summary>
        /// The working m p 3.
        /// </summary>
        ID3Info WorkingMp3 { get; set; }
        object Genres { set; }
        string Title { get; set; }
        string Artist { get; set; }
        string Album { get; set; }
        string Year { get; set; }
        string Comment { get; set; }
        byte TrackNumber { get; set; }
        byte Genre { get; set; }
    }
}
