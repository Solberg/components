﻿using System.Collections.Generic;

namespace Id3.Presentation
{
  using Id3.Domain;
    using Id3.Services;

  public interface IListfiles : IPathSelector
  {
    List<Id3Tag> FileList { get; set; }

    /// <summary>
    /// Return a list of modified files.
    /// </summary>
    List<Id3Tag> ModifiedFiles { get; }

    object Genres { set; }

    string DefaultArtist { get; set; }

    string DefaultAlbum { get; set; }

    string DefaultYear { get; set; }

    string DefaultComment { get; set; }

    int DefaultGenre { get; set; }

    bool UpdateFileNames { get; }

    bool RenameOnly { get; }

    void StatusMessage(string message);

    /// <summary>
    /// Path to database.
    /// </summary>
    string DataPath { get; }
  }
}
