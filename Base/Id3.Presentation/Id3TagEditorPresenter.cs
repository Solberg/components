﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ID3;

namespace Id3.Presentation
{
    /// <summary>
    /// Presenter for a id3 editor.
    /// </summary>
    public class Id3TagEditorPresenter
    {
        #region Variables

        private readonly IId3TagEditorView _view;
        
        #endregion

        #region Constructors

        public Id3TagEditorPresenter(IId3TagEditorView view)
        {
            _view = view;
        }

        #endregion

        #region Public
        /// <summary>
        /// Load mp3 file info.
        /// </summary>
        /// <param name="fileName">
        /// </param>
        public void LoadMp3File(string fileName)
        {
            // If a file was selected get its ID3 Tag
            if (fileName.Length > 0)
            {
                _view.WorkingMp3 = new ID3Info(fileName, true);

                if (_view.WorkingMp3.ID3v2Info.HaveTag)
                {
                    string id3Track = _view.WorkingMp3.ID3v2Info.GetTextFrame("TRCK");
                }

                _view.Title = _view.WorkingMp3.ID3v1Info.Title;
                _view.Artist = _view.WorkingMp3.ID3v1Info.Artist;
                _view.Album = _view.WorkingMp3.ID3v1Info.Album;
                _view.Year = _view.WorkingMp3.ID3v1Info.Year;
                _view.Comment = _view.WorkingMp3.ID3v1Info.Comment;
                _view.TrackNumber = _view.WorkingMp3.ID3v1Info.TrackNumber;
                _view.Genre = _view.WorkingMp3.ID3v1Info.Genre;
            }
        }

        public void SaveMp3File()
        {
            if (_view.WorkingMp3.ID3v1Info.Title == null)
            {
                return;
            }

            _view.WorkingMp3.ID3v1Info.Album = _view.Album;
            _view.WorkingMp3.ID3v1Info.Artist = _view.Artist;
            _view.WorkingMp3.ID3v1Info.Title = _view.Title;
            _view.WorkingMp3.ID3v1Info.Comment = _view.Comment;
            _view.WorkingMp3.ID3v1Info.TrackNumber = _view.TrackNumber;
            _view.WorkingMp3.ID3v1Info.Year = _view.Year;
            _view.WorkingMp3.ID3v1Info.Genre = _view.Genre;

            _view.WorkingMp3.Save();
        }

        #endregion
    }
}
