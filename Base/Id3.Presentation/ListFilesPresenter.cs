﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListFilesPresenter.cs" company="Tripple S">
// 2011  
// </copyright>
// <summary>
//   Presenter for the ListFiles form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Id3.Presentation
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics.Contracts;
  using System.Reflection;

  using Id3.Domain;
  using Id3.Services;
  using System.IO;
    using System.Linq;

  /// <summary>
  /// Presenter for the ListFiles form.
  /// </summary>
  public class ListFilesPresenter
  {
    #region Variables

    /// <summary>
    /// View beeing presented
    /// </summary>
    private readonly IListfiles view;

    private readonly IId3Service _id3Service;

    private readonly IFileService _fileService;

    private readonly IGenreRepository _genreRepository;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="ListFilesPresenter"/> class.
    /// </summary>
    /// <param name="view">
    /// The view beeing presented
    /// </param>
    public ListFilesPresenter(IListfiles view, IId3Service id3Service, IFileService fileService)
    {
      this.view = view;
      _id3Service = id3Service;
      _fileService = fileService;
      _genreRepository = new GenreRepository(view.DataPath + @"Genre.xml");
    }
    #endregion

    #region Public

    /// <summary>
    /// Get default data for view.
    /// </summary>
    public void InitializeView()
    {
      this.view.Genres = _genreRepository.List();
    }

    /// <summary>
    /// Initialize filelist and prefill default fields
    /// </summary>
    public void GetFiles()
    {
      IFileService fileService = new FileService(this.view, _genreRepository);
      fileService.GetTags();

      this.view.FileList = fileService.Tags;

      this.view.DefaultArtist = FindGroupValue(fileService.Tags, "Artist");
      this.view.DefaultAlbum = FindGroupValue(fileService.Tags, "Album");
      this.view.DefaultYear = FindGroupValue(fileService.Tags, "Year");
      this.view.DefaultComment = FindGroupValue(fileService.Tags, "Comment");

      string genre = FindGroupValue(fileService.Tags, "Genre");

      if (!string.IsNullOrEmpty(genre))
      {
        this.view.DefaultGenre = Convert.ToInt32(genre);        
      }
    }

    /// <summary>
    /// Save files
    /// </summary>
    public void SaveFiles()
    {
      IFileService fileService = new FileService(this.view, _genreRepository);

      fileService.OnMessage += this.view.StatusMessage;

      if ((view.RenameOnly && fileService.RenameFiles(this.view.ModifiedFiles) == 100) ||
          fileService.UpdateFiles(this.view.ModifiedFiles) == 100)
      {
        this.GetFiles();
      }

      this.view.StatusMessage("Files saved");
    }


    /// <summary>
    /// update all file names in the list
    /// </summary>
    public void UpdateFileNames()
    {
      foreach (Id3Tag t in this.view.FileList)
      {
        _id3Service.UpdateFileName(t);
      }
    }

    /// <summary>
    /// Update one property for alle records with a value
    /// </summary>
    /// <param name="property">
    /// The property.
    /// </param>
    /// <param name="value">
    /// The value.
    /// </param>
    public void UpdateRecords(Id3TagProperty property, string value)
    {
        _id3Service.UpdateRecords(view.FileList, property, value, view.UpdateFileNames);
    }


    /// <summary>
    /// Guess titles from filenames
    /// </summary>
    public void GuessTitles()
    {
      _id3Service.GuessTitles(this.view.FileList);
    }

    public void UpdateFileName(Id3.Domain.Id3Tag tag)
    {
        _id3Service.UpdateFileName(tag);
    }

    #endregion

    #region Private


    /// <summary>
    /// Find value which occures most times in a column.
    /// </summary>
    /// <param name="tags">List of Id3Tags to run through</param>
    /// <param name="columnName">Name of column to examine</param>
    /// <returns>Value that occures the most</returns>
    private static string FindGroupValue(
      List<Id3Tag> tags,
      string columnName)
    {
      Contract.Requires<ArgumentNullException>(tags != null);
      Contract.Requires<ArgumentNullException>(!String.IsNullOrEmpty(columnName));

      var values = new Dictionary<object, int>();
      Type t;
      PropertyInfo pi;

      if (tags.Count > 0)
      {
        t = tags[0].GetType();
        pi = t.GetProperty(columnName);

        foreach (Id3Tag tag in tags)
        {
            if (tag != null)
            {
                object value = pi.GetValue(tag, null);

                if (value != null)
                {
                    if (values.ContainsKey(value))
                    {
                        values[value] = values[value] + 1;
                    }
                    else
                    {
                        values.Add(value, 1);
                    }
                }
            }
        }
      }

      if (values.Count > 0)
      {
          // Find the value with the highest count.
          var items = (from value in values
                       select value).OrderByDescending(value => value.Value).First();

          return items.Key.ToString();
      }
      else
      {
          return string.Empty;
      }
      
    }


    #endregion
  }
}
