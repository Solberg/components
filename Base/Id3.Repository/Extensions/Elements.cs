﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Id3.Extensions
{
  using System.Xml.Linq;

  // Create a public static class
  public static class Elements
  {
    // Create the extension method that will be called from the Linq query. Give it a meaningful name in this cans
    // AttributeExist of generic type T also in this case returning a bool but can be of any type. The "this" keyword
    // in the first parameter makes it a extension method. The where at the end restricts it to type of XAttributes
    public static bool AttributeExist<T>(this IEnumerable<T> source, string attributeName) where T : XAttribute
    {
      // Set up the return value
      bool nameFound = false;

      // Check the attribute names for a match
      foreach (XAttribute a in source)
      {
        if (a.Name == attributeName)
        {
          // Attribute name was found set value and exit foreach
          nameFound = true;
          break;
        }
      }
      return nameFound;
    }

    // Create the extension method that will be called from the Linq query. Give it a meaningful name in this cans
    // AttributeExist of generic type T also in this case returning a bool but can be of any type. The "this" keyword
    // in the first parameter makes it a extension method. The where at the end restricts it to type of XAttributes
    public static bool ElementExist<T>(this IEnumerable<T> source, string elementName) where T : XElement
    {
      // Set up the return value
      bool nameFound = false;

      // Check the attribute names for a match
      foreach (XElement a in source)
      {
        if (a.Name == elementName)
        {
          // Attribute name was found set value and exit foreach
          nameFound = true;
          break;
        }
      }
      return nameFound;
    }

  }
}
