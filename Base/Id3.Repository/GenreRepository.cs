﻿namespace Id3.Services
{
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;
  using System.Xml.Linq;

  using Id3.Domain;
    using Extensions;
  
  /// <summary>
  /// Service handling Genres
  /// </summary>
  public class GenreRepository : IGenreRepository
  {
    #region Variables

    /// <summary>
    /// doc used to handle xml
    /// </summary>
    private XDocument doc;

    #endregion

    #region Constructors
    public GenreRepository(string dataPath)
    {
        //Path.GetDirectoryName(Application.ExecutablePath) + @"\App_Data\Genre.xml"
      this.doc = XDocument.Load(dataPath);
    }
    #endregion

    #region Public

    /// <summary>
    /// Get a genre by it's id
    /// </summary>
    /// <param name="id">
    /// Id for genre
    /// </param>
    /// <returns>
    /// Genre instance
    /// </returns>
    /// <exception cref="Exception">
    /// </exception>
    public Genre GetById(int id)
    {
      var rows = from row in this.doc.Descendants("row")
                 where
                   row.Element("id").Value == id.ToString()
                 select
                   new Genre(
                 row.Elements().ElementExist("id") ? Convert.ToInt32(row.Element("id").Value) : 0, row.Elements().ElementExist("text") ? row.Element("text").Value : string.Empty);

      if (rows.Count() == 0)
      {
        throw  new Exception("Genre not found");
      }

      return rows.ElementAt(0);
    }

    public Genre GetByText(string text)
    {
        var rows = from row in this.doc.Descendants("row")
                   where
                     row.Element("text").Value == text
                   select
                     new Genre(row.Elements().ElementExist("id") ? Convert.ToInt32(row.Element("id").Value) : 0, row.Elements().ElementExist("text") ? row.Element("text").Value : string.Empty);

        if (rows.Count() == 0)
        {
            return null;
        }

        return rows.ElementAt(0);

    }

    /// <summary>
    /// Get a list of all genres
    /// </summary>
    /// <returns>
    /// List of all genres
    /// </returns>
    public IList<Genre> List()
    {
      var rows =
        this.doc.Descendants("row").Select(
        row => new Genre(row.Elements().ElementExist("id") ? Convert.ToInt32(row.Element("id").Value) : 0, row.Elements().ElementExist("text") ? row.Element("text").Value : string.Empty));

      return rows.ToList();

    }

    #endregion


  }
}
