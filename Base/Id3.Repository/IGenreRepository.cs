﻿namespace Id3.Services
{
  using System.Collections.Generic;

  using Id3.Domain;

  public interface IGenreRepository
  {
    Genre GetById(int id);

    Genre GetByText(string text);

    IList<Genre> List();
  }
}
