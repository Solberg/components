﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ID3;
using Id3.Domain;
using Id3.Services;

namespace Id3.Repository
{
    public class Id3Factory
    {
        #region Variables
        IGenreRepository genreRepository;
        #endregion

        #region Constructors

        public Id3Factory(IGenreRepository genreRepository)
        {
            this.genreRepository = genreRepository;
        }

        #endregion


        public Id3Tag Create(ID3Info id3Info)
        {
            if (id3Info.ID3v1Info.HaveTag)
            {
                return new Id3Tag(id3Info.ID3v1Info.TrackNumber,
                                  id3Info.ID3v1Info.Artist,
                                  id3Info.ID3v1Info.Album,
                                  id3Info.ID3v1Info.Title,
                                  id3Info.ID3v1Info.Year,
                                  id3Info.ID3v1Info.Comment,
                                  id3Info.ID3v1Info.FileName,
                                  id3Info.ID3v1Info.FilePath,
                                  id3Info.ID3v1Info.Genre,
                                  id3Info);
            }
            else if (id3Info.ID3v2Info.HaveTag)
            {
                string track = id3Info.ID3v2Info.GetTextFrame("TRCK");
                byte trackNo = 0;

                if (track.Contains("�"))
                {
                    track = track.Replace("�", string.Empty);
                }

                if (track.Contains("/"))
                {
                    track = track.Substring(0, track.IndexOf("/"));
                }


                if (track != string.Empty)
                {
                    trackNo = System.Convert.ToByte(track);
                }

                Genre genre = genreRepository.GetByText(id3Info.ID3v2Info.GetTextFrame("TCON").Replace("�", string.Empty));

                return new Id3Tag(trackNo,
                    id3Info.ID3v2Info.GetTextFrame("TPE1").Replace("�", string.Empty),
                    id3Info.ID3v2Info.GetTextFrame("TALB").Replace("�", string.Empty),
                    id3Info.ID3v2Info.GetTextFrame("TIT2").Replace("�", string.Empty),
                    id3Info.ID3v2Info.GetTextFrame("TYER").Replace("�", string.Empty),
                    id3Info.ID3v2Info.GetTextFrame("COMM").Replace("�", string.Empty),
                    id3Info.ID3v2Info.FileName,
                    id3Info.ID3v2Info.FilePath,
                    Convert.ToByte(genre != null ? genre.Id : 0),
                    id3Info                 
                    );

            }

            return null;
        }
    }
}
