﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Id3Service.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Id3Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Id3.Services
{
  using ID3;

  using Id3.Domain;

  public class Id3Repository
  {
    #region Variables

    private IGenreRepository genreService;
    #endregion

    #region Constructors
    public Id3Repository(IGenreRepository genreService)
    {
      this.genreService = genreService;
    }
    #endregion


    /// <summary>
    /// Update V1 info.
    /// </summary>
    private void UpdateId3V1Info(ID3Info id3Info, Id3Tag id3Tag)
    {
      id3Info.ID3v1Info.HaveTag = true;
      id3Info.ID3v1Info.Album = Trim(id3Tag.Album, 30);
      id3Info.ID3v1Info.Artist = id3Tag.Artist ?? string.Empty;
      id3Info.ID3v1Info.Comment = id3Tag.Comment ?? string.Empty;
      id3Info.ID3v1Info.Genre = id3Tag.Genre;
      id3Info.ID3v1Info.Title = Trim(id3Tag.Title, 30);
      id3Info.ID3v1Info.TrackNumber = id3Tag.TrackNo;
      id3Info.ID3v1Info.Year = id3Tag.Year ?? string.Empty;
    }

    /// <summary>
    /// Update V1 info.
    /// </summary>
    private void UpdateId3V2Info(ID3Info id3Info, Id3Tag id3Tag)
    {
      int ver;

      if (!id3Info.ID3v2Info.HaveTag || (id3Info.ID3v2Info.VersionInfo.Minor != 3 && id3Info.ID3v2Info.VersionInfo.Minor != 4))
      {
        ver = 3;
      }
      else
      {
        ver = id3Info.ID3v2Info.VersionInfo.Minor;        
      }

      id3Info.ID3v2Info.HaveTag = true;
      id3Info.ID3v2Info.SetMinorVersion(ver);
      id3Info.ID3v2Info.SetTextFrame("TIT2", id3Tag.Title);
      //id3Info.ID3v2Info.SetTextFrame("TRCK", id3Tag.TrackNo.ToString());
//      id3Info.ID3v2Info.SetTextFrame("TPOS", string.Empty); // Set
      id3Info.ID3v2Info.SetTextFrame("TPE1", id3Tag.Artist);
      id3Info.ID3v2Info.SetTextFrame("TALB", id3Tag.Album);
      if (id3Tag.Genre > 0)
      {
          id3Info.ID3v2Info.SetTextFrame("TCON", this.genreService.GetById(id3Tag.Genre).Text);
      }
  //    id3Info.ID3v2Info.SetTextFrame("TLAN", string.Empty); // Languate
    }


    private string Trim(string s, int length)
    {
        return s != null ? s.Length > length ? s.Substring(0, length) : s : string.Empty;
    }

    public void SaveFile(Id3Tag id3Tag)
    {
      ID3Info id3Info = id3Tag.Id3Info;

      UpdateId3V1Info(id3Info, id3Tag);
      UpdateId3V2Info(id3Info, id3Tag);
      id3Info.Save();
    }
  }
}
