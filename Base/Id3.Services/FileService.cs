﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Id3.Domain;
using System.IO;
using ID3;
using Id3.Repository;

namespace Id3.Services
{
    public class FileService : Id3.Services.IFileService
    {
    public delegate void MessageDelegate(string message);

    #region Constants and Fields

    private readonly IGenreRepository _genreRepository;

    private readonly IPathSelector _pathSelector;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="FileList"/> class. 
    /// </summary>
    /// <param name="path">
    /// The is path.
    /// </param>
    public FileService(IPathSelector pathSelector, IGenreRepository genreRepository)
    {
        _genreRepository = genreRepository;
      this.Tags = new List<Id3Tag>();
      _pathSelector = pathSelector;
    }

    #endregion

    #region Properties

    /// <summary>
    /// Gets Table.
    /// </summary>
    public List<Id3Tag> Tags
    {
      get; private set;
    }

    #endregion

    #region Event handlers

    public MessageDelegate OnMessage { get; set; }
    #endregion

    #region Public Methods

    /// <summary>
    /// Update ID3 tags for MP3 files and rename files if filename changed
    /// </summary>
    /// <param name="tags">List of Id3 tags</param>
    /// <returns>
    /// 0   : Update ok
    /// 100 : Update ok and one or more file renamed.
    /// </returns>
    public int UpdateFiles(List<Id3Tag> tags)
    {
      int returnCode = 0;

      if (tags != null)
      {
        foreach (var tag in tags)
        {
          new Id3Repository(_genreRepository).SaveFile(tag);

          returnCode = RenameFile(tag);

          if (this.OnMessage != null)
          {
            this.OnMessage(string.Format("File {0} saved", tag.FileName));
          }
        }
      }

      return returnCode;
    }

    public int RenameFiles(List<Id3Tag> tags)
    {
        int returnCode = 0;

        if (tags != null)
        {
            foreach (var tag in tags)
            {
                returnCode = RenameFile(tag);

                if (this.OnMessage != null)
                {
                    this.OnMessage(string.Format("File {0} renamed", tag.FileName));
                }
            }
        }

        return returnCode;
    }

    /// <summary>
    /// Get tags for alle files in a directory.
    /// </summary>
    public void GetTags()
    {
        var di = new DirectoryInfo(_pathSelector.Path);

        foreach (FileInfo fi in di.GetFiles("*.mp3"))
        {
            var id3Info = new ID3Info(fi.FullName, true);

            this.Tags.Add(new Id3Factory(_genreRepository).Create(id3Info));
        }
    }

    #endregion

    #region Private
    private int RenameFile(Id3Tag tag)
    {
        string physicalName = Path.GetFileName(tag.FullPath);

        if (physicalName != tag.FileName)
        {
            File.Move(_pathSelector.Path + "\\" + physicalName, _pathSelector.Path + "\\" + tag.FileName);
        }

        return 100;
    }
        #endregion

    }
}
