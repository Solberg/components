﻿using System;
namespace Id3.Services
{
    public interface IFileService
    {
        FileService.MessageDelegate OnMessage { get; set; }
        System.Collections.Generic.List<Id3.Domain.Id3Tag> Tags { get; }
        int UpdateFiles(System.Collections.Generic.List<Id3.Domain.Id3Tag> tags);
        int RenameFiles(System.Collections.Generic.List<Id3.Domain.Id3Tag> tags);
        void GetTags();
    }
}
