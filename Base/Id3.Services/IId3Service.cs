﻿using System;
using System.Collections.Generic;
using Id3.Domain;

namespace Id3.Services
{
    public interface IId3Service
    {
        void GuessArtist(Id3Tag tag);

        void GuessTitle(Id3Tag tag);

        void UpdateFileName(Id3Tag tag);

        void GuessTitles(List<Id3Tag> items);

        void UpdateRecords(List<Id3Tag> items, Id3TagProperty property, string value, bool updateFileName);
    }
}
