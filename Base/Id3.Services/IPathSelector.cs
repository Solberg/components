﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Id3.Services
{
    public interface IPathSelector
    {
        string Path { get; }
    }
}
