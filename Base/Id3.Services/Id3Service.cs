﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Id3.Domain;

namespace Id3.Services
{
    public class Id3Service : Id3.Services.IId3Service
    {

        /// <summary>
        /// Update one property for alle records with a value
        /// </summary>
        /// <param name="property">
        /// The property.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void UpdateRecords(List<Id3Tag> items, Id3TagProperty property, string value, bool updateFileName)
        {
            if (value.Length > 0)
            {
                foreach (Id3Tag t in items)
                {
                    if (property == Id3TagProperty.Album)
                    {
                        t.Album = value;
                    }
                    else if (property == Id3TagProperty.Artist)
                    {
                        t.Artist = value;
                    }
                    else if (property == Id3TagProperty.Comment)
                    {
                        t.Comment = value;
                    }
                    else if (property == Id3TagProperty.Genre)
                    {
                        t.Genre = Convert.ToByte(value);
                    }
                    else
                    {
                        t.Year = value;
                    }

                    if (property != Id3TagProperty.Year
                      && property != Id3TagProperty.Comment
                      && property != Id3TagProperty.Genre)
                    {
                        if (updateFileName)
                        {
                            this.UpdateFileName(t);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Guess titles from filenames
        /// </summary>
        public void GuessTitles(List<Id3Tag> items)
        {
            foreach (Id3Tag id3Tag in items)
            {
                GuessTitle(id3Tag);
            }
        }

        /// <summary>
        /// Guess title from FileName
        /// </summary>
        /// <param name="tag">Id3 tag to be handled</param>
        public void GuessTitle(Id3Tag tag)
        {
            string[] a = tag.FileName.Split('-');

            if (a.Length > 0)
            {
                string title = a[a.Length - 1].Trim();

                if (title.Length > 3 && title[2] == '.')
                {
                    title = title.Substring(3);
                }

                tag.Title = title.Trim().Replace(".mp3", string.Empty);
            }
        }

        /// <summary>
        /// Create physical filename from the values in the ID3 tag
        /// </summary>
        /// <param name="tag">
        /// Tag beeing used for naming the file
        /// </param>
        public void UpdateFileName(Id3Tag tag)
        {
            if (tag.Artist != String.Empty
              && tag.Album != String.Empty
              && tag.Title != String.Empty)
            {
                tag.FileName = String.Format("{0} - {1} - {2:00}. {3}.mp3", tag.Artist, tag.Album, tag.TrackNo, tag.Title);
                tag.FileName = tag.FileName.Replace("?", String.Empty);
            }
        }


        public void GuessArtist(Id3Tag tag)
        {
            string[] a = tag.FileName.Split('-');

            if (a.Length > 0)
            {
                string artist = a[0].Trim();

                tag.Artist = artist;
            }
        }
    }


}
