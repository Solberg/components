using System;

namespace SSL.Data.SqlServer
{
  public class DbConvert
  {
    public static string ToString(object o)
    {
      return Convert.IsDBNull(o) ? String.Empty : Convert.ToString(o);
    }

    public static  DateTime ToDateTime(object o)
    {
      return Convert.IsDBNull(o) ? DateTime.MinValue : Convert.ToDateTime(o);
    }

    public static bool ToBoolean(object o)
    {
      return Convert.IsDBNull(o) ? false : Convert.ToBoolean(o);
    }


    public static Double ToDouble(object o)
    {
      return Convert.IsDBNull(o) ? 0 : Convert.ToDouble(o);
    }

    public static Decimal ToDecimal(object o)
    {
      return Convert.IsDBNull(o) ? 0 : Convert.ToDecimal(o);
    }


    public static int ToInt32(object o)
    {
      return Convert.IsDBNull(o) ? 0 : Convert.ToInt32(o);
    }

    /// <summary>
    /// Convert to string that can be parsed to the sql server eg. special chars
    /// as '
    /// </summary>
    /// <param name="o"></param>
    /// <returns></returns>
    /// <programmer version="2.1.0" date="20080626" programmer="SSL"></programmer>
    public static string ToSqlString(object o)
    {
      return Convert.IsDBNull(o) ? String.Empty : Convert.ToString(o).Replace("'", "''");
    }
  }
}
