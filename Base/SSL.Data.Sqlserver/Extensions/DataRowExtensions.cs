﻿using System;
using System.Data;

namespace SSL.Data.SqlServer.Extensions
{
  public static class DataRowExtensions
  {

    public static string ToString(this DataRow row, string fieldName)
    {
      if (Convert.IsDBNull(row[fieldName]))
      {
        return null;
      }

      return Convert.ToString(row[fieldName]);
    }

    public static int ToInt32(this DataRow row, string fieldName)
    {
      if (Convert.IsDBNull(row[fieldName]))
      {
        return 0;
      }

      try
      {
        return Convert.ToInt32(row[fieldName]);
      }
      catch (Exception)
      {
        return 0;
      }

    }

    public static bool ToBoolean(this DataRow row, string fieldName)
    {
      if (Convert.IsDBNull(row[fieldName]))
      {
        return false;
      }

      try
      {
        return Convert.ToBoolean(row[fieldName]);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static DateTime? ToNullableDateTime(this DataRow row, string fieldName)
    {
      if (Convert.IsDBNull(row[fieldName]))
      {
        return null;
      }

      try
      {
        return Convert.ToDateTime(row[fieldName]);
      }
      catch (Exception)
      {
        return null;
      }

    }


  }
}
