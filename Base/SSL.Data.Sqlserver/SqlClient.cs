namespace SSL.Data.SqlServer
{
  using System;
  using System.Data;
  using System.Data.SqlClient;

  public class SqlClient
  {

    #region Consts
    private const string ReturnValue = "@RETURN_VALUE";
    #endregion

    #region Variables
    private SqlConnection _conn;
    protected string ConnectionString;
    private readonly bool _autoClose = true;
    #endregion

    #region Constructors

    public SqlClient() : this(true)
    {
    }

    public SqlClient(bool autoClose)
    {
      _autoClose = autoClose;
    }

    #endregion

    #region Methods
    /// <summary>
    /// Dan ny oledbconnection.
    /// </summary>
    /// <returns></returns>
    /// <remarks>
    /// <revision date="20070509" programmer="S�ren Solberg Larsen">Returnerer OleDbConnection i stedet for null</revision>
    /// </remarks>
    public virtual SqlConnection Connect()
    {
      return new SqlConnection(ConnectionString);
    }

    private void OpenConnection()
    {
      if (_conn == null)
      {
        _conn = Connect();
      }

      if (_conn.State == ConnectionState.Closed)
      {
        _conn.Open();
      }
    }

    #endregion


    private void ExecStoredProcedure(string storedProcedure,
      bool throwOnError,
      ref SqlParameterCollection parameters)
    {
      OpenConnection();

      var cmd = new SqlCommand(storedProcedure, _conn) {CommandType = CommandType.StoredProcedure};

      /// Add parameter for return value
      if (! parameters.Contains(ReturnValue))
      {
        var p = new SqlParameter(ReturnValue, SqlDbType.Int);

        p.Direction = ParameterDirection.ReturnValue;

        parameters.Add(p);
      }

      for (int i = 0; i < parameters.Count; i++)
        cmd.Parameters.Add(parameters[i]);

      try
      {
        cmd.ExecuteNonQuery();

        if (throwOnError && Convert.ToInt32(cmd.Parameters[ReturnValue].Value) != 0)
        {
          throw new StoredProcedureException(storedProcedure, Convert.ToInt32(cmd.Parameters[ReturnValue].Value));  
        }
      }
      finally
      {
        if (_autoClose)
        {
          Close();
        }
      }
    }


    /// <summary>
    /// Execute a stored procedure.
    /// </summary>
    /// <param name="storedProcedure">Name of stored procedure to call</param>
    /// <param name="parameters">Parameters for the stored procedure</param>
    private void ExecStoredProcedure(string storedProcedure, 
      ref SqlParameterCollection parameters)
    {
      ExecStoredProcedure(storedProcedure, true, ref parameters);
    }


    /// <summary>
    /// Close database connection for object.
    /// </summary>
    public void Close()
    {
      if (_conn.State == ConnectionState.Open)
      {
        _conn.Close();
      }
    }

    protected virtual void QSetFields(DataTable dt)
    {

    }


    protected DataView StandardList(string storedProcedure)
    {
      var ac = new SqlParameterCollection();
      return StandardList(storedProcedure, ac, String.Empty);
    }

    protected DataView StandardList(string storedProcedure, string sortOrder)
    {
      var ac = new SqlParameterCollection();

      return StandardList(storedProcedure, ac, sortOrder);
    }


    protected DataView StandardList(string storedProcedure,
      SqlParameter[] parameters,
      string sortOrder)
    {
      var ac = new SqlParameterCollection();

      foreach (SqlParameter p in parameters)
      {
        ac.Add(p);
      }

      return StandardList(storedProcedure, ac, String.Empty);
    }


    protected DataView StandardList(string storedProcedure,
      SqlParameterCollection parameters)
    {
      return StandardList(storedProcedure, parameters, String.Empty);
    }


    protected DataView StandardList(string storedProcedure,
      SqlParameterCollection parameters,
      string sortOrder)
    {
      OpenConnection();

      var dt = new DataTable();
      var cmd = new SqlCommand(storedProcedure, _conn) {CommandType = CommandType.StoredProcedure};

      if (parameters != null)
      {
        for (int i = 0; i < parameters.Count; i++)
          cmd.Parameters.Add(parameters[i]);
      }

      var da = new SqlDataAdapter {SelectCommand = cmd};
      try
      {
        da.Fill(dt);
      }
      finally
      {
        if (_autoClose)
          _conn.Close();
      }
      dt.DefaultView.Sort = sortOrder;
      return dt.DefaultView;
    }


    protected bool ExecFetchCommand(string storedProcedure,
      SqlParameterCollection parameters)
    {
      OpenConnection();

      var dt = new DataTable();
      var cmd = new SqlCommand(storedProcedure, _conn) {CommandType = CommandType.StoredProcedure};

      foreach (var parm in parameters)
        cmd.Parameters.Add(parm);

      var da = new SqlDataAdapter {SelectCommand = cmd};

      try
      {
        da.Fill(dt);
      }
      finally
      {
        if (_autoClose)
          _conn.Close();
      }

      if (dt.Rows.Count > 0)
      {
        QSetFields(dt);
      }
      return dt.Rows.Count > 0;
    }

    protected bool StandardFetchIfExists(string storedProcedure,
      ref SqlParameterCollection parameters)
    {
      ExecStoredProcedure(storedProcedure, false, ref parameters);

      return Convert.ToInt32(Convert.IsDBNull(parameters[ReturnValue].Value) ? 1 : parameters[ReturnValue].Value) == 0;

    }


    protected void StandardFetch(string storedProcedure,
      ref SqlParameterCollection parameters)
    {
      ExecStoredProcedure(storedProcedure,ref parameters);
    }

    protected void StandardCreate(string storedProcedure,
      ref SqlParameterCollection parameters)
    {
      ExecStoredProcedure(storedProcedure, ref parameters);
    }

    protected void StandardDelete(string storedProcedure,
    SqlParameterCollection parameters)
    {
      ExecStoredProcedure(storedProcedure, ref parameters);
    }

    protected void StandardUpdate(string storedProcedure,
    ref SqlParameterCollection parameters)
    {
      ExecStoredProcedure(storedProcedure, ref parameters);
    }

  }
}
