using System.Data;
using System.Data.SqlClient;

namespace SSL.Data.SqlServer
{
  public class SqlParameterCollection : System.Collections.CollectionBase
  {
    #region Properties
    public SqlParameter this[int index]
    {
      get { return (SqlParameter)InnerList[index]; }
    }

    public SqlParameter this[string key]
    {
      get 
      {
        foreach (var o in InnerList)
        {
          if (o.ToString() == key)
          {
            return (SqlParameter)o;
          }
        }

        return null;
      }
    }
    #endregion

    #region Methods
      public int Add(SqlParameter parameter)
      {
        return InnerList.Add(parameter);
      }

      public int Add(SqlParameter parameter, object value)
      {
        parameter.Value = value;
        return InnerList.Add(parameter);
      }

      public int Add(SqlParameter parameter, ParameterDirection direction)
      {
        parameter.Direction = direction;
        return InnerList.Add(parameter);
      }

      public int Add(SqlParameter parameter, ParameterDirection direction, byte precision)
      {
        parameter.Precision = precision;
        return Add(parameter, direction);
      }

      public int Add(SqlParameter parameter, object value, byte precision)
      {
        parameter.Precision = precision;
        return Add(parameter, value);
      }

      public void Remove(SqlParameter parameter)
      {
        InnerList.Remove(parameter);
      }

      public bool Contains(string parameterByName)
      {
        foreach (SqlParameter dt in this)
        {
          if (parameterByName == dt.ParameterName)
            return true;
        }
        return false;
      }

      public bool Contains(SqlParameter parameter)
      {
        foreach (SqlParameter actualParameter in this)
        {
          if (parameter == actualParameter)
            return true;
        }
        return false;
      }
      #endregion
  }
}
