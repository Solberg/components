﻿using System.Data.SqlClient;

namespace SSL.Data.SqlServer
{
  using System;

  /// <summary>
  /// Exception is thrown if a call to a stored procedure returns a value different from 0.
  /// </summary>
  class StoredProcedureException : ApplicationException 
  {
    #region Variables

    private readonly string _storedProcedure;
    private readonly int _errorCode;
    #endregion

    #region Properties

    public override string Message
    {
      get
      {
        if (String.IsNullOrEmpty(_storedProcedure))
        {
          return base.Message;
        }
        
        return String.Format("Call to stored procedure {0} returned code {1}.", _storedProcedure, _errorCode);
      }
    }


    #endregion

    #region Constructors

    /// <summary>
    /// Ordinary constructor with a message.
    /// </summary>
    /// <param name="message"></param>
    public StoredProcedureException(string message) : base(message)
    {
    }


    public StoredProcedureException(string storedProcedure, int errorCode) : base()
    {
      _storedProcedure = storedProcedure;
      _errorCode = errorCode;
    }

    #endregion

  }
}
