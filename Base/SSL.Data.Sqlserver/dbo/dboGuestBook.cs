using System;
using System.Data;
using System.Data.SqlClient;

namespace SSL.Data.SqlServer.Dbo
{
  /// <summary>
  /// Dataobject for the GuestBook table.
  /// </summary>
  public class dboGuestBook : SqlClient
  {
    #region Constructors
    public dboGuestBook()
    {
    }

    public dboGuestBook(bool IbAutoClose) : base(IbAutoClose)
    {
    }

    /// <summary>
    /// Forbind med connectionstring angivet i parameter.
    /// </summary>
    /// <param name="IsConnectionString"></param>
    public dboGuestBook(string IsConnectionString)
    {
      ConnectionString = IsConnectionString;
    }

    #endregion


    #region Methods

    //Select From Database
    public DataView List(string IsSortOrder)
    {
      return StandardList("spGuestBookList", IsSortOrder);
    }

    //Select From Database
    public int RecordCount()
    {
      return StandardList("spGuestBookList").Table.Rows.Count;
    }



    public void Fetch(int IiId,
      out string IsName,
      out string OsMail,
      out string OsHomePageTitle,
      out string OsHomePageUrl,
      out int OiReferredBy,
      out string OsLocation,
      out string OsAddOn,
      out string OsComment,
      out bool ObPrivate,
      out DateTime OdStamp)
    {
      SqlParameterCollection spc = new SqlParameterCollection();
      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiId);
      spc.Add(new SqlParameter("@OsName", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsMail", SqlDbType.VarChar, 256), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsHomePageTitle", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsHomePageUrl", SqlDbType.VarChar, 100), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OiReferredBy", SqlDbType.TinyInt), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsLocation", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsAddOn", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsComment", SqlDbType.VarChar, 500), ParameterDirection.Output);
      spc.Add(new SqlParameter("@ObPrivate", SqlDbType.Bit), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OdStamp", SqlDbType.DateTime), ParameterDirection.Output);

      StandardFetch("sp_GuestBookFetch", ref spc);

      IsName          =  DbConvert.ToString(spc[1].Value); 
      OsMail          =  DbConvert.ToString(spc[2].Value); 
      OsHomePageTitle =  DbConvert.ToString(spc[3].Value); 
      OsHomePageUrl   =  DbConvert.ToString(spc[4].Value); 
      OiReferredBy    =  DbConvert.ToInt32(spc[5].Value); 
      OsLocation      =  DbConvert.ToString(spc[6].Value); 
      OsAddOn         =  DbConvert.ToString(spc[7].Value); 
      OsComment       =  DbConvert.ToString(spc[8].Value); 
      ObPrivate       =  DbConvert.ToBoolean(spc[9].Value);
      OdStamp         =  DbConvert.ToDateTime(spc[10].Value); 
    }


    /// <summary>
    /// Create a new entry in the guestbook.
    /// </summary>
    /// <param name="IsName"></param>
    /// <param name="IsMail"></param>
    /// <param name="IsHomePageTitle"></param>
    /// <param name="IsHomePageUrl"></param>
    /// <param name="IiReferredBy"></param>
    /// <param name="IsLocation"></param>
    /// <param name="IsAddOn"></param>
    /// <param name="IsComment"></param>
    /// <param name="IbPrivate"></param>
    /// <returns></returns>
    public void Create(string IsName,
      string IsMail,
      string IsHomePageTitle,
      string IsHomePageUrl,
      int    IiReferredBy,
      string IsLocation,
      string IsAddOn,
      string IsComment,
      bool IbPrivate)
    {
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IsName", SqlDbType.VarChar, 50), IsName);
      spc.Add(new SqlParameter("@IsMail", SqlDbType.VarChar, 256), IsMail);
      spc.Add(new SqlParameter("@IsHomePageTitle", SqlDbType.VarChar, 50), IsHomePageTitle);
      spc.Add(new SqlParameter("@IsHomePageUrl", SqlDbType.VarChar, 100), IsHomePageUrl);
      spc.Add(new SqlParameter("@IiReferredBy", SqlDbType.TinyInt), IiReferredBy);
      spc.Add(new SqlParameter("@IsLocation", SqlDbType.VarChar, 50), IsLocation);
      spc.Add(new SqlParameter("@IsAddOn", SqlDbType.VarChar, 50), IsAddOn);
      spc.Add(new SqlParameter("@IsComment", SqlDbType.VarChar, 500), IsComment);
      spc.Add(new SqlParameter("@IbPrivate", SqlDbType.Bit), IbPrivate);
      spc.Add(new SqlParameter("@IdTimeStamp", SqlDbType.DateTime), System.DateTime.Now);


      StandardCreate("spGuestBookCreate", ref spc);
    }
    #endregion
  }
}
