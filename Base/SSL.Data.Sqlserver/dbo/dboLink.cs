using System.Data;
using System.Data.SqlClient;

namespace SSL.Data.SqlServer.Dbo
{
  /// <summary>
  /// Dataobjekt for tabellen Link.
  /// </summary>
  public class dboLink : SqlClient
  {

    #region Constructors
    public dboLink()
    {
    }

    public dboLink(bool IbAutoClose)
      : base(IbAutoClose)
    {
    }

    /// <summary>
    /// Forbind med connectionstring angivet i parameter.
    /// </summary>
    /// <param name="IsConnectionString"></param>
    public dboLink(string IsConnectionString)
    {
      ConnectionString = IsConnectionString;
    }

    #endregion


    //Select From Database
    public DataView List(string IsSortOrder)
    {
      return StandardList("spLinkList", IsSortOrder);
    }

    //Select From Database
    public DataView ListActive(string IsSortOrder)
    {
      return StandardList("spLinkListActive", IsSortOrder);
    }


    public void Create(string IsName,
      string IsUrl,
      string IsDescription,
      int IiActive)
    {
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IsName", SqlDbType.VarChar, 100), IsName);
      spc.Add(new SqlParameter("@IsUrl", SqlDbType.VarChar, 256), IsUrl);
      spc.Add(new SqlParameter("@IsDescription", SqlDbType.VarChar, 256), IsDescription);
      spc.Add(new SqlParameter("@IiActive", SqlDbType.TinyInt), IiActive);

      StandardCreate("spLinkCreate", ref spc);
    }
  }
}
