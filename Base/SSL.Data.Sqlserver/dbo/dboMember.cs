using System;
using System.Data;
using System.Data.SqlClient;

namespace SSL.Data.SqlServer.Dbo
{
  public class dboMember : SqlClient
  {


    #region Constructors
    public dboMember()
    {
    }

    public dboMember(bool IbAutoClose)
      : base(IbAutoClose)
    {
    }
    #endregion


    //Select From Database
    public DataView List(string IsSortOrder)
    {
      return StandardList("sp_Member_select", IsSortOrder);
    }

    //Search From Database
    public bool Fetch(int IiId,
      out string OsLogin,
      out string OsName,
      out string OsPhone,
      out string OsMail,
      out string OsPassword,
      out int OiRole,
      out DateTime OdLastLogin)
    {
    	var spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiId);
      spc.Add(new SqlParameter("@OsLogin", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsName", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsPhone", SqlDbType.VarChar, 8), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsMail", SqlDbType.VarChar, 100), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OsPassword", SqlDbType.VarChar, 50), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OiRole", SqlDbType.Int), ParameterDirection.Output);
      spc.Add(new SqlParameter("@OdLastLogin", SqlDbType.DateTime), ParameterDirection.Output);

      bool ok = StandardFetchIfExists("spMemberFetch", ref spc);

      OsLogin =  DbConvert.ToString(spc[1].Value);
      OsName =  DbConvert.ToString(spc[2].Value);
      OsPhone =  DbConvert.ToString(spc[3].Value);
      OsMail =  DbConvert.ToString(spc[4].Value);
      OsPassword =  DbConvert.ToString(spc[5].Value);
      OiRole =  DbConvert.ToInt32(spc[6].Value);
      OdLastLogin = DbConvert.ToDateTime(spc[7].Value);
      return ok;
    }

    /// <summary>
    /// Search on login/password
    /// </summary>
    /// <returns></returns>
    public bool FetchDataOnLogin(string IsLogin,
      string IsPassword,
      out int OiId)
    {
    	var spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IsLogin", SqlDbType.VarChar, 50), IsLogin);
      spc.Add(new SqlParameter("@IsPassword", SqlDbType.VarChar, 50), IsPassword);
      spc.Add(new SqlParameter("@OiId", SqlDbType.Int), ParameterDirection.Output);

      bool ok = StandardFetchIfExists("spMemberFetchLogin", ref spc);

      OiId = ok ? Convert.ToInt32(spc[2].Value) : 0;
      return ok;
    }


    /// <summary>
    /// Search on login/password
    /// </summary>
    /// <returns></returns>
    public void FetchIdFromLogin(string IsLogin,
      out int OiId)
    {
    	var spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IsLogin", SqlDbType.VarChar, 50), IsLogin);
      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), ParameterDirection.Output);


      StandardFetch("sp_MemberFetchIdFromLogin", ref spc);

      OiId = Convert.ToInt32(spc[1].Value);
    }

    //Search From Database
    public void FetchLastForumLogin(int IiMemberId,
      out DateTime OdLastForumLogin)
    {
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiMemberId);
      spc.Add(new SqlParameter("@OdLastForumLogin", SqlDbType.DateTime), ParameterDirection.Output);

      StandardFetch("spMemberFetchLastForumLogin", ref spc);

      OdLastForumLogin = DbConvert.ToDateTime(spc[1].Value);
    }




    /// <summary>
    /// Search on mail
    /// </summary>
    /// <returns></returns>
    public bool FetchPasswordFromMail(string IsMail,
      out string OsPassword)
    {
      bool bOk;
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IsMail", SqlDbType.VarChar, 100), IsMail);
      spc.Add(new SqlParameter("@OsPassword", SqlDbType.VarChar, 50), ParameterDirection.Output);

      bOk = StandardFetchIfExists("spMemberFetchPasswordFromMail", ref spc);

      OsPassword = bOk ? Convert.ToString(spc[1].Value) : String.Empty;

      return bOk;
    }


    //Update Database
    public void UpdateLastLogin(int IiId,
      DateTime IdLastLogin)
    {
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiId);
      spc.Add(new SqlParameter("@IdLastLogin", SqlDbType.DateTime), IdLastLogin);

      StandardUpdate("spMemberUpdateLastLogin", ref spc);
    }

    public void xxUpdateLastForumLogin(int IiId,
      DateTime IdLastForumLogin)
    {
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiId);
      spc.Add(new SqlParameter("@IdLastForumLogin", SqlDbType.DateTime), IdLastForumLogin);

      StandardUpdate("spMemberUpdateLastForumLogin", ref spc);
    }

      public void UpdateLastForumLogin(int IiId)
      {
          SqlParameterCollection spc = new SqlParameterCollection();

          spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiId);

          StandardUpdate("spMemberUpdateLastForumLogin", ref spc);
      }

    //Update Database
    public void Update(int IiId,
      string IsLogin,
      string IsName,
      string IsPhone,
      string IsMail,
      string IsPassword)
    {
      SqlParameterCollection spc = new SqlParameterCollection();

      spc.Add(new SqlParameter("@IiId", SqlDbType.Int), IiId);
      spc.Add(new SqlParameter("@IsLogin", SqlDbType.VarChar, 150), IsLogin);
      spc.Add(new SqlParameter("@IsName", SqlDbType.VarChar, 50), IsName);
      spc.Add(new SqlParameter("@IsPhone", SqlDbType.VarChar, 8), IsPhone);
      spc.Add(new SqlParameter("@IsMail", SqlDbType.VarChar, 100), IsMail);
      spc.Add(new SqlParameter("@IsPassword", SqlDbType.VarChar, 50), IsPassword);

      StandardUpdate("spMemberUpdate", ref spc);
    }


  }
}
