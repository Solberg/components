using System;
using System.Data;

namespace SSL.Data.SqlServer.Dbo
{
  /// <summary>
  /// Database class for the table Job.
  /// </summary>
  public class dboTask : SqlClient
  {

    #region Constructor
    /// <summary>
    /// Forbind med connectionstring angivet i parameter.
    /// </summary>
    /// <param name="IsConnectionString"></param>
    public dboTask(string IsConnectionString)
    {
      ConnectionString = IsConnectionString;
    }
    #endregion

    #region Methods
    public DataView List()
    {
      return StandardList("spTaskList");
    }

    /// <summary>
    /// Opdater medlem p� et job.
    /// </summary>
    /// <param name="IiJobId"></param>
    /// <param name="IiMemberId"></param>
    /// <returns></returns>
    public void UpdateMember(int IiJobId,
                             int IiMemberId)
    {
      var pt = new SqlParameterCollection
                 {
                   {new System.Data.SqlClient.SqlParameter("@IiJobId", SqlDbType.Int), IiJobId},
                   {new System.Data.SqlClient.SqlParameter("@IiMemberId", SqlDbType.Int), IiMemberId}
                 };

      StandardUpdate("spTaskUpdateMember", ref pt);
    }

    /// <summary>
    /// Opdater medlem p� et job.
    /// </summary>
    /// <param name="IiJobId"></param>
    /// <param name="IiActive"></param>
    /// <returns></returns>
    public void UpdateActive(int IiJobId,
                             int IiActive)
    {
      var pt = new SqlParameterCollection
                 {
                   {new System.Data.SqlClient.SqlParameter("@IiJobId", SqlDbType.Int), IiJobId},
                   {new System.Data.SqlClient.SqlParameter("@IiActive", SqlDbType.Int), IiActive}
                 };

      StandardUpdate("spTaskUpdateActive", ref pt);
    }


    /// <summary>
    /// Create record
    /// </summary>
    /// <param name="IsText"></param>
    /// <param name="IdWorkDate"></param>
    /// <param name="IiCreatedBy"></param>
    /// <returns></returns>
    /// <revision version="2.1" date="20081001" programmer="SSL">IiCreatedBy added</revision>
    public void Create(string IsText,
                       DateTime IdWorkDate,
                       int IiCreatedBy)
    {
      var pt = new SqlParameterCollection
                 {
                   {new System.Data.SqlClient.SqlParameter("@IsText", SqlDbType.VarChar, 100), IsText},
                   {new System.Data.SqlClient.SqlParameter("@IdWorkDate", SqlDbType.DateTime), IdWorkDate},
                   {new System.Data.SqlClient.SqlParameter("@IiCreatedBy", SqlDbType.Int), IiCreatedBy}
                 };

      StandardCreate("spTaskCreate", ref pt);

    }

    #endregion
  }
}