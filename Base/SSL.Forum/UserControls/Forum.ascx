<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Forum.ascx.cs" Inherits="SSL.Forum.UserControls.Forum" %>
<STYLE type="text/css">A:link { TEXT-DECORATION: none }
	A:visited { TEXT-DECORATION: none }
	A:active { TEXT-DECORATION: none }
	A:hover { TEXT-DECORATION: underline }
</STYLE>
		<script language="javascript" type="text/javascript">
if (top != self) top.location.href = location.href;
		</script>
		<script language="JavaScript" type="text/javascript">
		onerror = report;
var Selected = 1;


function OnOffPost(e)
{

   if ( !e ) e = window.event;
   var target = e.target ? e.target : e.srcElement;
   

if (!target) return;
   
 while (target.id.indexOf('LinkTrigger')<0)
 {
     target = target.parentNode;
     if (target.id ==null) return;
     }
  if ( target.id.indexOf('LinkTrigger')<0 )
   return;
   

   if (Selected)
   {
      var body = document.getElementById(Selected + "ON");
      if (body)
         body.style.display = 'none';
      var head = document.getElementById(Selected + "OFF");
      if (head)
         head.bgColor = '#EDF8F4';
   }

   if (Selected == target.name) // just collapse
      Selected="";
   else
   {
      Selected = target.name;
      var body = document.getElementById(Selected + "ON");
      if (body)
      {
         if (body.style.display=='none')
            body.style.display='';
         else
            body.style.display = 'none';
      }
      var head = document.getElementById(Selected + "OFF");
      if (head)
         head.bgColor = '#B7DFD5';

      if ( body && head && body.style.display != 'none' )
      {
         document.body.scrollTop = FindPosition(head, "Top") - document.body.clientHeight/10;
         OpenMessage(target.name, true);
      }
   }

   if ( e.preventDefault )
      e.preventDefault();
   else
      e.returnValue = false;
   return false;
}

// does its best to make a message visible on-screen (vs. scrolled off somewhere).
function OpenMessage(msgID, bShowTop) {
   var msgHeader = document.getElementById(msgID + "OFF");
   var msgBody = document.getElementById(msgID + "ON");

   // determine scroll position of top and bottom
   var MyBody = document.body;
   var top = FindPosition(msgHeader, 'Top');
   var bottom = FindPosition(msgBody, 'Top') + msgBody.offsetHeight;

   // if not already visible, scroll to make it so
   if ( MyBody.scrollTop > top && !bShowTop)
      MyBody.scrollTop = top - document.body.clientHeight/10;
   if ( MyBody.scrollTop+MyBody.clientHeight < bottom )
      MyBody.scrollTop = bottom-MyBody.clientHeight;
   if ( MyBody.scrollTop > top && bShowTop)
      MyBody.scrollTop = top - document.body.clientHeight/10;
}

// utility
function FindPosition(i,which)
{
   iPos = 0
   while (i!=null)
   {
      iPos += i["offset" + which];
      i = i.offsetParent;
   }
   return iPos
}

function report(message,url,line) {
    alert('Error : ' + message + ' at line ' + line + ' in ' + url);
}

		</script>
<table id="Forum" class="ContentArea" cellSpacing="0" cellPadding="0"  border="0">
	<tbody>
		<tr vAlign="top">
			<td class="ContentPane">
				<!-- Main Page Contents Start -->
				<DIV onclick="OnOffPost(event)">
					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
						<TBODY>
							<tr>
								<td width="100%">
										<table id="ForumTable" cellSpacing="1" cellPadding="0" width="100%" border="0">
											<TBODY>
												<tr>
													<td align="left">
														<table class="ForumHeader" border="0" width="100%" cellpadding="4" cellspacing="0">
															<tr>
																<td class="ForumLabel">Forum&nbsp;:&nbsp;
																  <asp:DropDownList ID="ddlForums" runat="server"  cssclass="ForumInput" OnSelectedIndexChanged="ddlForums_SelectedIndexChanged" AutoPostBack="true">
																    <asp:ListItem Value="1" Text="Forslag"></asp:ListItem>
																  </asp:DropDownList>
																</td>
																<td valign="middle" class="ForumLabel" style="Color:Black;text-align:right">Pr side&nbsp;:&nbsp;
																	<asp:DropDownList id="ddlPageSize" runat="server" cssclass="ForumInput" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" AutoPostBack="true">
																		<asp:ListItem Value="5">5</asp:ListItem>
																		<asp:ListItem Value="10">10</asp:ListItem>
																		<asp:ListItem Value="20" Selected="True">20</asp:ListItem>
																		<asp:ListItem Value="30">30</asp:ListItem>
																		<asp:ListItem Value="40">40</asp:ListItem>
																		<asp:ListItem Value="50">50</asp:ListItem>
																	</asp:DropDownList>
                               </td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="ForumHeader">
														<table cellPadding="2" width="100%" border="0">
															<tr>
																<td class="ForumLabel">
																   <IMG height="16" alt="screen" src="forum/images/forum_newmsg.gif" width="16" align="top" border="0">&nbsp;
																	  <asp:label id="lblnewmessage" ForeColor="Green" runat="server"></asp:label>
															  </td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="ForumBackground">
															<tr>
																<td>
																	<table cellSpacing="0" cellPadding="2" class="ForumHeader" border="0">
																		<tr style="height:22px">
																			<td width="100%" class="ForumLabel">Emne&nbsp;</td>
																			<td noWrap width="100" class="ForumLabel">Bruger&nbsp;</td>
																			<td noWrap width="100" class="ForumLabel">Dato&nbsp;</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
															  <td>
															    <div style="overflow-y:scroll;height:330px;width:500px;">
															      <table cellpadding="2" cellspacing="0" style="width:100%">
															        <asp:literal id="ltlPost" runat="server"></asp:literal>
															      </table>
														      </div>															    
															  </td>
															</tr>
														</table>

													</td>
												</tr>
												<tr class="ForumHeader">
													<td>
														<table cellpadding="0" width="100%" border="0">
															<tr>
																<td align="left" class="ForumLabel">
																  <asp:label id="lbldate" runat="server"></asp:label>
																</td>
																<td valign="middle" class="ForumLabel" align="right">
																  <table>
																    <tr>
																      <td>
                                        <asp:ImageButton runat="server" cssclass="ToolButton" ID="btnFirst" ImageUrl="Forum/Images/first.gif" OnClick="btnFirst_Click" />
																      </td>
																      <td>
  																      <asp:ImageButton runat="server" cssclass="ToolButton" ID="btnPrevious" ImageUrl="Forum/Images/Previous.gif" OnClick="btnPrevious_Click" />
																      </td>
																      <td>
																        <asp:ImageButton runat="server" cssclass="ToolButton" ID="btnNext" ImageUrl="Forum/Images/next.gif" OnClick="btnNext_Click" />                                
																      </td>
																      <td>
																        <asp:ImageButton runat="server" cssclass="ToolButton" ID="btnLast" ImageUrl="Forum/Images/last.gif" OnClick="btnLast_Click" />                                                                
																      </td>
																      <td style="padding-right:16px">
																        <asp:Label cssclass="ForumLabel" runat="server" ID="lblRecCount"></asp:Label>
																      </td>
																    </tr>
																  </table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</TBODY>
										</table>
								</td>
							</tr>
						</TBODY>
					</table>
				</DIV>
			</td>
		</tr>
	</tbody>
</table>


