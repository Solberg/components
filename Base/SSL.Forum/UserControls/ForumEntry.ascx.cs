using System;
using System.Data.SqlClient;
using System.Drawing;
using SSL.Data.SqlServer;

namespace SSL.Forum.UserControls
{
  public partial class ForumEntry : System.Web.UI.UserControl
  {

    #region Properties

    private int ArticleId
    {
      get
      {
        return Convert.ToInt32(ViewState["ArticleId"]);
      }

      set
      {
        ViewState["ArticleId"] = value;
      }
    }

    private int CommentId
    {
      get
      {
        return Convert.ToInt32(ViewState["CommentId"]);
      }

      set
      {
        ViewState["CommentId"] = value;
      }
    }

    private int Indent
    {
      get
      {
        return Convert.ToInt32(ViewState["Indent"]);
      }

      set
      {
        ViewState["Indent"] = value;
      }
    }


    public string UserName
    {
      set
      {
        txtname.Text = value;
        txtname.Enabled = false; /// If username is set from external source disable it.
      }
    }

    public int UserId
    {
      set
      {
        ViewState["UserId"] = value;
      }

      get
      {
        return ViewState["UserId"] != null ? Convert.ToInt32(ViewState["UserId"]) : 0;
      }
    }

    #endregion

    #region Events
    public event EventHandler OnNewEntry;
    #endregion

    #region Methods

    private void LoadComment()
    {
      CommentId = Convert.ToInt32(Request.QueryString["CID"]);

      SqlConnection myC = new SqlConnection();
      myC.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnForum"].ConnectionString;//"workstation id=RAJ;packet size=4096;integrated security=SSPI;data source=RAJ;persist security info=False;initial catalog=iLakshmi";
      string myQuery = "SELECT * FROM  CP_FORUM_Comments WHERE ID=" + CommentId + " and ArticleID=" + ArticleId;

      myC.Open();
      SqlCommand myCommand = new SqlCommand(myQuery, myC);

      SqlDataReader myReader = myCommand.ExecuteReader();
      try
      {
        if (myReader.HasRows)
        {
          while (myReader.Read())
          {
            lblname.Text = myReader["Username"].ToString();
            if (lblemail != null)
            {
              lblemail.Text = myReader["UserEmail"].ToString();
            }
            lbltitle.Text = myReader["Title"].ToString();
            txtsubject.Text = "Re: " + lbltitle.Text;
            lblComment.Text = myReader["Description"].ToString();
            lblDate.Text = myReader["DateAdded"].ToString();
            Indent = Convert.ToInt32(myReader["Indent"].ToString()) + 1;

          }
        }
      }
      finally
      {
        myReader.Close();
      }
    }

    #endregion

    #region Event-handlers

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      if (txtcomment.MaxLength > 0)
      {
        txtcomment.Attributes.Add("maxlength", txtcomment.MaxLength.ToString());
      }
    }

    protected override void OnLoad(EventArgs e)
    {
 	    base.OnLoad(e);

      if (!Page.IsPostBack)
      {
        if (Request.QueryString["id"] != null)
          ArticleId = Convert.ToInt32(Request.QueryString["id"]);
        else
          ArticleId = 1;

        if (Request.QueryString["Cid"] != null)
        {
          pnlComment.Visible = true;
          LoadComment();
        }
        else
        {
          pnlComment.Visible = false;
        }
        if (OnNewEntry != null)
        {
          OnNewEntry(this, e);
        }
      }
    }

    protected void btnPost_Click(object sender, EventArgs e)
    {
      try
      {
        if (Page.IsValid)
        {
          int mParentId = CommentId;
          int mArticleId = ArticleId;
          string mTitle;
          string mUserName;
          string mUserEmail;
          string mDescription;
          int mIndent = Indent;

          mTitle = txtsubject.Text;
          mUserName = txtname.Text;
          mUserEmail = txtemail != null ? txtemail.Text : String.Empty;
          mDescription = txtcomment.Text;
          string mProfile = txtProfile != null ? txtProfile.Text : String.Empty;
          int mCommentType;

          if (MsgType_1.Checked)
            mCommentType = 1;
          else if (MsgType_2.Checked)
            mCommentType = 2;
          else if (MsgType_3.Checked)
            mCommentType = 3;
          else if (MsgType_4.Checked)
            mCommentType = 4;
          else
            mCommentType = 5;

          SqlConnection myC = new SqlConnection();
          myC.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnForum"].ConnectionString;
          string sqlQuery = "INSERT into CP_FORUM_Comments(ParentId,ArticleId,Title,UserName,UserEmail,Description,Indent,UserProfile,CommentType,UserId) VALUES ('" + mParentId + "','" + mArticleId + "','" + mTitle + "','" + mUserName + "','" + mUserEmail + "','" + DbConvert.ToSqlString(mDescription) + "','" + mIndent + "','" + mProfile + "','" + mCommentType + "','" + UserId + "')";
          myC.Open();
          SqlCommand myCommand = new SqlCommand();
          myCommand.CommandText = sqlQuery;
          myCommand.Connection = myC;
          myCommand.ExecuteNonQuery();
          myC.Close();
          lblStatus.ForeColor = Color.Green;
          lblStatus.Text = "Status: Success";
          Response.Redirect("Forum.aspx?id=" + mArticleId);
        }
      }
      catch (Exception exc)
      {
        lblStatus.ForeColor = Color.Red;
        lblStatus.Text = "Error: " + exc.Message;
      }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
      Response.Redirect("Forum.aspx?id=" + ArticleId);
    }



    #endregion


  }
}