//*****************************************************************************************************************
//Class name : clsDataAccess 
//Defination : This class is the most important class as it does all the updating,Inserting,deleting,Retrieving
//part in the database.The methods in this class is called by all the other methods in other classes.Connection
//with the database is set up here only.We have used Granth SQL database.We added this class after our analysis
//phase.As you will go through the code we have not used any attributes in any class the only thing we have used is
//a query and nothing else. 
//Date Added : 11/11/05
//Please keep the comments if you distribute
//Author :        Quartz (Rajesh Lal - connectrajesh@hotmail.com)
//*****************************************************************************************************************

using System.Data;
using System.Data.SqlClient;

namespace SSL.Forum
{
	public class clsDataAccess // Class defination
	{
		SqlConnection mycon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnForum"].ConnectionString);  
 		
		public bool openConnection() // Opens database connection with Granth in SQL SERVER
		{
			if (mycon.State == ConnectionState.Closed)
			{
				mycon.Open();
			}

 		return true;
		}
		public void closeConnection() // Closes database connection with Granth in SQL SERVER
		{

		mycon.Close(); 
		mycon = null;
		}

		public SqlDataReader getForumData(int ArticleId) // Getdata from the table required(given in query)in datareader
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandType= CommandType.StoredProcedure;
			sqlCommand.CommandText = "CP_FORUM_ShowHierarchy";
			SqlParameter newSqlParam = new SqlParameter();
            newSqlParam.ParameterName = "@ArticleId";
            newSqlParam.SqlDbType = SqlDbType.Int ;
            newSqlParam.Direction = ParameterDirection.Input;  
			newSqlParam.Value = ArticleId;
            sqlCommand.Parameters.Add(newSqlParam);

			SqlParameter newSqlParam2 = new SqlParameter();
			newSqlParam2.ParameterName = "@Root";
			newSqlParam2.SqlDbType = SqlDbType.Int ;
			newSqlParam2.Direction = ParameterDirection.Input;  
			newSqlParam2.Value = 0;
			sqlCommand.Parameters.Add(newSqlParam2);
			sqlCommand.Connection=mycon;

			SqlDataReader myr = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
			return myr;
			 
		}
		public bool DeleteForumData(int ArticleId, int root) // Getdata from the table required(given in query)in datareader
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandType= CommandType.StoredProcedure;
			
			sqlCommand.CommandText = "CP_FORUM_DeleteHierarchy";
			SqlParameter newSqlParam = new SqlParameter();
			newSqlParam.ParameterName = "@ArticleId";
			newSqlParam.SqlDbType = SqlDbType.Int ;
			newSqlParam.Direction = ParameterDirection.Input;  
			newSqlParam.Value = ArticleId;
			sqlCommand.Parameters.Add(newSqlParam);

			SqlParameter newSqlParam2 = new SqlParameter();
			newSqlParam2.ParameterName = "@Root";
			newSqlParam2.SqlDbType = SqlDbType.Int ;
			newSqlParam2.Direction = ParameterDirection.Input;  
			newSqlParam2.Value = root;
			sqlCommand.Parameters.Add(newSqlParam2);
			sqlCommand.Connection=mycon;

			int i = sqlCommand.ExecuteNonQuery ();
			if (i == 0)
				return true;
			else
				return false;
			 
		}
	}
}
