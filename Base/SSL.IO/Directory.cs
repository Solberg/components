﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Directory.cs" company="">
//   
// </copyright>
// <summary>
//   The directory.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SSL.IO
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// The directory.
    /// </summary>
    public class Directory
    {
        #region Public Methods

        /// <summary>
        /// Determine if a directory or one of it's subdirectories contain
        /// a file which date is newer than Idt.
        /// </summary>
        /// <param name="rootDirectory">
        /// </param>
        /// <param name="dt">
        /// </param>
        /// <returns>
        /// The has newer item.
        /// </returns>
        public static bool HasNewerItem(string rootDirectory, DateTime dt)
        {
            if (String.IsNullOrEmpty(rootDirectory))
            {
                throw new Exception("IsPath is blank or null");
            }

            ArrayList directories = ALKA.IO.Directory.GetSubDirs(rootDirectory);

            return (from string dir in directories select new DirectoryInfo(dir)).Any(di => di.GetFiles("*.*").Any(fi => fi.LastWriteTime > dt));
        }

        /// <summary>
        /// Return a list of all files newer than Idt.
        /// </summary>
        /// <param name="rootDirectory">
        /// </param>
        /// <param name="dt">
        /// </param>
        /// <returns>
        /// </returns>
        public static StringCollection ListNewerFiles(string rootDirectory, DateTime dt)
        {
            if (String.IsNullOrEmpty(rootDirectory))
            {
                throw new Exception("IsPath is blank or null");
            }

            var aFiles = new StringCollection();

            ArrayList aDirectories = ALKA.IO.Directory.GetSubDirs(rootDirectory);

            foreach (string dir in aDirectories)
            {
                var di = new DirectoryInfo(dir);
                foreach (FileInfo fi in di.GetFiles("*.*"))
                {
                    if (fi.LastWriteTime > dt)
                    {
                        aFiles.Add(dir + "\\" + fi.Name);
                    }
                }
            }

            return aFiles;
        }

        #endregion
    }
}