﻿namespace SSL.IO.Domain
{
    using System;

    public class AlbumInfo
    {
        public string Title { get;private set; }
        public bool ShowInfo { get; private set; }
        public string FullPath { get; private set; }
        public DateTime? ReleaseDate { get; private set; }

        public AlbumInfo(string fullPath, string title, bool showInfo, DateTime? releaseDate)
        {
            this.FullPath = fullPath;
            this.Title = title;
            this.ShowInfo = showInfo;
            this.ReleaseDate = releaseDate;
        }

    }
}
