using System.Collections.Specialized;
using SSL.IO.Domain;

namespace SSL.IO
{
  public interface IImageDirectory
  {

    /// <summary>
    /// S�t directory der arbejdes i.
    /// </summary>
    /// <param name="folder"></param>
    void SetRootFolder(string folder);

    /// <summary>
    /// Get years
    /// </summary>
    /// <returns></returns>
    StringCollection ListYears();

    /// <summary>
    /// Get albums for a year
    /// </summary>
    /// <param name="year"></param>
    /// <returns></returns>
    StringCollection ListAlbums(string year);

    /// <summary>
    /// Hent subalbums til et hovedalbum.
    /// </summary>
    /// <param name="year"></param>
    /// <param name="album"></param>
    /// <returns></returns>
    StringCollection ListSubAlbums(string year,
      string album);

    StringCollection ListImages(string albumPath);

    /// <summary>
    /// Fetch info about the album.
    /// </summary>
    /// <param name="fullPath"></param>
    /// <param name="language"></param>
    /// <returns></returns>
    AlbumInfo GetAlbumInfo(
      string fullPath,
      string language);

    /// <summary>
    /// True if imageinfo from the imagefiles should be read.
    /// </summary>
    /// <param name="fullPath">Full physical path to directory.</param>
    /// <returns></returns>
    bool ShowImageInfo(string fullPath);

    string GetAlbumStamp(string albumPath,
      string language);

    /// <summary>
    /// Determine if a catalog was added within a specific period.
    /// </summary>
    /// <param name="fullAlbumPath"></param>
    /// <param name="numberOfDays">Number of days where album is considered new.</param>
    /// <returns></returns>
    bool IsNewAlbum(string fullAlbumPath,
      int numberOfDays);

    string GetAlbumDescription(string albumPath,
      string language);
  }
}