namespace SSL.IO
{
  using System;
  using System.IO;
  using System.Collections.Specialized;
  using System.Xml;
  using ALKA.Extensions;
  using Domain;

    public class ImageDirectory : IImageDirectory
    {
    #region Variables
    private string rootFolder;
  	private  string settingsFile;
    #endregion

    #region Constructors

      public ImageDirectory()
      {
        settingsFile = "Settings.xml";
      }
    
      public ImageDirectory(string rootFolder) : this()
    {
      this.rootFolder = rootFolder;
    }

	public ImageDirectory(string rootFolder,
		string settingsFile)
	{
		this.rootFolder = rootFolder;
		this.settingsFile = settingsFile;

	}

  	#endregion

    #region Private Methods
    private static StringCollection GetSubDirs(string directory)
    {
      var aDirectories = new StringCollection();

      try
      {
        var oRootDir = new DirectoryInfo(directory);

        foreach (DirectoryInfo sDir in oRootDir.GetDirectories())
        {
          aDirectories.Add(sDir.Name);
        }
      }
      catch(Exception)
      {
      }

      return aDirectories;
    }
    #endregion

    #region Methods

      public void SetRootFolder(string folder)
      {
        this.rootFolder = folder;
        this.settingsFile = "Settings.xml";
      }

      /// <summary>
    /// Get years
    /// </summary>
    /// <returns></returns>
    public StringCollection ListYears()
    {
      return GetSubDirs(this.rootFolder);
    }

    /// <summary>
    /// Get albums for a year
    /// </summary>
    /// <param name="year"></param>
    /// <returns></returns>
    public StringCollection ListAlbums(string year)
    {
      return GetSubDirs(String.Format(@"{0}\{1}", this.rootFolder, year));
    }

    /// <summary>
    /// Hent subalbums til et hovedalbum.
    /// </summary>
    /// <param name="year"></param>
    /// <param name="album"></param>
    /// <returns></returns>
    public StringCollection ListSubAlbums(string year,
      string album)
    {
      return GetSubDirs(String.Format(@"{0}\{1}\{2}", this.rootFolder, year, album));
    }

    public StringCollection ListImages(string albumPath)
    {
      var sc = new StringCollection();
      var di = new DirectoryInfo(albumPath);

      foreach (var fi in di.GetFiles("*.jpg"))
      {
        sc.Add(fi.Name);
      }
      return sc;
    }

    /// <summary>
    /// Fetch info about the album.
    /// </summary>
    /// <param name="fullPath"></param>
    /// <param name="language"></param>
    /// <returns></returns>
    public AlbumInfo GetAlbumInfo(
      string fullPath,
      string language)
    {
      string filename = string.Format(@"{0}\{1}", fullPath, this.settingsFile);
        bool showImageInfo;
        string title;
        DateTime? releaseDate = null;

      if (File.Exists(filename))
      {
        var doc = new XmlDocument();
        doc.Load(filename);
        XmlNode node = doc.SelectSingleNode("settings/setup/imagefiles/showinfo");

        showImageInfo = (node != null) && (node.InnerText == "1");

        node = doc.SelectSingleNode(String.Format("settings/setup/album/title/{0}", string.IsNullOrEmpty(language) ? "dk" : language));

        title = node != null ? node.InnerText : fullPath.Substring(fullPath.LastIndexOf(@"\") + 1);

        node = doc.SelectSingleNode("settings/setup/releasedate");

        if (node != null)
        {
            releaseDate = DateTime.ParseExact(node.InnerText, "yyyyMMdd", null);             
        }
      }
      else
      {
        title = fullPath.Substring(fullPath.LastIndexOf(@"\") + 1);
        showImageInfo = false;
      }

      return new AlbumInfo(fullPath, title, showImageInfo, releaseDate);
    }



    /// <summary>
    /// True if imageinfo from the imagefiles should be read.
    /// </summary>
    /// <param name="fullPath">Full physical path to folder.</param>
    /// <returns></returns>
    public bool ShowImageInfo(string fullPath)
    {
    	bool bOk;
      string sFileName = String.Format(@"{0}\{1}",fullPath, this.settingsFile);

      if (File.Exists(sFileName))
      {
        var doc = new XmlDocument();
        doc.Load(sFileName);
        var node = doc.SelectSingleNode("settings/setup/imagefiles/showinfo");
        if (node != null)
        {
          bOk = (node.InnerText == "1");
        }
        else
        {
          bOk = false;
        }
      }
      else
      {
        bOk = false;
      }
    	return bOk;
    }


    public string GetAlbumStamp(string albumPath,
      string language)
    {
      string sFileName = String.Format(@"{0}\{1}\{2}", this.rootFolder, albumPath, this.settingsFile);

      if (File.Exists(sFileName))
      {
        var doc = new XmlDocument();
        doc.Load(sFileName);
        XmlNode node = doc.SelectSingleNode("settings/setup/releasedate");

        return node != null ? node.InnerText : String.Empty;
      }
      return String.Empty;
    }

    /// <summary>
    /// Determine if a catalog was added within a specific period.
    /// </summary>
    /// <param name="fullAlbumPath"></param>
    /// <param name="numberOfDays">Number of days where album is considered new.</param>
    /// <returns></returns>
    public bool IsNewAlbum(string fullAlbumPath,
      int numberOfDays)
    {
      string sFileName = String.Format(@"{0}\{1}",  fullAlbumPath, this.settingsFile);

      if (File.Exists(sFileName))
      {
        var doc = new XmlDocument();
        doc.Load(sFileName);
        var node = doc.SelectSingleNode("settings/setup/releasedate");

        return node != null
          ? String.Compare(node.InnerText, DateTime.Now.AddDays(- numberOfDays).IsoDate()) > 0 : false;
      }
      return false;
    }

    public string GetAlbumDescription(string albumPath,
      string language)
    {
      string sFileName = String.Format(@"{0}\{1}\{2}", this.rootFolder, albumPath, this.settingsFile);

      if (File.Exists(sFileName))
      {
        var doc = new XmlDocument();
        doc.Load(sFileName);
        return doc.SelectSingleNode(String.Format("settings/description/text/{0}", language)).InnerText;
      }
      return albumPath.Substring(albumPath.LastIndexOf(@"\") + 1);
    }

    #endregion

  }
}
