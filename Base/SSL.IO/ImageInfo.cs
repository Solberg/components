using System.Text;

namespace SSL.IO
{
  using System.Drawing;
  using System.Drawing.Imaging;

  /// <summary>
  /// Get information about a jpeg image
  /// </summary>
  public class ImageInfo
  {

    #region Properties

    public string Title { get; set; }

    public string Author { get; set; }

    public string Subject { get; set; }

    public string KeyWords { get; set; }

    public string Comments { get; set; }

    #endregion

    #region Constructors

    public ImageInfo(string imagePath)
    {
      // Create an Image object. 
      Image theImage = new Bitmap(imagePath);

      // Get the PropertyItems property from image.
      PropertyItem[] propItems = theImage.PropertyItems;

      foreach (PropertyItem propItem in propItems)
      {
        switch (propItem.Id)
        {
          case 40091: Title = Encoding.Unicode.GetString(propItem.Value);
            break;
          case 40092: Comments = Encoding.Unicode.GetString(propItem.Value);
            break;
          case 40093: Author = Encoding.Unicode.GetString(propItem.Value);
            break;
          case 40094: KeyWords = Encoding.Unicode.GetString(propItem.Value);
            break;
          case 40095: Subject = Encoding.Unicode.GetString(propItem.Value);
            break;
        }
      }
    }
    #endregion
  }
}
