﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Moq;
using NUnit.Framework;
using Ssl.Logging;
using SSL.Web.Logging;

namespace SSL.Web.Tests.Logging
{

  [TestFixture]
  public class WebExceptionFilterTests
  {

    [Test]
    public void be_able_to_filter_on_browser()
    {

      var filterProviderMock = new Mock<IFilterProvider>();

      filterProviderMock.Setup(c => c.List()).Returns(new List<FilterEntry> {new FilterEntry(FilterType.Browser, @"http:\/\/www\.bing\.com\/bingbot\.htm")});
      
     var result = new WebExceptionFilter(filterProviderMock.Object).Filter(new WebException("Test",
        "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)", "127.0.0.1"));

      Assert.IsNull(result);

    }

    [Test]
    public void be_able_to_filter_on_ip()
    {

      var filterProviderMock = new Mock<IFilterProvider>();

      filterProviderMock.Setup(c => c.List()).Returns(new List<FilterEntry> { new FilterEntry(FilterType.Ip, "127.0.0.1") });

      var result = new WebExceptionFilter(filterProviderMock.Object).Filter(new WebException("Test",
         "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)", "127.0.0.1"));

      Assert.IsNull(result);

    }

    [Test]
    public void be_able_to_filter_on_exception()
    {

      var filterProviderMock = new Mock<IFilterProvider>();

      filterProviderMock.Setup(c => c.List()).Returns(new List<FilterEntry> { new FilterEntry(FilterType.ClassName, "System.ArgumentException") });

      var result = new WebExceptionFilter(filterProviderMock.Object).Filter(new WebException("Test",
         "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)", "127.0.0.1", new ArgumentException("Test")));

      Assert.IsNull(result);

    }



  }
}
