﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using SSL.Domain;
using SSL.Domain.Services;

namespace SSL.Web.Logging
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILogService" in both code and config file together.
  [ServiceContract]
  public interface ILogService
  {

    /// <summary>
    /// Get a log entry
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json,
      RequestFormat = WebMessageFormat.Json, UriTemplate = "GetEntry")]
    ServiceResponse<WebExceptionEntry> GetEntry(IdRequest<int> request);

    /// <summary>
    /// Delete log entry
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json,
      RequestFormat = WebMessageFormat.Json, UriTemplate = "DeleteEntry")]
    ServiceResponse<bool> DeleteEntry(IdRequest<int> request);

    /// <summary>
    /// Delete log entry
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json,
      RequestFormat = WebMessageFormat.Json, UriTemplate = "ListEntries")]
    ServiceResponse<List<WebExceptionEntry>> ListEntries(IdRequest<int> request);

  }
}
