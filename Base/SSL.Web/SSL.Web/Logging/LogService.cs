﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using SSL.Domain;
using SSL.Domain.Services;

namespace SSL.Web.Logging
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LogService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select LogService.svc or LogService.svc.cs at the Solution Explorer and start debugging.
  [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
  public class LogService : ILogService
  {

    #region Variables

    private readonly ILogger<WebExceptionEntry> _logger;

    private readonly ITicketFactory _ticketFactory;

    public LogService(ILogger<WebExceptionEntry> logger, ITicketFactory ticketFactory)
    {
      _logger = logger;
      _ticketFactory = ticketFactory;
    }

    #endregion


    public ServiceResponse<List<WebExceptionEntry>> ListEntries(IdRequest<int> request)
    {
      //if (request.Ticket != _ticketFactory.GetTicket())
      //{
      //  return new ServiceResponse<List<WebExceptionEntry>>(null, 401, "Access denied");
      //}

      try
      {
        var entry = _logger.List(DateTime.MinValue, DateTime.MaxValue);

        return new ServiceResponse<List<WebExceptionEntry>>(entry, entry == null ? 1 : 0, entry == null ? string.Format("Entry with id {0} not found", request.Id) : string.Empty);
      }
      catch (Exception exc)
      {
        _logger.Log(new WebExceptionEntry(exc.Message, exc.StackTrace, DateTime.Now, null, null, null, null));
        return new ServiceResponse<List<WebExceptionEntry>>(null, 1, exc.Message);
      }
      
    }

    public ServiceResponse<WebExceptionEntry> GetEntry(IdRequest<int> request)
    {
      if (request.Ticket != _ticketFactory.GetTicket())
      {
        return new ServiceResponse<WebExceptionEntry>(null, 401, "Access denied");
      }

      try
      {
        var entry = _logger.Fetch(request.Id);

        return new ServiceResponse<WebExceptionEntry>(entry, entry == null ? 1 : 0, entry == null ? string.Format("Entry with id {0} not found", request.Id) : string.Empty);
      }
      catch (Exception exc)
      {
        _logger.Log(new WebExceptionEntry(exc.Message, exc.StackTrace, DateTime.Now, null, null, null, null));
        return new ServiceResponse<WebExceptionEntry>(null, 1, exc.Message);
      }
    }


    /// <summary>
    /// Delete an entry from the log
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    public ServiceResponse<bool> DeleteEntry(IdRequest<int> request)
    {
      if (request.Ticket != _ticketFactory.GetTicket())
      {
        return new ServiceResponse<bool>(false, 401, "Access denied");
      }

      try
      {
       _logger.Delete(request.Id);

        return new ServiceResponse<bool>(true, 0);
      }
      catch (Exception exc)
      {
        _logger.Log(new WebExceptionEntry(exc.Message, exc.StackTrace, DateTime.Now, null, null, null, null));
        return new ServiceResponse<bool>(false, 1, exc.Message);
      }
    }
  }
}
