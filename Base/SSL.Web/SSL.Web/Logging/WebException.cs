﻿using System;

namespace SSL.Web.Logging
{
  public class WebException : Exception
  {

    #region Properties

    public string Browser { get; private set; }

    public string Ip { get; private set; }

    #endregion


    public WebException(string message, string browser, string ip, Exception inner = null) : base(message, inner)
    {
      Browser = browser;
      Ip = ip;
    }

  }
}
