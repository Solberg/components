﻿using System;
using System.Runtime.Serialization;
using Ssl.Logging;

namespace SSL.Web.Logging
{

    [DataContract]
    public class WebExceptionEntry : ExceptionEntry
    {

        [DataMember]
        public string Browser { get; private set; }

        [DataMember]
        public string QueryString { get; private set; }

        [DataMember]
        public string Url { get; private set; }

        [DataMember]
        public string Ip { get; private set; }


        public WebExceptionEntry(int id,string message, string stacktrace, DateTime? timestamp, string browser, string queryString, string url, string ip) 
          : base(id, message, stacktrace, timestamp)
        {
          Url = url;
          Browser = browser;
          QueryString = queryString;
          Ip = ip;
        }

        public WebExceptionEntry(string message, string stacktrace, DateTime? timestamp, string browser, string queryString, string url, string ip)
          : base(message, stacktrace, timestamp)
        {
          Url = url;
          Browser = browser;
          QueryString = queryString;
          Ip = ip;
        }

    }
}
