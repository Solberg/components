﻿using System;
using System.Xml.Linq;
using SSL.Extensions;
using Ssl.Logging;

namespace SSL.Web.Logging
{
  public class WebExceptionEntryMapper : IEntryMapper<WebExceptionEntry>
  {
    public WebExceptionEntry Map(XElement element)
    {
      return new WebExceptionEntry(element.Element("Id").ToInt32(),
                element.Element("Message").ToStr(),
                element.Element("Stacktrace").ToStr(),
                element.Element("Timestamp").ToNullableDateTime(),
                element.Element("Browser").ToStr(),
                element.Element("QueryString").ToStr(),
                element.Element("Url").ToStr(),
                element.Element("Ip").ToStr()
                );
    }

    public XElement Map(WebExceptionEntry entry)
    {
      return new XElement("Entry",
        new XElement("Id", entry.Id),
        new XElement("Message", entry.Message),
        new XElement("Stacktrace", entry.Stacktrace),
        new XElement("Timestamp",  entry.Timestamp == null ? DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") : ((DateTime)entry.Timestamp).ToString("dd.MM.yyyy HH:mm:ss")),
        new XElement("Browser", entry.Browser),
        new XElement("QueryString", entry.QueryString),
        new XElement("Url", entry.Url),
        new XElement("Ip", entry.Ip)
        );
    }
  }
}
