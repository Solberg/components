﻿using System.Linq;
using System.Text.RegularExpressions;
using Ssl.Logging;

namespace SSL.Web.Logging
{
  public class WebExceptionFilter : ExceptionFilter
  {
    public WebExceptionFilter(IFilterProvider filterProvider) : base(filterProvider)
    {


    }

    public WebException Filter(WebException item)
    {
      if (base.Filter(item.InnerException) == null)
      {
        return null;
      }

      
      // Match on IP
      if (Filters.Any(c => c.FilterType == FilterType.Ip && c.FilterText == item.Ip))
      {
        return null;
      }

      // Match browser to regular expression
      if (Filters.Any(c => c.FilterType == FilterType.Browser && new Regex(c.FilterText, RegexOptions.IgnoreCase).IsMatch(item.Browser)))
      {
        return null;
      }
    

      return item;
    }
  }
}
