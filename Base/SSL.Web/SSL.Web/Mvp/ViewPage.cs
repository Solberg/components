﻿using System;
using System.Web.UI;
using SSL.Mvp;

namespace SSL.Web.Mvp
{
  /// <summary>
  /// Side der understøtter mvp model.
  /// </summary>
  public class ViewPage<TPresenter> : Page, IView where TPresenter : class, IPresenter
  {

    #region Variables

    /// <summary>
    /// Presenter instans
    /// </summary>
    protected TPresenter Presenter { get; set; }

    #endregion

    #region Protected

    /// <summary>
    /// Opsæt presenter
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {

      Presenter = PresenterFactory.Current.Create<TPresenter>(this);

      base.OnInit(e);
    }

    /// <summary>
    /// Kør onload på presenter hvis denne er sat.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
      if (Presenter != null)
      {
        Presenter.OnLoad(! IsPostBack);
      }

      base.OnLoad(e);        
    }

    #endregion

  }

}
