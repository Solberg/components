﻿using System.Web;

namespace SSL.Web
{
  public class PathMapper : IPathMapper
  {

    public PathMapper()
    {
    }


    public string MapPath(string path)
    {
      return HttpContext.Current.Server.MapPath(path);
    }


  }
}