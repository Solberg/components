﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using SSL.State;

namespace SSL.Web
{
  public class State : IState
  {

    public object Get(string key)
    {
       return HttpContext.Current.Session[key];
    }

    public void Set(string key, object o)
    {
      HttpContext.Current.Session[key] = o;
    }

  }
}
