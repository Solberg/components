using System;
using System.Web;
using System.Data;

namespace SSL.Web
{
	/// <summary>
	/// XML SystemErrorLog
	/// </summary>
	public class SystemErrorLog : ALKA.Web.SystemErrorLog
	{


    #region Variables
    private string _QueryString = String.Empty;
    private string _ExceptionType;
    #endregion

    #region Properties

    /// <summary>
    /// QueryString
    /// </summary>
    protected virtual string QueryString
    {
      get { return _QueryString; }
      set { _QueryString = value; }
    }


    public string ExceptionType
    {
      get { return _ExceptionType; }
      set { _ExceptionType = value; }
    }
	
    #endregion

    #region Methods

    /// <summary>
    /// Tilf�j felter til DataRow ved Add kommando.
    /// </summary>
    /// <param name="dr"></param>
    /// <param name="IsMessage"></param>
    protected override void AddFields(DataRow dr, string IsMessage)
    {
      base.AddFields(dr, IsMessage);

      dr["QueryString"]   = this.QueryString;
      dr["ExceptionType"] = _ExceptionType.ToString();
    }

    /// <summary>
    /// Skriv fejlmeddelelse til log.
    /// </summary>
    /// <param name="IsMessage"></param>
    /// <param name="IsStackTrace"></param>
    public int Write(string IsMessage,
      string IsStackTrace,
      string IsQueryString,
      string IsExceptionType)
    {
      _ExceptionType = IsExceptionType;
      _QueryString = IsQueryString;
      return Write(IsMessage, IsStackTrace);
    }

    #endregion


  }
}
