using System;
using System.Web;


namespace SSL.Web.UI
{
	/// <summary>
	/// Summary description for Basepage.
	/// </summary>
	public class BasePage : ALKA.Web.UI.BasePage
	{
		#region Const
		private const string AUTHORIZED = "Authorized";
		#endregion

		#region Properties

		/// <summary>
		/// Get cookie for response.
		/// </summary>
		protected HttpCookie ResponseCookie
		{
			get
			{ 
				HttpCookie cook = Request.Cookies[PageCookieName] ?? new HttpCookie(PageCookieName);
				cook.Expires = DateTime.Now.AddDays(90);
				return cook;
			}
		}

		/// <summary>
		/// Accesscontrol saved in a session variable.
		/// </summary>
		protected bool AccessAllowed
		{
			get 
			{
				return Session[AUTHORIZED] == null ? false : Convert.ToBoolean(Session[AUTHORIZED]);
			}
			set 
			{
					if (Session[AUTHORIZED] == null)
					 Session.Add(AUTHORIZED, value);
				 else
					 Session[AUTHORIZED] = value;
			}
		}
		#endregion

		#region Methods

    /// <summary>
    /// Write error to systemlog.
    /// </summary>
    public void LogSystemError()
    {
      LogSystemError(Server.GetLastError());
    }

    public void LogSystemError(Exception exc)
    {
      if (exc != null)
      {
        if (exc is HttpUnhandledException)
        {
          exc = exc.InnerException;
        }

        var sel = new SystemErrorLog();

        sel.Write(exc.Message, exc.StackTrace, Request.QueryString.ToString(), exc.ToString());
      }
    }
    
    public void ShowSystemError(Exception exc,
			bool IbAdviceClient)
		{
			var oLog = new SystemErrorLog();

			oLog.Write(exc.Message, exc.StackTrace, Request.QueryString.ToString(), exc.ToString());

			if (IbAdviceClient)
			{
        ShowClientMessage(exc.Message);
			}
		}


    public void ShowClientMessage(string IsMessage)
    {
      if (!Page.ClientScript.IsStartupScriptRegistered("ClientMessage"))
      {
        string sJs = string.Format("alert(\"{0}\");\n", IsMessage.Replace("\r\n", ""));

        Page.ClientScript.RegisterStartupScript(GetType(), "ClientMessage", sJs, true);
      }
    }


    protected static void PageNotFound()
    {
      throw new HttpException(404, "Page not found");
    }


		#endregion

		#region Event-handlers
		/// <summary>
		/// Don't call the inherited OnPreRender because it holds a lot
		/// off Company related code.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
      base.OnPreRender(e);

			if (Request.Browser.Browser == "Netscape" ||
				Request.Browser.Browser == "Firefox")
			{
				if (! Page.ClientScript.IsClientScriptBlockRegistered("GeckoCSS"))
					Page.ClientScript.RegisterClientScriptBlock(GetType(), "GeckoCSS","<LINK href=\"/stylesheets/gecko.css\" type=\"text/css\" rel=\"stylesheet\">");
			}

			RegisterRefreshCheck();
		}
		#endregion
	}
}
