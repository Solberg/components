using System;

namespace SSL.Web.UI
{
	/// <summary>
	/// Summary description for SecuredPage.
	/// </summary>
	public interface ISecuredPage
	{

		/// <summary>
		/// Check User/Password. 
		/// </summary>
		/// <param name="IsLogin"></param>
		/// <param name="IsPassword"></param>
		/// <returns></returns>
		bool UserPasswordOk(string IsLogin,string IsPassword);
	}
}
