using System;
using System.Web;
using ALKA.Web;


namespace SSL.Web.UI
{
	/// <summary>
	/// Summary description for PrivatePage.
	/// </summary>
	public class PrivatePage : BasePage, ISecuredPage
	{

		#region Properties
		protected virtual string LoginPage
		{
		  get{return String.Empty;}
		}
		#endregion

		#region ISecuredPage
		public virtual bool UserPasswordOk(string user,
			string password)
		{
			return false;
		}
		#endregion

		#region Methods
		/// <summary>
		/// Load cookie and check securityinformation.
		/// </summary>
		private void LoadCookie()
		{
			HttpCookie cook = Request.Cookies[PageCookieName];

			if (cook != null)
			{
				AccessAllowed = UserPasswordOk(Cookie.GetCookieValue(cook,"User"),
					Cookie.GetCookieValue(cook,"Password"));
			}
		}


		/// <summary>
		/// Check security when page is loaded and redirect to
		/// login page if user is not authorized.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (! Page.IsPostBack)
			{
				LoadCookie();
				if (! AccessAllowed)
				{
					Session["PasswordRequester"] = Page.Request.RawUrl;
					Response.Redirect(LoginPage, false);
				}
			}
		}
		#endregion
	}
}
