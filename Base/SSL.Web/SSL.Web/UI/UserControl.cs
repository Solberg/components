using System;
using System.Web;

namespace SSL.Web.UI
{
  public class UserControl : System.Web.UI.UserControl
  {
    #region Properties
    protected bool IsRefresh
    {
      get { return ViewState["RefreshGuid"] != null 
                   &&  HttpContext.Current.Session["RefreshGuid"].ToString() != ViewState["RefreshGuid"].ToString(); }
    }
    #endregion

    #region Event-handlers
    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
      HttpContext.Current.Session["RefreshGuid"] = Guid.NewGuid().ToString();

      ViewState["RefreshGuid"] = HttpContext.Current.Session["RefreshGuid"];
    }
    #endregion


  }
}