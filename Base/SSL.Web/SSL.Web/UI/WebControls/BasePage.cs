using System;
using System.Web;
using System.Collections.Specialized;
using ALKA.Exceptions;


namespace SSL.Web.UI
{
	/// <summary>
	/// Summary description for Basepage.
	/// </summary>
	public class BasePage : ALKA.Web.UI.BasePage
	{
		#region Const
		private const string AUTHORIZED = "Authorized";
		#endregion

		#region Properties

		/// <summary>
		/// Get cookie for response.
		/// </summary>
		protected HttpCookie ResponseCookie
		{
			get
			{ 
				HttpCookie cook = Request.Cookies[this.PageCookieName] != null ? Request.Cookies[this.PageCookieName] : new HttpCookie(this.PageCookieName);
				cook.Expires = DateTime.Now.AddDays(90);
				return cook;
			}
		}

		/// <summary>
		/// Accesscontrol saved in a session variable.
		/// </summary>
		protected bool AccessAllowed
		{
			get 
			{
				return Session[AUTHORIZED] == null ? false : Convert.ToBoolean(Session[AUTHORIZED]);
			}
			set 
			{
					if (Session[AUTHORIZED] == null)
					 Session.Add(AUTHORIZED, value);
				 else
					 Session[AUTHORIZED] = value;
			}
		}
		#endregion

		#region Methods

    /// <summary>
    /// Write error to systemlog.
    /// </summary>
    public void LogSystemError()
    {
      LogSystemError(Server.GetLastError());
    }

    public void LogSystemError(Exception exc)
    {
      if (exc != null)
      {
        if (exc is HttpUnhandledException)
        {
          exc = exc.InnerException;
        }

        SystemErrorLog sel = new SystemErrorLog();

        sel.Write(exc.Message, exc.StackTrace, Request.QueryString.ToString());
      }
    }
    
    public void ShowSystemError(Exception exc,
			bool IbAdviceClient)
		{
			SystemErrorLog oLog = new SystemErrorLog();

			oLog.Write(exc.Message, exc.StackTrace, Request.QueryString.ToString());

			if (IbAdviceClient)
			{
				if (! Page.ClientScript.IsStartupScriptRegistered("SystemError"))
				{
					string sJs = "<script language=javascript>\n"
						+ string.Format("alert(\"{0}\");\n", exc.Message.Replace("\r\n",""))
						+ "</script>";

					Page.ClientScript.RegisterStartupScript(this.GetType(), "SystemError",sJs);
				}
			}
		}

		/// <summary>
		/// Log an error to xml log and show error in a javascript alert().
		/// </summary>
		/// <param name="exc"></param>
		public override void ShowSystemError(Exception exc)
		{
			ShowSystemError(exc, true);
		}

		#endregion

		#region Event-handlers
		/// <summary>
		/// Don't call the inherited OnPreRender because it holds a lot
		/// off Company related code.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPreRender(EventArgs e)
		{
			if (Request.Browser.Browser == "Netscape" ||
				Request.Browser.Browser == "Firefox")
			{
				if (! Page.ClientScript.IsClientScriptBlockRegistered("GeckoCSS"))
					Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GeckoCSS","<LINK href=\"/stylesheets/gecko.css\" type=\"text/css\" rel=\"stylesheet\">");
			}

			RegisterRefreshCheck();
		}
		#endregion
	}
}
