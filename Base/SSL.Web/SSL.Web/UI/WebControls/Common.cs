using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
  /// <summary>
  /// Common functions for use with webcontrols.
  /// </summary>
  class Common
  {
    /// <summary>
    /// Write a transparant image with dimensions 1 x 1.
    /// </summary>
    /// <param name="writer"></param>
    public static void SpacerImage(HtmlTextWriter writer)
    {
      writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "1");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "1");
      writer.AddAttribute(HtmlTextWriterAttribute.Src, "/images/standard/dummy.gif");
      writer.RenderBeginTag(HtmlTextWriterTag.Img);
      writer.RenderEndTag(); /// Img
    }
  }
}
