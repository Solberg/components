﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
    public class CompareValidator : System.Web.UI.WebControls.CompareValidator, IValidator
    {
        #region Variables
        ValidatorPresenter _presenter;
        #endregion

        #region protected
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _presenter = new ValidatorPresenter(this);
        }
        #endregion

        /// <summary>
        /// Før kontrol udskrives indsættes meddelelse i billede og fejlmarkering af tilknyttet kontrol
        /// foretages.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            _presenter.PreRender();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            _presenter.Render(writer);
        }

        public void BaseRender(HtmlTextWriter writer)
        {
            base.Render(writer);
        }

    }
}
