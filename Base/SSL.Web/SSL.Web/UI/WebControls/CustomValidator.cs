namespace SSL.Web.UI.WebControls
{
  /// <summary>
  /// Udvidet CustomValidator, der markerer fejl med gif fil.
  /// </summary>
  /// <remarks>
  ///<programmer version="2.0" date="20071220" programmer="SSL"></programmer>
  /// </remarks>
  public class CustomValidator : System.Web.UI.WebControls.CustomValidator
  {
    #region Event-handlers
    /// <summary>
    /// F�r kontrol udskrives inds�ttes meddelelse i billede.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(System.EventArgs e)
    {
      base.OnPreRender(e);

      if (!ErrorMessage.Contains("<img src="))
      {
        ErrorMessage = string.Format("<img src=\"/images/btn/error.gif\" alt=\"{0}\" />", ErrorMessage);
      }
    }
    #endregion

  }
}
