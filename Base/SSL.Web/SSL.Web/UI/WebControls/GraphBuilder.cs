using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.UI;
using DavidDao.Charting;

namespace SSL.Web.UI.WebControls
{
  public enum GraphType 
  {
    Bar,
    Line,
    Pie
  }

  public enum GraphTarget
  {
    File,
    Image
  }

  /// <summary>
	/// Component build upon Chart component.
	/// </summary>
	public class GraphBuilder : System.Web.UI.WebControls.WebControl
	{

    #region Variables
    private GraphType _GraphType     = GraphType.Bar;
    private GraphTarget _GraphTarget = GraphTarget.Image;

    private GraphFieldCollection _Fields = new GraphFieldCollection();
    private object _DataSource = null;
    private AbstractChart Chart = null;
    private string _Title;
    private string _HorizontalTitle;
    private string _VerticalTitle;
    private string _ImageUrl;
    private byte[] _ImageGraphField;
    private DavidDao.Charting.Orientation _Orientation;
    private Color _GraphColor = Color.Green;
    #endregion

    #region Properties
    /// <summary>
    /// Property which type of graph to show.
    /// </summary>
    public GraphType GraphType
    {
      get {return _GraphType;}
      set {_GraphType = value;}
    }

    /// <summary>
    /// How Graph should be saved (File or Image)
    /// </summary>
    public GraphTarget GraphTarget
    {
      get {return _GraphTarget;}
      set {_GraphTarget = value;}
    }

    public object DataSource
    {
      get {return _DataSource;}
      set {_DataSource = value;}
    }

    /// <summary>
    /// Datatyper for de enkelte felter i datasource.
    /// </summary>
    public GraphFieldCollection Fields
    {
      get {return _Fields;}
      set {_Fields = value;}
    }

    public string Title
    {
      get{return _Title;}
      set{_Title = value;}
    }

    public string HorizontalTitle
    {
      get{return _HorizontalTitle;}
      set{_HorizontalTitle = value;}
    }

    public string VerticalTitle
    {
      get{return _VerticalTitle;}
      set{_VerticalTitle = value;}
    }


    public string ImageUrl
    {
      get{return _ImageUrl;}
    }


    public byte[] ImageGraphField
    {
      get{return _ImageGraphField;}
    }


    public Color GraphColor
    {
      get
      {
        return _GraphColor;
      }

      set
      {
        _GraphColor = value;
      }
    }

    /// <summary>
    /// Graph orientation Vertical/horizontal.
    /// </summary>
    [DefaultValue(DavidDao.Charting.Orientation.Vertical)]
    public DavidDao.Charting.Orientation Orientation
    {
      get{return _Orientation;}
      set{_Orientation = value;}
    }
    #endregion

    #region Methods

    private Color ColorFromInt(int i)
    {
      switch (i)
      {
        case 0 : return Color.Red;
        case 1 : return Color.Yellow;
        case 2 : return Color.Green;
        case 3 : return Color.Blue;
        case 4 : return Color.White;
        case 5 : return Color.Orange;
        case 6 : return Color.Gray;
        case 7 : return Color.LightGray;
        case 8 : return Color.Turquoise;
        case 9 : return Color.Azure;
        case 10 : return Color.RosyBrown;
        default : return Color.Silver;
      }
      
    }

    private void GraphToGraphField(AbstractChart Ichart)
    {
      Bitmap bitmap = new Bitmap(Ichart.Width, Ichart.Height, PixelFormat.Format32bppArgb);

      Graphics g = Graphics.FromImage(bitmap);

      Ichart.draw(g);

      MemoryStream stream = new MemoryStream();

      bitmap.Save(stream, ImageFormat.Png);

      _ImageGraphField = stream.ToArray();
      
      g.Dispose();
    }
    

    private void GraphToFile(AbstractChart Ichart, string IsFileName)
    {
        Bitmap bitmap = new Bitmap(Ichart.Width, Ichart.Height, PixelFormat.Format32bppArgb);

        Graphics g = Graphics.FromImage(bitmap);

        Ichart.draw(g);

        System.IO.FileStream stream = new FileStream(IsFileName,FileMode.Create);

        bitmap.Save(stream, ImageFormat.Png);

        stream.Close();

        g.Dispose();
    }



    /// <summary>
    /// Get a tempfilename. Mind that the key TempFilePath i required in the web.config file 
    /// for the application and that a Setup.xml should exist in the application directory.
    /// </summary>
    /// <returns>Full path to a temp-file.</returns>
    private string GetTempFileName()
    {
      HttpApplication currApp = HttpContext.Current.ApplicationInstance;
      string sGuid = System.Guid.NewGuid().ToString();

      string sTempFilePath = System.Configuration.ConfigurationManager.AppSettings["TempFilePath"];

      _ImageUrl = sTempFilePath + String.Format("\\Tmp{0}.png", sGuid);

      return currApp.Server.MapPath(sTempFilePath) + String.Format("\\Tmp{0}.png", sGuid);
    }


    #endregion

    #region Event-handlers

    /// <summary>
    /// Bind data to the graph.
    /// </summary>
    public override void DataBind()
    {
        if (DataSource != null)
        {
          string sMask;
          string sLabel;
          int    iValue;

          switch (GraphType)
          {
            case GraphType.Bar  : Chart = new Barchart(Convert.ToInt32(Width.Value),Convert.ToInt32(Height.Value));
              Chart.BackgroundColor = this.BackColor;
              Chart.EnableLegend = false;
              Chart.Enable3D = false;
              ((Barchart)Chart).Orientation = _Orientation;
              ((Barchart)Chart).IsShowLabel = true;
              break;
            case GraphType.Line : break;
            case GraphType.Pie  : Chart = new Piechart(Convert.ToInt32(Width.Value),Convert.ToInt32(Height.Value));
              Chart.BackgroundColor = this.BackColor;
              Chart.EnableLegend = false;
              Chart.Enable3D = false;
              break;

          }

          DataView dw = (DataView)DataSource;

          DefaultDataSource ds = new DefaultDataSource(this.Title);
          ds.HorizontalTitle.Text = this.HorizontalTitle;
          ds.VerticalTitle.Text   = this.VerticalTitle; 

          Series series = new Series("Data 0");

          for (int i=0; i < dw.Table.Rows.Count;i++)
          {
            object[] aFields = dw.Table.Rows[i].ItemArray;

            sLabel = "";
            iValue = 0;

            for (int i2=0;i2 < aFields.Length;i2++)
            {
              switch (Fields[i2].Type)
              {
                case DataType.Label : sMask = Fields[i2].DisplayMask=="" ? "{0}" : Fields[i2].DisplayMask;
                  sLabel = String.Format(sMask, aFields[i2]);
                  break;
                case DataType.Value : iValue = Convert.ToInt32(aFields[i2]);
                  break;
              }
            }
            
            DataPoint dp = new DataPoint(sLabel,iValue,GraphType == GraphType.Pie ? ColorFromInt(i) : _GraphColor);

            series.add(dp);
          }

          ds.add(series);
          Chart.DataSource = ds;

          if (this.GraphTarget == GraphTarget.File)
            GraphToFile(Chart,GetTempFileName());
          else
            GraphToGraphField(Chart);
        }
    }

    /// <summary>
    /// Generate HTML for control.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
      /// Only show GraphField if DataSource is set.
      if (DataSource != null)
      {
        writer.AddAttribute("SRC",ImageUrl);
        writer.AddStyleAttribute("Height", Height.ToString());
        writer.AddStyleAttribute("Width", Width.ToString());
        writer.RenderBeginTag("IMG");
        writer.RenderEndTag();
      }
    }
    #endregion
	}
}
