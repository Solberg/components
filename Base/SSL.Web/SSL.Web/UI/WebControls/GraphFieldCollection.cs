using System;
using System.Collections;

namespace SSL.Web.UI.WebControls
{
	/// <summary>
	/// Summary description for DataTypeCollection.
	/// </summary>


  public enum DataType
  {
    NotUsed,
    Value,
    Label
  }


  /// <summary>
  /// Identification of an element in an resultset.
  /// </summary>
  public class GraphField
  {
    DataType _Type;
    string   _DisplayMask = "";
 
    public GraphField()
    {
      _Type        = DataType.Value;
      _DisplayMask = "";
    }

    public GraphField(DataType Itype)
    {
      _Type        = Itype;
      _DisplayMask = "";
    }

    
    public GraphField(DataType Itype,
      string IsDisplayMask)
    {
      _Type        = Itype;
      _DisplayMask = IsDisplayMask;
    }

    public DataType Type
    {
      get {return _Type;}
      set {_Type = value;}
    }

    public string DisplayMask
    {
      get {return _DisplayMask;}
      set {_DisplayMask = value;}
    }
  }


 

  /// <summary>
  ///  References to fields passed to a graph by it's datasource.
  /// </summary>
  public class GraphFieldCollection : CollectionBase
	{

      public GraphField this[int index]
      {
        get{return (GraphField)base.InnerList[index];}
      }

      public int Add(GraphField Idt)
      {
        return base.InnerList.Add(Idt);
      }

      public void Remove(GraphField Idt)
      {
        base.InnerList.Remove(Idt);
      }

      public bool Contains(GraphField Idt)
      {
        foreach (GraphField dt in this)
        {
          if (Idt == dt)
            return true;
        }
        return false;
      }
  }
}
