using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SSL.Web.UI.WebControls
{
	/// <summary>
	/// Pushbutton with image.
	/// </summary>
  [DefaultProperty("Text"), DefaultEvent("Click")] 
  public class ImageButton : WebControl,IPostBackEventHandler
	{
    #region Variables
    string _ImageUrl;
    string _Text;
    #endregion

    #region Events
		public event EventHandler Click;
    #endregion
    
    /// <summary>
    /// Path to the image displayed on the control
    /// </summary>
    [Description("Path to the image displayed on the control.")]
    public string ImageUrl
    {
      get{return _ImageUrl;}
      set{_ImageUrl = value;}
    }

    /// <summary>
    /// Text displayed on the button.
    /// </summary>
    [Description("Text displayed on the button.")]    
    public string Text
    {
      get{return _Text;}
      set{_Text = value;}
    }

    /// <summary>
    /// Render HTML for the control.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(System.Web.UI.HtmlTextWriter writer)
    {
      writer.AddAttribute("OnClick",Page.ClientScript.GetPostBackEventReference(this, String.Empty));
      writer.AddAttribute("Title", this.ToolTip);
      writer.AddStyleAttribute("Cursor","Hand");
      writer.AddStyleAttribute("Width", this.Width.ToString());
      writer.AddAttribute("Class", this.CssClass);
      writer.RenderBeginTag("BUTTON");
      writer.AddStyleAttribute("Width","100%");
      writer.RenderBeginTag("TABLE");
      writer.RenderBeginTag("TR");
      writer.AddStyleAttribute("Text-align","Center");
      writer.AddStyleAttribute("Font-weight", Font.Bold ? "bold" : "normal");
      writer.AddStyleAttribute("Font-style", Font.Italic ? "italic" : "normal");
      writer.AddStyleAttribute("Font-size", Font.Size.ToString());
      writer.RenderBeginTag("TD");
      writer.Write(this.Text);
      writer.RenderEndTag(); ///TD
      writer.AddStyleAttribute("Width","20px");
      writer.RenderBeginTag("TD");
      writer.AddAttribute("SRC",this.ImageUrl);
      writer.AddStyleAttribute("ImageAlign","Right");
      writer.RenderBeginTag("IMG");
      writer.RenderEndTag(); ///TD
      writer.RenderEndTag(); ///TR
      writer.RenderEndTag(); ///TABLE
      writer.RenderEndTag();/// IMG
      writer.RenderEndTag();/// BUTTON
    }

    /// <summary>
    /// React to click on the button
    /// </summary>
    /// <param name="e"></param>
    protected virtual void OnClick(EventArgs e)
    {
      if (Click != null)
        // Raise event.
        Click(this, e);
    }

    /// <summary>
    /// Entrypoint for postback from the client.
    /// </summary>
    /// <param name="eventArgument"></param>
    public void RaisePostBackEvent(string eventArgument)
    {
      OnClick(EventArgs.Empty);
    }




	}
}
