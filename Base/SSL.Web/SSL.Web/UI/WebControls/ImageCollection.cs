using System;
using System.Collections.Generic;
using System.Text;

namespace SSL.Web.UI.WebControls
{
  public class ImageCollection : System.Collections.CollectionBase
  {
    #region Properties
    public Image this[int index]
      {
        get { return (Image)base.InnerList[index]; }
      }
    #endregion

      #region Methods
      public int Add(Image Idt)
      {
        return base.InnerList.Add(Idt);
      }

      public void Remove(Image Idt)
      {
        base.InnerList.Remove(Idt);
      }

      public bool Contains(Image Idt)
      {
        foreach (Image dt in this)
        {
          if (Idt == dt)
            return true;
        }
        return false;
      }
      #endregion
    }
}
