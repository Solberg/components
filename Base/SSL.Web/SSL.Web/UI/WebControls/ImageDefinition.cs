using System;

namespace SSL.Web.UI.WebControls
{
  /// <summary>
  /// Definitions for an image.
  /// </summary>
  public class ImageDefinition
  {
    #region Variables
    private string _ToolTip = String.Empty;
    private string _Src = String.Empty;
    private readonly int _ThumbHeight = 150;
    private readonly int _ThumbWidth = 200;
    private readonly int _Width = 0;
    private readonly int _Height = 0;
    private bool _ShowThumbnail = true;
    private readonly string _ImageFolder;
    private readonly string _IndexFolder;
   
   #endregion

    #region Properties

    /// <summary>
    /// Thumbnail width
    /// </summary>
    public int ThumbWidth
    {
      get { return _ThumbWidth; }
    }

    /// <summary>
    /// Thumbnail height
    /// </summary>
    public int ThumbHeight
    {
      get { return _ThumbHeight; }
    }

    /// <summary>
    /// Image width
    /// </summary>
    public int Width
    {
      get { return _Width; }
    }

    /// <summary>
    /// Image height
    /// </summary>
    public int Height
    {
      get { return _Height; }
    }

    /// <summary>
    /// Path to the library where the image is located.
    /// </summary>
    public string Path
    {
      get 
      {
        return FullSizeImage.LastIndexOf("/") >= 0 ? FullSizeImage.Substring(0, FullSizeImage.LastIndexOf("/")) : String.Empty;
      }
    }


    /// <summary>
    /// FileName only.
    /// </summary>
    public string FileName
    {
      get 
      {
        return _Src.LastIndexOf("/") != -1 ? _Src.Substring(Src.LastIndexOf("/") + 1) : _Src;
      }
    }
	

    public string ToolTip
    {
      get { return _ToolTip; }
      set { _ToolTip = value; }
    }

    private string Src
    {
      get { return _Src; }
    }


    public bool ShowThumbnail
    {
      get { return _ShowThumbnail; }
      set { _ShowThumbnail = value; }
    }
	

	
    #endregion

    #region Constructors

    public ImageDefinition(string IsImageFolder,
      string IsIndexFolder,
      string IsSrc)
    {
      _IndexFolder = IsIndexFolder;
      _ImageFolder = IsImageFolder;
      _Src = IsSrc;

      string sPhysicalFile = System.Web.HttpContext.Current.Server.MapPath(String.Format("{0}/{1}", _ImageFolder, IsSrc));

      if (System.IO.File.Exists(sPhysicalFile))
      {
        System.Drawing.Bitmap bMap = new System.Drawing.Bitmap(sPhysicalFile);

        _Width = bMap.Width;
        _Height = bMap.Height;
      }

    }

    public ImageDefinition(string IsImageFolder,
      string IsImageIndex,
      string IsSrc,
      bool IbShowThumbnail) : this(IsImageFolder, IsImageIndex,IsSrc)
    {
      _ShowThumbnail = IbShowThumbnail;
    }

    public ImageDefinition(string IsImageFolder,
      string IsImageIndex,
      string IsSrc,
      string IsToolTip)
      : this(IsImageFolder, IsImageIndex, IsSrc)
    {
      _ToolTip = IsToolTip;
    }



    public ImageDefinition(string IsImageFolder,
      string IsImageIndex,
      string IsSrc,
      string IsToolTip,
      int IiThumbWidth,
      int IiThumbHeight) : this(IsImageFolder, IsImageIndex, IsSrc, IsToolTip)
    {
      _ThumbWidth = IiThumbWidth;
      _ThumbHeight = IiThumbHeight;
    }

    public ImageDefinition(string IsImagePath,
      string IsToolTip,
      int IiThumbWidth,
      int IiThumbHeight) : this(IsImagePath, IsToolTip)
    {
      _ThumbHeight = IiThumbHeight;
      _ThumbWidth = IiThumbWidth;
    }

    public ImageDefinition(string IsImagePath,
      string IsToolTip) : this(IsImagePath, true)
    {
      _ToolTip = IsToolTip;
    }


    public ImageDefinition(string IsImagePath, 
        bool IbUseIndexFolder)
    {
      _ImageFolder = IsImagePath.Substring(0, IsImagePath.LastIndexOf("/"));

      _IndexFolder = IbUseIndexFolder ? _ImageFolder + "/index" : _ImageFolder;
      _Src = IsImagePath.Substring(IsImagePath.LastIndexOf("/") + 1);

        _ShowThumbnail = IbUseIndexFolder;

      string sPhysicalFile = System.Web.HttpContext.Current.Server.MapPath(String.Format("{0}/{1}", _ImageFolder, _Src));

      if (System.IO.File.Exists(sPhysicalFile))
      {
        System.Drawing.Bitmap bMap = new System.Drawing.Bitmap(sPhysicalFile);

        _Width = bMap.Width;
        _Height = bMap.Height;
      }

    }

    #endregion

    #region Methods
    public string FullSizeImage
    {
      get
      {
        return String.Format("{0}/{1}", _ImageFolder, Src).Replace(" ", "%20");
      }
    }

    public string Thumbnail
    {
      get
      {
        return String.Format("{0}/{1}", _IndexFolder, Src).Replace(" ", "%20");
      }
    }

    #endregion
  }

}
