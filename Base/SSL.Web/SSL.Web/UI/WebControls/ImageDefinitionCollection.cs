namespace SSL.Web.UI.WebControls
{
    using System.Collections;
    /// <summary>
    ///  Collection of ImageDefinitions
    /// </summary>
    public class ImageDefinitionCollection : CollectionBase
    {
			#region Methods
			/// <summary>
			/// Indexer.
			/// </summary>
      public ImageDefinition this[int index]
      {
        get{return (ImageDefinition)InnerList[index];}
      }

			/// <summary>
			/// Add item to list
			/// </summary>
			/// <param name="dt"></param>
			/// <returns></returns>
      public int Add(ImageDefinition dt)
      {
        return InnerList.Add(dt);
      }

			/// <summary>
			/// Remove item from list
			/// </summary>
			/// <param name="dt"></param>
      public void Remove(ImageDefinition dt)
      {
        InnerList.Remove(dt);
      }

			/// <summary>
			/// Examine if a item is in the list
			/// </summary>
			/// <param name="dt"></param>
			/// <returns></returns>
      public bool Contains(ImageDefinition dt)
      {
        foreach (ImageDefinition dti in this)
        {
          if (dti == dt)
            return true;
        }
        return false;
      }
			#endregion
    }
  }

