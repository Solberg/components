using System;
using System.Collections.Generic;
using System.Text;

namespace SSL.Web.UI.WebControls
{
  public class NameValue
  {
    #region Variables
    string _Name;
    int _Value;
    #endregion

    #region Properties
    public string Name
    {
      get
      {
        return _Name;
      }

      set
      {
        _Name = value;
      }
    }

    public int Value
    {
      get
      {
        return _Value;
      }

      set
      {
        _Value = value;
      }
    }
    #endregion

    #region Constructors
    public NameValue()
    {
    }

    public NameValue(int IiValue,
      string IsName
      )
    {
      _Value = IiValue;
      _Name = IsName;

    }

    #endregion
  }
}
