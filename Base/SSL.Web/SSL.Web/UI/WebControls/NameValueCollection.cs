using System;
using System.Collections.Generic;
using System.Text;

namespace SSL.Web.UI.WebControls
{
  public class NameValueCollection : System.Collections.CollectionBase
  {
    #region Properties
    public NameValue this[int index]
      {
        get { return (NameValue)base.InnerList[index]; }
      }
    #endregion

    #region Methods
      public int Add(NameValue Idt)
      {
        return base.InnerList.Add(Idt);
      }

      public void Remove(NameValue Idt)
      {
        base.InnerList.Remove(Idt);
      }

      public bool Contains(NameValue Idt)
      {
        foreach (NameValue dt in this)
        {
          if (Idt == dt)
            return true;
        }
        return false;
      }
      #endregion
  }
}
