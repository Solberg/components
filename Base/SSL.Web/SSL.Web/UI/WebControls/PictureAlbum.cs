using System;
using System.ComponentModel;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
  /// <summary>
  /// Picturealbum with indexbar.
  /// </summary>
  [ParseChildren(true, "Albums")]
  public class PictureAlbum : System.Web.UI.WebControls.WebControl, IPostBackEventHandler
  {
    #region Event-handlers

    public event EventHandler AlbumChange;
    #endregion

    #region Variables
    private ImageDefinitionCollection _Images = new ImageDefinitionCollection();
    private NameValueCollection _Albums = new NameValueCollection();
		private string _PostBackEventReference;
    private System.Web.UI.WebControls.DropDownList cboAlbums = new System.Web.UI.WebControls.DropDownList();
    private int _AlbumId = -1;
    private bool _ShowAlbumList = true;
    #endregion

    #region Properties
    [Bindable(true)]
    [PersistenceMode(PersistenceMode.InnerProperty)] 
    public ImageDefinitionCollection Images
    {
      get { return _Images; }

      /// set m� ikke defineres, da det f�r kontrol til at fejle i designer.
      /// Jeg har 22.09.2003 ledt efter en l�sning p� nettet men eneste l�sning er ikke at definere set

    }

    [Bindable(true)]
    [PersistenceMode(PersistenceMode.InnerDefaultProperty)] 
    public NameValueCollection Albums
    {
      get
      {
        return _Albums;
      }

      set
      {
        _Albums = value;
      }
    }

    public int AlbumId
    {
      get
      {
        return _Albums.Count > 0 ? _AlbumId : -1;
      }

      set
      {
        _AlbumId = value;
      }
    }

    

    private int ImageBarWidth
    {
      get
      {
        return Convert.ToInt32(Width.Value - 25);
      }
    }

    public bool ShowAlbumList
    {
      get
      {
        return _ShowAlbumList;
      }

      set
      {
        _ShowAlbumList = value;
      }
    }


    #endregion

    #region Methods

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      _PostBackEventReference = Page.ClientScript.GetPostBackEventReference(this, ClientID + "_" + "cboAlbum");

      if (!Page.ClientScript.IsClientScriptBlockRegistered("PictureAlbum"))
      {
        string sJavaScript = "var aImages = new Array();";

        for (int i = 0; i < _Images.Count; i++)
        {
          sJavaScript += String.Format("aImages[{0}] = new imageItem(\"{1}\", \"{2}\");\n", i, _Images[i].FullSizeImage, _Images[i].ToolTip);
        }

        sJavaScript += "var bSlideshowRunning = false;\n"
                     + "function displayImagebar()\n"
                     + "{\n"
                     + "  document.getElementById(\"" + ClientID + "_Imagebar\").style.display=\"block\"\n"
                     + "  document.getElementById(\"" + ClientID + "_Message\").style.display=\"none\"\n"
                     + "}\n\n"
                     + "function imageItem(image_location, image_description)\n"
                     + "{\n"
                     + "  image_item = new Image();\n"
                     + "  image_item.src = image_location;\n"
                     + "  image_item.alt = image_description;\n"
                     + "}\n\n"
                    + "function switchImage(iCurrentImage)\n"
                    + "{\n"
                    + "  if (bSlideshowRunning)\n"
                    + "  {\n"
                    + "    var oImage = aImages[iCurrentImage];\n"
                    + "    BigImage().src = oImage.image_item.src;\n"
                    + "    BigImage().alt = oImage.image_item.alt;\n"
                    + "    var recur_call = \"switchImage(\" + (iCurrentImage < aImages.length - 1 ? iCurrentImage + 1 : 0) + \")\";\n"
                    + "    setTimeout(recur_call, 3000);\n"
                    + "  }\n"
                    + "}\n\n"
                    + "function BigImage()\n"
                    + "{\n"
                    + "return document.getElementById(\"" + ClientID + "_" + "BigPicture\");\n"
                    + "}\n"
                    + "\n\n"
                    + "  function PA_CurrentImage()\n"
                    + "  {\n"
                    + "    for (i=0;i < aImages.length; i++)\n"
                    + "    {\n"
                    + "      if (aImages[i].image_item.src == BigImage().src)\n"
                    + "      {\n"
                    + "        return i;\n"
                    + "      }\n"
                    + "    }\n"
                    + "}\n"
                    + "\n\n"
                    + "function PA_OnMouseOver(sender)\n"
                    + "{\n"
                    + " if (sender.className==\"BarImage\")\n"
                    + " {\n"
                    + "    sender.className=\"BarImageOver\";\n"
                    + " }\n"
                    + "}\n"
                    + "\n\n"
                    + "function PA_OnMouseOut(sender)\n"
                    + "{\n"
                    + "  if (sender.className==\"BarImageOver\")\n"
                    + "  {\n"
                    + "    sender.className=\"BarImage\";\n"
                    + "  }\n"
                    + "}\n"
                    + "\n"
                    + "function PA_OnClick(sender, IsSrc)\n"
                    + "{\n"
                    + "BigImage().style.display = sender.className==\"BarImageOver\" ? \"block\" : \"none\";\n"
                    + "if (sender.className==\"BarImageOver\")\n"
                    + "{\n"
                    + "var sPath = sender.src;\n"
                    + "sPath = sPath.replace(\"/index\", \"\");\n"
                    + "BigImage().src = IsSrc;\n"
                    + "BigImage().alt = sender.alt;\n"
                    + "}\n"
                    + "}\n\n"
                    + "function PA_SlideShow(sender)\n"
                    + "{\n"
                    + "  if (! bSlideshowRunning)\n"
                    + "  {\n"
                    + "    sender.src = \"/images/btn/stop.gif\";\n"
                    + "    sender.alt = \"Stop slideshow\";\n"
                    + "    bSlideshowRunning = true;\n"
                    + "    BigImage().style.display = \"block\";\n"
                    + "    switchImage(PA_CurrentImage());\n"
                    + "  }\n"
                    + "  else\n"
                    + "  {\n"
                    + "    sender.src = \"/images/btn/start.gif\";\n"
                    + "    sender.alt = \"Start slideshow\";\n"
                    + "    bSlideshowRunning = false;\n"
                    + "  }\n"
                    + "}\n\n"
                    + "function PA_ScrollImagebar(sender)\n"
                    + "{\n"
                    + "  var div = document.getElementById(\"" + ClientID + "_ImageDiv\");\n"
                    + "  switch (sender)\n"
                    + "  {\n"
                    + "    case document.getElementById(\"ScrollLeft\") :\n"
                    + "    if (div.scrollLeft > 0)\n"
                    + "    {\n"
                    + "      div.scrollLeft -= 400;\n"
                    + "    }\n"
                    + "    break;\n"
                    + "    case document.getElementById(\"ScrollRight\"):\n"
                    + "    if (div.scrollLeft < div.scrollWidth - " + Convert.ToString(ImageBarWidth) + ")\n"
                    + "    {\n"
                    + "	    div.scrollLeft += 400;\n"
                    + "	  }\n"
                    + "	  break;\n"
                    + "  }\n"
                    + "}\n"
                    + "function PA_ShowImage(sender)\n"
                    + "{\n"
                    + "  var oImage;\n"
                    + "  switch (sender)\n"
                    + "  {\n"
                    + "    case document.getElementById(\"PreviousImage\") :\n"
                    + "    if (PA_CurrentImage() > 0)\n"
                    + "      oImage = aImages[PA_CurrentImage() - 1];\n"
                    + "    break;\n"
                    + "    case document.getElementById(\"NextImage\"):\n"
                    + "    if (PA_CurrentImage() < aImages.length - 1)\n"
                    + "      oImage = aImages[PA_CurrentImage() + 1];\n"
                    + "	  break;\n"
                    + "  }\n"
                    + "  if (oImage != null)\n"
                    + "  {\n"
                    + "    PA_UntagImage(aImages[PA_CurrentImage()]);\n"
                    + "    BigImage().src = oImage.image_item.src;\n"
                    + "    BigImage().alt = oImage.image_item.alt;\n"
                    + "    PA_TagImage(oImage);\n"
                    + "  }\n"
                    + "}\n"
                    + "\n"
                    + "function PA_GetImageName(oImage)\n"
                    + "{\n"
                    + "  var sFileName = oImage.image_item.src.substr(7);\n"
                    + "  return sFileName.substr(sFileName.indexOf(\"/\"));\n"
                    + "}\n"
                    + "\n"
                    + "function PA_UntagImage(oImage)\n"
                    + "{\n"
                    + "  document.getElementById(PA_GetImageName(oImage)).className = \"BarImage\";\n"
                    + "}\n"
                    + "\n"
                    + "function PA_TagImage(oImage)\n"
                    + "{\n"
                    + "  var oImage = document.getElementById(PA_GetImageName(oImage));\n"
                    + "\n"
                    + "  oImage.className = \"BarImageOver\"\n"
                    + "  oImage.scrollIntoView();\n"
                    + "}\n"
                    
                    + "window.onload=displayImagebar;\n";

        Page.ClientScript.RegisterClientScriptBlock(GetType(), "PictureAlbum", sJavaScript, true);
      }
    }


    private void RenderButton(System.Web.UI.HtmlTextWriter writer,
      string IsId,
      string IsImage,
      string IsAlt,
      string IsOnClick)
    {
      writer.RenderBeginTag(HtmlTextWriterTag.Tr);
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toolbutton");
      writer.AddAttribute(HtmlTextWriterAttribute.Src, IsImage);
      writer.AddAttribute(HtmlTextWriterAttribute.Alt, IsAlt);
      writer.AddAttribute(HtmlTextWriterAttribute.Id, IsId);
      writer.AddAttribute(HtmlTextWriterAttribute.Onclick, IsOnClick);
      writer.RenderBeginTag(HtmlTextWriterTag.Img);
      writer.RenderEndTag(); /// img
      writer.RenderEndTag(); /// td
      writer.RenderEndTag(); /// tr
    }

    private void RenderButtonBox(HtmlTextWriter writer)
    {
      writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
      writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
      writer.RenderBeginTag(HtmlTextWriterTag.Table);
      RenderButton(writer, "btnSlideShow", "/images/btn/start.gif", "Start slideshow", "PA_SlideShow(this);");
      RenderButton(writer, "ScrollLeft", "/images/btn/leftmore.gif", "First picture", "PA_ScrollImagebar(this);");
      RenderButton(writer, "PreviousImage", "/images/btn/leftone.gif", "Previous picture", "PA_ShowImage(this);");
      RenderButton(writer, "NextImage", "/images/btn/rightone.gif", "Next picture", "PA_ShowImage(this);");
      RenderButton(writer, "ScrollRight", "/images/btn/rightmore.gif", "Last pictures", "PA_ScrollImagebar(this);");
      writer.RenderEndTag(); /// table
    }
    
    
    private void RenderImagebar(HtmlTextWriter writer)
    {
      writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
      writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
      writer.RenderBeginTag(HtmlTextWriterTag.Table);
      writer.RenderBeginTag(HtmlTextWriterTag.Tr);
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID + "_ImageDiv");
      writer.AddAttribute(HtmlTextWriterAttribute.Class, "ImageBar");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Width, Convert.ToString(ImageBarWidth) + "px");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Overflow, "hidden");
      writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "solid");
      writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "1px");
      writer.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "black");
      writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, "center");
      writer.AddStyleAttribute(HtmlTextWriterStyle.VerticalAlign, "middle");
      writer.RenderBeginTag(HtmlTextWriterTag.Div);
      writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID + "_Imagebar");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
      writer.RenderBeginTag(HtmlTextWriterTag.Table);
      writer.RenderBeginTag(HtmlTextWriterTag.Tr);
      for (int i = 0; i < _Images.Count; i++)
      {
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "ImageBarCell");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        writer.AddAttribute("onmouseover", "PA_OnMouseOver(this);");
        writer.AddAttribute("onmouseout", "PA_OnMouseOut(this);");

        writer.AddAttribute(HtmlTextWriterAttribute.Id, _Images[i].FullSizeImage);

        writer.AddAttribute(HtmlTextWriterAttribute.Onclick, String.Format("PA_OnClick(this, '{0}');", _Images[i].FullSizeImage));
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "BarImage");
        writer.AddAttribute(HtmlTextWriterAttribute.Border, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Src, _Images[i].Thumbnail);
        writer.AddAttribute(HtmlTextWriterAttribute.Alt, _Images[i].ToolTip);
        writer.AddStyleAttribute(HtmlTextWriterStyle.Width, _Images[i].ThumbWidth.ToString());
        writer.AddStyleAttribute(HtmlTextWriterStyle.Height, _Images[i].ThumbHeight.ToString());
        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag(); /// IMG
        writer.RenderEndTag(); /// TD
      }
      writer.RenderEndTag(); /// Tr
      writer.RenderEndTag(); /// Table
        writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID + "_Message");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontWeight, "bold");
        writer.RenderBeginTag(HtmlTextWriterTag.Span);
        writer.Write("Loading pictures wait.......");
        writer.RenderEndTag(); /// Span
      writer.RenderEndTag(); /// Div
      writer.RenderEndTag(); /// td
      writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "2px");
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      RenderButtonBox(writer);
      writer.RenderEndTag(); /// td
      writer.RenderEndTag(); /// tr
      writer.RenderEndTag(); /// table
    }

    private void RenderAlbumButtons(HtmlTextWriter writer)
    {
      writer.RenderBeginTag(HtmlTextWriterTag.Table);
      writer.RenderBeginTag(HtmlTextWriterTag.Tr);
      writer.AddAttribute(HtmlTextWriterAttribute.Class, "AlbumHeader");
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      writer.Write("Albums");
      writer.RenderEndTag();
      writer.RenderEndTag();

      for (int i = 0; i < _Albums.Count; i++)
      {
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "AlbumBtn");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "TabLink");
        writer.AddAttribute(HtmlTextWriterAttribute.Href,  "javascript:" + Page.ClientScript.GetPostBackEventReference(this, Convert.ToString(_Albums[i].Value)));
        writer.RenderBeginTag(HtmlTextWriterTag.A);
        writer.RenderBeginTag(HtmlTextWriterTag.Nobr);
        writer.Write(Albums[i].Name);
        writer.RenderEndTag(); // Nobr
        writer.RenderEndTag(); // A
        writer.RenderEndTag();
        writer.RenderEndTag();
      }
      writer.RenderEndTag();

    }


    #endregion

    #region Event-handlers
    protected override void LoadViewState(object savedState)
    {
      if (savedState != null)
      {
        // Load State from the array of objects that was saved at ;
        // SavedViewState.
        object[] myState = (object[])savedState;

        if (myState[0] != null)
          base.LoadViewState(myState[0]);

        if (myState[1] != null)
          _Albums = (NameValueCollection)myState[1];

      }
    }


    protected override object SaveViewState()
    {
      // Save State as a cumulative array of objects.
      object baseState = base.SaveViewState();
      object[] allStates = new object[3];
      allStates[0] = baseState;
///      allStates[1] = _Albums;
      return allStates;

    }
    
    
    /// <summary>
    /// Udskriv kontrol.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
      writer.AddStyleAttribute(HtmlTextWriterStyle.Width, Width.ToString());
      writer.AddStyleAttribute(HtmlTextWriterStyle.Height, Height.ToString());
      writer.AddAttribute(HtmlTextWriterAttribute.Border, "0");
      writer.RenderBeginTag(HtmlTextWriterTag.Table);
      writer.RenderBeginTag(HtmlTextWriterTag.Tr);
      if (_ShowAlbumList)
      {
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        RenderAlbumButtons(writer);
        writer.RenderEndTag();
      }
      writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, "center");
      writer.AddStyleAttribute(HtmlTextWriterStyle.VerticalAlign, "middle");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Width, "360px");
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID + "_" + "BigPicture");
      writer.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "black");
      writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "solid");
      writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "1px");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "360px");
      if (_Images.Count > 0)
      {
        writer.AddAttribute(HtmlTextWriterAttribute.Src, _Images[0].FullSizeImage);
        writer.AddAttribute(HtmlTextWriterAttribute.Alt, _Images[0].ToolTip);
      }
      writer.AddStyleAttribute(HtmlTextWriterStyle.Display, (Images.Count > 0 ? "block" : "none"));
      writer.RenderBeginTag(HtmlTextWriterTag.Img);
      writer.RenderEndTag();
      writer.RenderEndTag(); /// td
      writer.RenderEndTag(); /// tr
      writer.AddStyleAttribute(HtmlTextWriterStyle.VerticalAlign, "bottom");
      writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "90px");
      writer.RenderBeginTag(HtmlTextWriterTag.Tr);
      writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "3");
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      RenderImagebar(writer);
      writer.RenderEndTag(); // td
      writer.RenderEndTag(); // tr
      writer.RenderEndTag(); // table
    }
    #endregion

    #region IPostBackEventHandler

    // (4) Override RaisePostDataChangedEvent method.
    // You must create this method.
    public void RaisePostBackEvent(string eventArgument)
    {
      _AlbumId = Convert.ToInt32(eventArgument);

      if (AlbumChange != null)
      {
        AlbumChange(this, EventArgs.Empty);
      }
    }

    #endregion
  }
}
