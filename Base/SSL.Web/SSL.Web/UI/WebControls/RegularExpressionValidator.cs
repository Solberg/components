using System;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
  public class RegularExpressionValidator : System.Web.UI.WebControls.RegularExpressionValidator
  {
    /// <summary>
    /// Overstyr udskrivning af validator. Som default inds�tter CustomValidator en &nbsp; p� siden hvis
    /// den er valid. Dette giver layout problemer. Desuden er der brug for at inds�tte validator som
    /// spacer, n�r validator er valid.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
      if (Visible)
      {
        if (!IsValid
          || EnableClientScript)
        {
          writer.AddAttribute(HtmlTextWriterAttribute.Id, ClientID);
          if (IsValid)
          {
            writer.AddStyleAttribute(HtmlTextWriterStyle.Visibility, "hidden");
          }

          writer.AddAttribute(HtmlTextWriterAttribute.Alt, this.ToolTip);
          writer.RenderBeginTag(HtmlTextWriterTag.Span);
          writer.AddAttribute(HtmlTextWriterAttribute.Src, "/images/btn/error.gif");
          writer.AddAttribute(HtmlTextWriterAttribute.Alt, String.Empty);
          writer.RenderBeginTag(HtmlTextWriterTag.Img);
          writer.RenderEndTag(); /// Img
          writer.RenderEndTag(); /// Span
        }
      }
    }

  }
}
