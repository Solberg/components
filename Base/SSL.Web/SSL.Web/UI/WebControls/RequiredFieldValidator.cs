using System;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
  /// <summary>
  /// Udvidet RequiredFieldValidator, der markerer fejl med gif fil.
  /// </summary>
  /// <remarks>
  ///<programmer version="2.0" date="20071220" programmer="SSL"></programmer>
  /// </remarks>
  public class RequiredFieldValidator : System.Web.UI.WebControls.RequiredFieldValidator, IValidator
  {
      #region Variables
      ValidatorPresenter _presenter;
      #endregion

      #region protected
      protected override void OnInit(EventArgs e)
      {
          base.OnInit(e);
          _presenter = new ValidatorPresenter(this);
      }
      #endregion

      /// <summary>
    /// F�r kontrol udskrives inds�ttes meddelelse i billede og fejlmarkering af tilknyttet kontrol
    /// foretages.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      _presenter.PreRender();
    }

    protected override void Render(HtmlTextWriter writer)
    {
        _presenter.Render(writer);
    }

    public void BaseRender(HtmlTextWriter writer)
    {
       base.Render(writer);
    }
  }
}
