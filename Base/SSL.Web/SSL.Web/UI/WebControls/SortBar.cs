using System;
using System.Web.UI;
using System.ComponentModel;
using System.Collections.Generic;

namespace SSL.Web.UI.WebControls
{

  /// <summary>
  /// Sortbar containing af number of sortbuttons.
  /// </summary>
	[ParseChildren(true, "SortButtons")]
  public class SortBar : System.Web.UI.WebControls.LinkButton
		//////////////////////////////////////////////////////////////////////////////////////////////////
		/// Funktion   : Sortbar indeholdende x antal sortbuttons.
		/// Programm�r : S�ren Solberg Larsen
		/// Dato       : 22.09.2003.
		//////////////////////////////////////////////////////////////////////////////////////////////////
	{
		private List<SortButton> sortbuttons = new List<SortButton>();

    public event EventHandler OnChangeSortorder;


		protected override void OnInit(EventArgs e)
			//////////////////////////////////////////////////////////////////////////////////////////////////
			/// Funktion   : Gem info om evt. valgt knap.
			/// Programm�r : S�ren Solberg Larsen
			/// Dato       : 26.09.2003.
			//////////////////////////////////////////////////////////////////////////////////////////////////
		{
			base.OnInit (e);
			SortButton sb = SelectedButton;

			if (sb != null)
			{
				SortKey       = (sb.SortOrder==SortOrderType.Ascending) ? sb.SortKey : sb.SortKeyDesc;
				SelectedIndex = sb.Index; 
				SortOrder     = sb.SortOrder;
			}
		}




///		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]					
			///  Definerer at SortButtons er den eneste Inner control i Toolbar
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)] 
		public List<SortButton> SortButtons
			//////////////////////////////////////////////////////////////////////////////////////////////////
			/// Funktion   : Collection af SortButton. SortButtons defineres i designer.
			/// Programm�r : S�ren Solberg Larsen
			/// Dato       : 22.09.2003.
			//////////////////////////////////////////////////////////////////////////////////////////////////
		{
			get
			{
				return sortbuttons;
			}

			/// set m� ikke defineres, da det f�r kontrol til at fejle i designer.
			/// Jeg har 22.09.2003 ledt efter en l�sning p� nettet men eneste l�sning er ikke at definere set
		}


		[Browsable(false)]
		public string SortKey
		{
			get
			{
				object o = ViewState["SortKey"];

				return o != null ? (string)o : "";
			}
 
			set
			{
				// Store the property setting.
				ViewState["SortKey"] = value;
			}
		}


		protected SortOrderType SortOrder
		{
			get
			{
				object o = ViewState["SortOrderType"];

				return o != null ? (SortOrderType)o : SortOrderType.Ascending;
			}

			set
			{
				ViewState["SortOrderType"] = value;

				if (SelectedIndex != -1)
  			  SortButtons[SelectedIndex].SortOrder = value;
			}
		
		}



    /// <summary>
    /// Index p� den SortButton der er selected.
    /// </summary>
   	/// <remarks>
	  /// <programmer date="20030926">S�ren Solberg Larsen</programmer>
	  /// <revision date="20040519">SelectedId �ndret til SelectedIndex</revision>
	  /// </remarks>

		private int SelectedIndex
		{
			get
			{
				object o = ViewState["SelectedIndex"];

				return o != null ? (int)o : -1;
			}

			set
			{
				ViewState["SelectedIndex"] = value;

				foreach (SortButton sb in SortButtons)
				{
					sb.Selected = (value == sb.Index) && (value != -1);
				}
			}
		}



		/// <summary>
		/// Reference til den knap der er markeret som valgt.
		/// </summary>
		/// <remarks>
		/// <programmer>S�ren Solberg Larsen</programmer>
		/// <date>26.09.2003</date>
		/// </remarks>
		protected SortButton SelectedButton
		{
			get
			{
				foreach (SortButton sb in SortButtons)
				{
					if (sb.Selected)
					{
						return sb;
					}
				}
				return null;
			}
		}

		
  	///////////////////////////////////////// FUNCTIONS ////////////////////////////////////		
  	
    /// <summary>
    /// S�t selected button og dennes sortorder.
    /// </summary>
    /// <param name="IiIndex">Index for valgt knap</param>
    /// <param name="Isot"></param>
	  /// <remarks>
	  /// <programmer date="20031008">S�ren Solberg Larsen</programmer>
	  /// <revision date="20040519">SelectedId �ndret til Index</revision>
	  /// </remarks>

		public void SetSelected(int IiIndex, SortOrderType Isot)
		{
			this.SelectedIndex = IiIndex;
			SortButton sb = SelectedButton;
			if (sb != null)
			  sb.SortOrder = Isot;
		}

		

		
		///////////////////////////////////////// EVENT HANDLERS ////////////////////////////////////		
		
		protected override void LoadViewState(object savedState) 
			//////////////////////////////////////////////////////////////////////////////////////////////////
			/// Funktion   : Loadviewstate for kontrol og opdater bla. info om valgt knap.
			/// Programm�r : S�ren Solberg Larsen
			/// Dato       : 26.09.2003.
			//////////////////////////////////////////////////////////////////////////////////////////////////
		{
			if (savedState != null)
			{
				// Load State from the array of objects that was saved at ;
				// SavedViewState.
				object[] myState = (object[])savedState;

				if (myState[0] != null)
					base.LoadViewState(myState[0]);

				if (myState[1] != null)
					SelectedIndex = (int)myState[1];

				if (myState[2] != null)
					SortOrder = (SortOrderType)myState[2];

			}
		}



		protected override object SaveViewState()
			//////////////////////////////////////////////////////////////////////////////////////////////////
			/// Funktion   : Saveviewstate for kontrol incl. valgt knap.
			/// Programm�r : S�ren Solberg Larsen
			/// Dato       : 26.09.2003.
			//////////////////////////////////////////////////////////////////////////////////////////////////
		{  
			// Save State as a cumulative array of objects.
			var baseState = base.SaveViewState();
			var allStates = new object[3];
			allStates[0] = baseState;
			allStates[1] = SelectedIndex;
			allStates[2] = SortOrder;
			return allStates;
		}

		/// <summary>
		/// Skriv HTML til client
		/// </summary>
		/// <param name="output">Output objekt</param>
		/// <remarks>
		/// <programmer date=20030922>S�ren Solberg Larsen</programmer>
		/// <revision date="20031027">nowrap og width p� TH</revision>
    /// <revision date=20060621>Page.ClientScript erstatter Page</revision>
		/// <revision version="2.10.0" date="20090428" programmer="SSL">Tag h�jde for om de enkelte kolonner er synlige.</revision>
    /// </remarks>
		protected override void Render(HtmlTextWriter output)
		{
      output.RenderBeginTag(HtmlTextWriterTag.Div);

			/// Hvis der ikke er nogen knapper, s� inds�t en dummyknap
			/// s� kontrol ikke er tom i layout editor
			if (SortButtons.Count == 0)
			{
				var sb = new SortButton();
				sb.ButtonType = SortButtonType.Spacer;
				sb.Text = "Ingen knapper defineret";
				SortButtons.Add(sb);
			}

			/// Skriv alle definerede SortButtons til side.
			foreach (SortButton sb in SortButtons)
			{
				if (sb.Visible)
				{
					if (Enabled)
					{
            sb.Click += ButtonClick;
					}

					sb.Enabled = Enabled;
					sb.RenderControl(output);
				}
			}
			output.RenderEndTag(); /// Div
		}



    private void ButtonClick(object sender, EventArgs e)
    {
      SortButton sbSender = (SortButton)sender;

      foreach (SortButton sb in SortButtons)
        SortButton.UpdateSortButton(sbSender, sb);


      SortKey = (sbSender.SortOrder == SortOrderType.Ascending) ? sbSender.SortKey : sbSender.SortKeyDesc;
      SelectedIndex = sbSender.Index;
      SortOrder = sbSender.SortOrder;


      if (OnChangeSortorder != null)
      {
        OnChangeSortorder(sbSender, System.EventArgs.Empty);
      }

    }

	}

}
