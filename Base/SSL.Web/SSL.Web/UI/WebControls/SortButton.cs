using System;
using System.Web;
using System.ComponentModel;
using System.Web.UI;
namespace SSL.Web.UI.WebControls
{
  /// <summary>
  /// Sorteringsmuligheder for knap.
  /// </summary>
  public enum SortOrderType
  {
    Ascending,
    Descending
  }

  /// <summary>
  /// Mulige typer for en knap.
  /// </summary>
  public enum SortButtonType
  {
    Spacer,
    Button,
    ScrollSpacer
  }


  public class SortButton : System.Web.UI.WebControls.LinkButton
  {

    #region Variables

    private SortButtonType _ButtonType = SortButtonType.Button;
    private string _PostBackEventReference;
    private string _SortKey = "";
    private string _SortKeyDesc = "";
    private int _index;

    #endregion

    #region Properties

    [Description("Index for kontrol i forhold til andre af samme type.")]
    [Category("Behavior")]
    [DefaultValue(0)]
    [Browsable(false)]
    [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
    public int Index
    {
      get { return _index; }
      set { _index = value; }
    }

    /// <summary>
    /// Knappens type. SortButton eller Spacer. Spacere har ingen sorteringsfunktion.
    /// </summary>
    /// <remarks>
    /// <programmer date="20030922">S�ren Solberg Larsen</programmer>
    /// </remarks>
    [DefaultValue(SortButtonType.Button)]
    [RefreshProperties(RefreshProperties.All)]
    public SortButtonType ButtonType
    {
      get { return _ButtonType; }

      set
      {
        if (value != SortButtonType.Button)
        {
          Selected = false;
          SortKey = "";
          SortKeyDesc = "";
          ID = "";
          if (value == SortButtonType.ScrollSpacer)
          {
            Text = "&nbsp;";
          }
        }
        _ButtonType = value;
      }
    }

    /// <summary>
    /// Angiver om sorteringsr�kkef�lge er stigende eller faldende. V�rdi skal gemmes
    /// i viewstate for at overleve postback til server.
    /// </summary>
    /// <remarks>
    /// <programmer date="20030131">S�ren Solberg Larsen</programmer>
    /// </remarks>
    [DefaultValue(SortOrderType.Ascending)]
    [Browsable(true)] // Metatag der s�rger for at property kan s�ttes p� designtime.
    [Description("Sorteringsr�kkef�lge Ascending=stigende, descending=faldende")]
    public SortOrderType SortOrder
    {
      get
      {
        object o = ViewState["SortOrderType"];

        return (o != null) ? (SortOrderType)o : SortOrderType.Ascending;
      }

      set { ViewState["SortOrderType"] = value; }
    }


    /// <summary>
    /// Angiver om en knap er valgt
    /// </summary>
    /// <remarks>
    /// <programmer date="20030131">S�ren Solberg Larsen</programmer>
    /// <revisions>
    ///   <revision date="20031029">Select kun hvis buttontype=Button</revision>
    /// </revisions>
    /// </remarks>
    [Bindable(true)]
    public bool Selected
    {
      get
      {
        object o = ViewState["Selected"];

        return (o != null) ? (bool)o : false;
      }

      set { ViewState["Selected"] = value && ButtonType == SortButtonType.Button; }
    }


    /// <summary>
    /// Angiver hvilke felter der skal benyttes til en sortering af dataudtr�k.
    /// </summary>
    [Browsable(true)] // Metatag der s�rger for at property kan s�ttes p� designtime.
    [Description("Sorteringsn�gle stigende orden")]
    public string SortKey
    {
      get { return _SortKey; }
      set { _SortKey = value; }
    }


    /// <summary>
    /// Angiver hvilke felter der skal benyttes til en sortering af dataudtr�k faldende
    /// </summary>
    [Description("Sorteringsn�gle faldende orden.")]
    public string SortKeyDesc
    {
      get { return _SortKeyDesc; }
      set { _SortKeyDesc = value; }
    }


    /// <summary>
    /// Tekst der vises p� knap.
    /// </summary>
    /// <remarks>
    /// <programmer date="20030204">S�ren Solberg Larsen</programmer>
    /// </remarks>
    [Bindable(true)]
    [Description("Tekst der skal vises p� sortbutton")]
    public override string Text
    {
      get { return base.Text; }
      set { base.Text = value; }
    }


    /// <summary>
    /// Navn p� funktion der skal kaldes ved postback (Click) fra komponent.
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public string PostBackEventReference
    {
      get { return _PostBackEventReference; }
      set { _PostBackEventReference = value; }
    }

    #endregion

    #region Methods

    /// <summary>
    /// V�lg knap og s�t sorteringsorden til Iso
    /// </summary>
    /// <param name="Iso"></param>
    /// <remarks>
    /// <programmer date="20030501">S�ren Solberg Larsen</programmer>
    /// </remarks>
    public void Select(SortOrderType Iso)
    {
      SortOrder = Iso;
      Selected = true;
    }


    /// <summary>
    /// Frav�lg Sortbutton med mindre sortbutton er lig med objekt der sender meddelelse.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Dsb"></param>
    /// <remarks>
    /// <programmer date="20030204">S�ren Solberg Larsen</programmer>
    /// </remarks>
    public static void UpdateSortButton(object sender, SortButton Dsb)
    {
      Dsb.Selected = sender == Dsb ? Dsb.Selected : false;
    }

    #endregion

    #region Event-handlers

    /// <summary>
    /// Udf�res f�r Render p�begyndes.
    /// </summary>
    /// <param name="e"></param>
    /// <remarks>
    /// <revision date=20060621>Page.ClientScript erstatter Page</revision>
    /// </remarks>
    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);
      _PostBackEventReference = Page.ClientScript.GetPostBackEventReference(this, String.Empty);
    }


    /// <summary>
    /// Udskriv HTML for kontrol.
    /// </summary>
    /// <param name="output">Reference til HTMLWriter</param>
    /// <remarks>
    /// <programmer date="20031125">S�ren Solberg Larsen</programmer>
    /// <revisions>
    /// <revision date="20030930">Omfattende �ndringer i forbindelse med Sortbar komponent, bla. Enabled property.</revision>
    /// <revision date="20031125">Byttet om p� bitmaps for sortup og sortdown</revision>
    /// <revision date="20040224">Scrollspacer tilf�jet</revision>
    /// <revision date="20041026">Sortable.gif p� SortButton</revision>
    /// <revision date="20060306">Fakebutton class �ndret til FakeButton</revision>
    /// </revisions>
    /// </remarks>
    protected override void Render(HtmlTextWriter output)
    {
      string sImg = SortOrder == SortOrderType.Ascending ? "SortUp.gif" : "SortDown.gif";
      
      CssClass = ButtonType == SortButtonType.ScrollSpacer
                        ? "ScrollSpacer"
                        : Enabled && ButtonType == SortButtonType.Button ? "sortButton" : "FakeButton";


      base.Render(output);
    }

    /// <summary>
    /// :todo Denne rutine skal kaldes n�r der klikkes p� linkbutton.
    /// </summary>
    /// <param name="e"></param>
    protected virtual void OnClick(EventArgs e)
    {
      // Skift sorteringorden
      if (Selected)
        SortOrder = SortOrder == SortOrderType.Ascending ? SortOrderType.Descending : SortOrderType.Ascending;

      Selected = true;

    }

    #endregion

  }
}
