using System;
using System.ComponentModel;
using System.Reflection;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
	/// <summary>
	/// Page on tabbed notebook.
	/// </summary>
	/// 


  public enum TabPageType
  {
  
    TabPage,
    Filler,
    PreviousSet,
    NextSet
  }



  [DefaultProperty("Text"), DefaultEvent("Click")] 
  public class TabPage :  System.Web.UI.WebControls.WebControl
  {
    private string      _Text      = "Page";
    private bool        _LastPage  = true;
    private bool        _FirstPage = true;
    private int         _PageIndex = 0;
    private bool        _NextPageSelected = false;
    private string      _PostBackEventReference;
    private TabPageType _PageType;
    private bool        _Selected = false;

    public event EventHandler Click;

    #region Constructors

    public TabPage(TabPageType Itpt,
      string IsText, 
      int IiWidth,
      int IiPageIndex,
      bool IbSelected,
      bool IbNextPageSelected)
    {
      _Text             = IsText;
      Width             = IiWidth;
      _PageType         = Itpt;
      _PageIndex        = IiPageIndex;
      _Selected         = IbSelected;
      _NextPageSelected = IbNextPageSelected;
    }

    public TabPage(TabPageType Itpt,
      string IsText, 
      int IiWidth) : this(Itpt, IsText, IiWidth,-1,false,false)
    {
    }

    
    public TabPage(string IsText, 
      int IiWidth) : this(TabPageType.TabPage, IsText, IiWidth)
    {
    }

    #endregion

    #region Properties

    /// <summary>
    /// Titletext for page
    /// </summary>
    [Bindable(true)]
    [Description("Titletext for page")]    
    public string Text 
    {
      get{return _Text;}
      set{_Text = value;}
    }


    [Bindable(true)]
    [DefaultValue(TabPageType.TabPage)]
    [Description("Type of the page.")]    
    public TabPageType PageType
    {
      get{return _PageType;}
      set{_PageType = value;}
    }



    /// <summary>
    /// If true then the page is selected.
    /// </summary>
    [Description("If true then this page is selected")]    
    public bool Selected
    {
      set { _Selected = value;}
      get {return _Selected;}
    }


    /// <summary>
    /// PageIndex within a tabbednotebook.
    /// </summary>
    [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
    [Browsable(false)]
    public int PageIndex
    {
      get{return _PageIndex;}
      set{_PageIndex = value;}
    }

    
    /// <summary>
    /// If true then the next page in set is selected.
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
    public bool NextPageSelected
    {
      get{return _NextPageSelected;}
      set{_NextPageSelected = value;}
    }


    /// <summary>
    /// If true then this page is the last page on a tabbednotebook
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
    public bool LastPage
    {
      get{return _LastPage;}
      set{_LastPage = value;}
    }

    /// <summary>
    /// If true then this page is the first page on a tabbednotebook
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
    public bool FirstPage
    {
      get{return _FirstPage;}
      set{_FirstPage = value;}
    }

    /// <summary>
    /// Reference til funktion der kaldes ved postback. S�ttes enten i OnPrerender eller
    /// fra TabbedNoteBook.
    /// </summary>
    [Browsable(false)]
    [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
    public string PostBackEventReference
    {
      get{return _PostBackEventReference;}

      set{_PostBackEventReference = value;}
    }

    #endregion

    #region Methods
    /// <summary>
    /// Return right style for page.
    /// </summary>
    /// <param name="IiPageIdx"></param>
    /// <returns></returns>
    private string StyleRight(int IiPageIdx)
    {
      if (LastPage)
      {
        if (Selected)
          return "TabRightSelect";
        else
          return "TabRight";
      }
      else if (Selected)
        return "TabSepRight";
      else if (NextPageSelected) // If next page is selected.
        return "TabSepLeft";
      else
        return "TabSepMiddle";
    }

    private void BlankImage(HtmlTextWriter writer)
    {
      writer.AddAttribute(HtmlTextWriterAttribute.Src, "/images/layout/blank.gif");
      writer.RenderBeginTag(HtmlTextWriterTag.Img);
      writer.RenderEndTag();
    }


    private void RenderImageLink(HtmlTextWriter writer,
      string IsAlt,
      string IsImage)
    {
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      writer.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:" + PostBackEventReference);
      writer.AddAttribute(HtmlTextWriterAttribute.Class, "TabLink");
      writer.RenderBeginTag(HtmlTextWriterTag.A);
      writer.AddAttribute(HtmlTextWriterAttribute.Alt, IsAlt);
      writer.AddAttribute(HtmlTextWriterAttribute.Src, IsImage);
      writer.AddAttribute(HtmlTextWriterAttribute.Border, "0");
      writer.RenderBeginTag(HtmlTextWriterTag.Img);
      writer.RenderEndTag(); /// IMG
      writer.RenderEndTag(); /// A
      writer.RenderEndTag(); // TD


    }

 

    #endregion


    /// <summary>
    /// Render control to page.
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
      if (FirstPage)
      {
        if (Selected)
        {
          writer.AddAttribute(HtmlTextWriterAttribute.Class,"TabLeftSelect");
        }
        else
        {
          writer.AddAttribute(HtmlTextWriterAttribute.Class, "TabLeft");
        }
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        BlankImage(writer);
        writer.RenderEndTag();
      }


      if (Selected)
      {
        writer.AddAttribute(HtmlTextWriterAttribute.Class,"TabBackSelect");
      }
      else
      {
        if (PageType == TabPageType.Filler)
          writer.AddAttribute(HtmlTextWriterAttribute.Class,"TabFiller");
        else
          writer.AddAttribute(HtmlTextWriterAttribute.Class,"TabBack");
      }

      writer.AddStyleAttribute(HtmlTextWriterStyle.Width, Convert.ToString(Width));

      switch (PageType)
      {
        case TabPageType.TabPage :
          writer.RenderBeginTag(HtmlTextWriterTag.Td);
          writer.AddAttribute(HtmlTextWriterAttribute.Href, "javascript:" + PostBackEventReference);
          writer.AddAttribute(HtmlTextWriterAttribute.Class, "TabLink");
          writer.RenderBeginTag(HtmlTextWriterTag.A);
          writer.WriteLine(this.Text);
          writer.RenderEndTag();
          writer.RenderEndTag(); // TD
          writer.AddAttribute(HtmlTextWriterAttribute.Class,StyleRight(PageIndex));
          break;
        case TabPageType.Filler :
          writer.RenderBeginTag(HtmlTextWriterTag.Td);
          BlankImage(writer);
          writer.RenderEndTag();
          writer.AddAttribute(HtmlTextWriterAttribute.Class,"TabFillerRight");
          break;
        case TabPageType.NextSet :
          RenderImageLink(writer, "Show next pageset", "/images/btn/next.gif");
          writer.AddAttribute(HtmlTextWriterAttribute.Class,StyleRight(PageIndex));
          break;
        case TabPageType.PreviousSet :
          RenderImageLink(writer, "Show previous pageset", "/images/btn/previous.gif");
          writer.AddAttribute(HtmlTextWriterAttribute.Class,StyleRight(PageIndex));
          break;
      }
      writer.RenderBeginTag(HtmlTextWriterTag.Td);
      BlankImage(writer); /// To make sure something is in the cell.
      writer.RenderEndTag();
    }

    public virtual void OnClick(EventArgs e)
    {
      if (Click != null)
        // Raise event.
        Click(this, e);
    }
	}
}
