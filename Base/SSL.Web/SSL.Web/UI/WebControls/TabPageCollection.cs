using System;
using System.Collections;
using System.Reflection;

namespace SSL.Web.UI.WebControls
{
  /// <summary>
  ///  References to fields passed to a graph by it's datasource.
  /// </summary>
  public class TabPageCollection : CollectionBase
  {

    #region Properties

    /// <summary>
    /// Index property.
    /// </summary>
    public TabPage this[int index]
    {
      get{return (TabPage)base.InnerList[index];}
    }
    
    #endregion


    #region Methods
    public int Add(TabPage Idt)
    {
      return base.InnerList.Add(Idt);
    }

    /// <summary>
    /// Copy a ranges of pages from another TabPageCollection
    /// </summary>
    /// <param name="IaPages">TabPageCollection to copy from</param>
    /// <param name="IiFromIdx">Copy from index</param>
    /// <param name="IiCnt">Copy count.</param>
    public void AddRange(TabPageCollection IaPages,
      int IiFromIdx,
      int IiCnt)
    {
      try
      {
        for (int i = IiFromIdx; i < IiFromIdx + IiCnt;i++)
        {
          if (i < IaPages.Count)
          {
            this.Add(new TabPage(IaPages[i].PageType, 
                                 IaPages[i].Text, 
                                 Convert.ToInt32(IaPages[i].Width.Value),
                                 IaPages[i].PageIndex,
                                 IaPages[i].Selected,
                                 IaPages[i].NextPageSelected));
          }
        }
      }
      catch (Exception exc)
      {
        throw new ALKA.Exceptions.FunctionException(MethodInfo.GetCurrentMethod(), exc.Message);
      }
    }

    public void Remove(TabPage Idt)
    {
      base.InnerList.Remove(Idt);
    }

    public bool Contains(TabPage Idt)
    {
      foreach (TabPage dt in this)
      {
        if (Idt == dt)
          return true;
      }
      return false;
    }

    #endregion
  }
}
