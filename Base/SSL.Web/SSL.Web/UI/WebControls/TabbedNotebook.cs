using System;
using System.ComponentModel;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{

	/// <summary>
	/// Summary description for TabbedNotebook.
	/// </summary>
  [ParseChildren(true, "TabPages")]
  public class TabbedNotebook : System.Web.UI.WebControls.WebControl, IPostBackEventHandler
  {
    private TabPageCollection _TabPages = new TabPageCollection();
    private int _SelectedPageIndex = 0;
    private bool _AllowSets = false;
    private int  _SetSize = 5;
    public event EventHandler OnPageChange;
    public event EventHandler OnRenderPage;

    #region Properties
   
    /// <summary>
    /// Number of pages on the Notebook
    /// </summary>
    [PersistenceMode(PersistenceMode.InnerDefaultProperty)] 
    public TabPageCollection TabPages
    {
      get {return _TabPages;}

      /// set m� ikke defineres, da det f�r kontrol til at fejle i designer.
      /// Jeg har 22.09.2003 ledt efter en l�sning p� nettet men eneste l�sning er ikke at definere set

    }

    private TabPage CurrentPage
    {
      get
      {
        return TabPages[SelectedPageIndex];
      }
    }


    /// <summary>
    /// Index of selected page on NoteBook.
    /// </summary>
    [DefaultValue(0)]
    public int SelectedPageIndex
    {
      get {return _SelectedPageIndex;}
      set {
            _SelectedPageIndex = value;

            if (OnPageChange != null)
            {
              OnPageChange(CurrentPage,System.EventArgs.Empty);
            }
          }
    }

    /// <summary>
    /// Allow that pages are divided into more than one set.
    /// </summary>
    public bool AllowSets
    {
      get {return _AllowSets;}
      set {_AllowSets = value;}
    }


    /// <summary>
    /// Number of pages in each set.
    /// </summary>
    public int SetSize
    {
      get {return AllowSets ? _SetSize : TabPages.Count;}
      set {_SetSize = value;}
    }

    /// <summary>
    /// Determine the index of the current pageset.
    /// </summary>
    private int CurrentSetIndex
    {
      get
      {
        return AllowSets ? Convert.ToInt32(SelectedPageIndex / SetSize) : 0;
      }
    }

    /// <summary>
    /// Index of the first page in the current set.
    /// </summary>
    private int FirstPageInSet
    {
      get
      {
        return CurrentSetIndex * SetSize;
      }
    }

    /// <summary>
    /// Return index of the last page in the current set.
    /// </summary>
    private int LastPageInSet
    {
      get
      {
        return ((CurrentSetIndex + 1) * SetSize) - 1 > TabPages.Count - 1 ? TabPages.Count - 1 : ((CurrentSetIndex + 1) * SetSize) - 1;
      }
    }

    #endregion


/// <summary>
/// Writer control to output.
/// </summary>
/// <param name="writer"></param>

    protected override void Render(HtmlTextWriter writer)
    {
        int iFillerWidth = Convert.ToInt32(Width.Value);
        TabPageCollection  aPages = new TabPageCollection();


        if (Convert.ToString(Width) != String.Empty)
          writer.AddStyleAttribute(HtmlTextWriterStyle.Width, Convert.ToString(Width));
        
        if (Convert.ToString(Height) != String.Empty)
          writer.AddStyleAttribute(HtmlTextWriterStyle.Height, Convert.ToString(Height));

        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing,"0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding,"0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);
        writer.AddAttribute(HtmlTextWriterAttribute.Height, "18px");
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        /// Update  SelectedPageIndex.
        for (int i = 0; i < TabPages.Count; i++)
        {
          TabPages[i].PageIndex = i;
          TabPages[i].Selected = (SelectedPageIndex == i);
          TabPages[i].NextPageSelected = (i == SelectedPageIndex - 1);
        }

        /// Copy pages to be used in set.
        aPages.AddRange(TabPages, FirstPageInSet, SetSize);

        if (this.CurrentSetIndex > 0)
        {
          aPages.Add(new TabPage(TabPageType.PreviousSet,"", 50, FirstPageInSet - SetSize, false, false));
          iFillerWidth -= 50;
        }

        if (TabPages.Count - 1 > this.LastPageInSet)
        {
          aPages.Add(new TabPage(TabPageType.NextSet,"", 50, this.LastPageInSet + 1, false, false));
          iFillerWidth -= 50;
        }

        iFillerWidth -= 6; /// Left separator.
                           /// 
        /// See if there is need for a filler.
        for (int i = 0; i < aPages.Count; i++)
        {
          iFillerWidth -= Convert.ToInt32(TabPages[i].Width.Value);
          if (i > 0)
          {
            iFillerWidth -= 6; /// Separator.
          }
        }

      
        if (iFillerWidth > 0)
        {
          aPages.Add(new TabPage(TabPageType.Filler,"<img src=\"/images/layout/blank.gif\" />",iFillerWidth));
        }


        /// Render the tab headers.
        for (int i = 0; i < aPages.Count; i++)
        {
          aPages[i].FirstPage = (i == 0);
          aPages[i].LastPage  = (i == aPages.Count - 1) || aPages[i+1].PageType == TabPageType.Filler;
          aPages[i].PostBackEventReference = Page.ClientScript.GetPostBackEventReference(this,Convert.ToString(aPages[i].PageIndex));
          aPages[i].RenderControl(writer);
        }

        writer.RenderEndTag(); /// TR header

        writer.AddAttribute(HtmlTextWriterAttribute.Height, "100%");
        writer.RenderBeginTag(HtmlTextWriterTag.Tr); // TR Body

        writer.AddAttribute(HtmlTextWriterAttribute.Colspan, Convert.ToString((aPages.Count * 2) + 1));
        writer.AddAttribute(HtmlTextWriterAttribute.Class, "TabsheetBody");

        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        if (this.OnRenderPage != null)
        {
          OnRenderPage(writer, EventArgs.Empty);
        }
        writer.RenderEndTag(); /// TD
        writer.RenderEndTag(); /// TR Body
        writer.RenderEndTag(); /// TABLE
    }

    public void RaisePostBackEvent(string eventArgument)
    {
      SelectedPageIndex = Convert.ToInt32(eventArgument);

      CurrentPage.OnClick(EventArgs.Empty);
    }



	}
}
