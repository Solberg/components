﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace SSL.Web.UI.WebControls
{
    public interface IValidator : System.Web.UI.IValidator
    {
        void BaseRender(HtmlTextWriter writer);
    }
}
