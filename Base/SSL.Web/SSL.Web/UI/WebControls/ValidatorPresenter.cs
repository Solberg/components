﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SSL.Web.UI.WebControls
{
    public class ValidatorPresenter
    {
        #region variables
        private BaseValidator _validator;
        #endregion

        #region constructor



        public ValidatorPresenter(BaseValidator validator)
        {
            _validator = validator;
        }

        #endregion

        public void PreRender()
        {

            if (_validator.EnableClientScript
                || !_validator.IsValid)
            {
                Image img = new Image();
                img.ImageUrl = "/images/error.gif";
                _validator.Controls.Add(img);
            }

        }

        /// <summary>
        /// Som default udskriver standard RequiredFieldValidator ingenting, hvis
        /// ClientScript ikke er enabled. Dette er ikke hvad vi ønsker, da kontrol
        /// bruges som spacer. Derfor udskriver vi selv Span og indhold hvis
        /// EnableClientScript er false.
        /// </summary>
        /// <param name="writer"></param>
        public void Render(HtmlTextWriter writer)
        {
            _validator.ToolTip = _validator.ErrorMessage;

            if (_validator.EnableClientScript || !_validator.IsValid) /// Hvis ClientScript er enabled eller der er fejl
            {
                ((IValidator)_validator).BaseRender(writer);
            }
            else /// Udskriv selv span og indhold.
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.AddAttribute(HtmlTextWriterAttribute.Src, "/images/btn/error.gif");
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }
        }
    }
}
