using System;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using ALKA.Exceptions;

namespace ALKA.Collections
{
	/// <summary>
	/// Indeholder funktioner til behandling af Arrays.
	/// </summary>
	public class Array
	{
		/// <summary>
		/// Konverter StringArray til ArrayList.
		/// </summary>
		/// <param name="Ia"></param>
		/// <returns></returns>
		public static ArrayList ToList(string[] Ia)
		{
			try
			{
				ArrayList a = new ArrayList();
				a.AddRange(Ia);
				return a;
			}
			catch (Exception exc)
			{
				throw new FunctionException(MethodInfo.GetCurrentMethod(), exc.Message);
			}
		}

   

		/// <summary>
		/// Kopier indhold af en ArrayList til et endiminsionelt string array.
		/// </summary>
		/// <param name="Ia"></param>
		/// <returns>Endimensionelt array</returns>
		public static string[] FromList(ArrayList Ia)
		{
			string[] a = new string[Ia.Count];

			Ia.CopyTo(a);

			return a;
		}
	}
}
