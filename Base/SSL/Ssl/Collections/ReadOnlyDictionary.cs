﻿using System;
using System.Collections.Generic;

namespace SSL.Collections
{
  /// <summary>
  /// A readonly Dictionary
  /// </summary>
  /// <typeparam name="TKey">The type of the key.</typeparam>
  /// <typeparam name="TValue">The type of the value.</typeparam>
  public class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>
  {
    private readonly IDictionary<TKey, TValue> _dictionary;

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlyDictionary&lt;TKey, TValue&gt;"/> class.
    /// </summary>
    public ReadOnlyDictionary()
    {
      _dictionary = new Dictionary<TKey, TValue>();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlyDictionary&lt;TKey, TValue&gt;"/> class.
    /// </summary>
    /// <param name="dictionary">The dictionary.</param>
    public ReadOnlyDictionary(IDictionary<TKey, TValue> dictionary)
    {
      _dictionary = dictionary;
    }

    #region IDictionary<TKey,TValue> Members

    /// <summary>
    /// Adds the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    public void Add(TKey key, TValue value)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    /// <summary>
    /// Determines whether the specified key contains key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns>
    ///   <c>true</c> if the specified key contains key; otherwise, <c>false</c>.
    /// </returns>
    public bool ContainsKey(TKey key)
    {
      return _dictionary.ContainsKey(key);
    }

    /// <summary>
    /// Gets the keys.
    /// </summary>
    public ICollection<TKey> Keys
    {
      get { return _dictionary.Keys; }
    }

    /// <summary>
    /// Removes the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    public bool Remove(TKey key)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    /// <summary>
    /// Tries the get value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public bool TryGetValue(TKey key, out TValue value)
    {
      return _dictionary.TryGetValue(key, out value);
    }

    /// <summary>
    /// Gets the values.
    /// </summary>
    public ICollection<TValue> Values
    {
      get { return _dictionary.Values; }
    }

    /// <summary>
    /// Gets or sets the <typeparamref name="TValue"/> with the specified key.
    /// </summary>
    public TValue this[TKey key]
    {
      get
      {
        return _dictionary[key];
      }
      set
      {
        throw new NotSupportedException("This dictionary is read-only");
      }
    }

    #endregion

    #region ICollection<KeyValuePair<TKey,TValue>> Members

    /// <summary>
    /// Adds the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    public void Add(KeyValuePair<TKey, TValue> item)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    /// <summary>
    /// Clears this instance.
    /// </summary>
    public void Clear()
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    /// <summary>
    /// Determines whether [contains] [the specified item].
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns>
    ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
    /// </returns>
    public bool Contains(KeyValuePair<TKey, TValue> item)
    {
      return _dictionary.Contains(item);
    }

    /// <summary>
    /// Copies to.
    /// </summary>
    /// <param name="array">The array.</param>
    /// <param name="arrayIndex">Index of the array.</param>
    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      _dictionary.CopyTo(array, arrayIndex);
    }

    /// <summary>
    /// Gets the count.
    /// </summary>
    public int Count
    {
      get { return _dictionary.Count; }
    }

    /// <summary>
    /// Gets a value indicating whether this instance is read only.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
    /// </value>
    public bool IsReadOnly
    {
      get { return true; }
    }

    /// <summary>
    /// Removes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns></returns>
    public bool Remove(KeyValuePair<TKey, TValue> item)
    {
      throw new NotSupportedException("This dictionary is read-only");
    }

    #endregion

    #region IEnumerable<KeyValuePair<TKey,TValue>> Members

    /// <summary>
    /// Gets the enumerator.
    /// </summary>
    /// <returns></returns>
    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      return _dictionary.GetEnumerator();
    }

    #endregion

    #region IEnumerable Members

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return (_dictionary as System.Collections.IEnumerable).GetEnumerator();
    }

    #endregion
  }
}
