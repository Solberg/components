using System;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using ALKA.Exceptions;

namespace ALKA.Collections
{
	/// <summary>
	/// Summary description for StringCollection.
	/// </summary>
	public class StringCollection
	{
    /// <summary>
    /// Kopier indhold af en StringCollection til et endiminsionelt string array.
    /// </summary>
    /// <param name="Ia"></param>
    /// <returns>Endimensionelt array</returns>
    public static string[] ToArray(System.Collections.Specialized.StringCollection Ia)
    {
      try
      {
        string[] a = new string[Ia.Count];

        Ia.CopyTo(a,0);

        return a;
      }
      catch (Exception exc)
      {
        throw new FunctionException(MethodInfo.GetCurrentMethod(), exc.Message);
      }
    }

    /// <summary>
    /// Konverter StringArray til StringCollection.
    /// </summary>
    /// <param name="Ia"></param>
    /// <returns></returns>
    public static System.Collections.Specialized.StringCollection FromArray(string[] Ia)
    {
      try
      {
        System.Collections.Specialized.StringCollection a = new System.Collections.Specialized.StringCollection();
        a.AddRange(Ia);
        return a;
      }
      catch (Exception exc)
      {
        throw new FunctionException(MethodInfo.GetCurrentMethod(), exc.Message);
      }
    }

  }
}
