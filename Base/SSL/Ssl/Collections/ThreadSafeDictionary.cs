﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using SSL.Threading;

namespace SSL.Collections
{
  /// <summary>
  /// A Thread Safe Dictionary 
  /// </summary>
  /// <typeparam name="TKey">The type of the key.</typeparam>
  /// <typeparam name="TValue">The type of the value.</typeparam>
  [Serializable]
  public class ThreadSafeDictionary<TKey, TValue> : IThreadSafeDictionary<TKey, TValue>, IDisposable
  {
    //This is the internal dictionary that we are wrapping
    private readonly IDictionary<TKey, TValue> _dict = new Dictionary<TKey, TValue>();


    [NonSerialized]
    private readonly ReaderWriterLockSlim _dictionaryLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion); //setup the lock;


    /// <summary>
    /// This is a blind remove. Prevents the need to check for existence first.
    /// </summary>
    /// <param name="key">Key to remove</param>
    public void RemoveSafe(TKey key)
    {
      using (new ReadLock(_dictionaryLock))
      {
        if (_dict.ContainsKey(key))
        {
          using (new WriteLock(_dictionaryLock))
          {
            _dict.Remove(key);
          }
        }
      }
    }

    /// <summary>
    /// Merge does a blind remove, and then add.  Basically a blind Upsert.  
    /// </summary>
    /// <param name="key">Key to lookup</param>
    /// <param name="newValue">New Value</param>
    public void MergeSafe(TKey key, TValue newValue)
    {
      using (new WriteLock(_dictionaryLock)) // take a writelock immediately since we will always be writing
      {
        if (_dict.ContainsKey(key))
        {
          _dict.Remove(key);
        }

        _dict.Add(key, newValue);
      }
    }

    /// <summary>
    /// Removes the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns></returns>
    public virtual bool Remove(TKey key)
    {
      using (new WriteLock(_dictionaryLock))
      {
        return _dict.Remove(key);
      }
    }

    /// <summary>
    /// Determines whether the specified key contains key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <returns>
    ///   <c>true</c> if the specified key contains key; otherwise, <c>false</c>.
    /// </returns>
    public virtual bool ContainsKey(TKey key)
    {
      using (new ReadOnlyLock(_dictionaryLock))
      {
        return _dict.ContainsKey(key);
      }
    }

    /// <summary>
    /// Tries the get value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public virtual bool TryGetValue(TKey key, out TValue value)
    {
      using (new ReadOnlyLock(_dictionaryLock))
      {
        return _dict.TryGetValue(key, out value);
      }
    }

    /// <summary>
    /// Gets or sets the <typeparamref name="TValue"/> with the specified key.
    /// </summary>
    public virtual TValue this[TKey key]
    {
      get
      {
        using (new ReadOnlyLock(_dictionaryLock))
        {
          return _dict[key];
        }
      }
      set
      {
        using (new WriteLock(_dictionaryLock))
        {
          _dict[key] = value;
        }
      }
    }

    /// <summary>
    /// Gets the keys.
    /// </summary>
    public virtual ICollection<TKey> Keys
    {
      get
      {
        using (new ReadOnlyLock(_dictionaryLock))
        {
          return new List<TKey>(_dict.Keys);
        }
      }
    }

    /// <summary>
    /// Gets the values.
    /// </summary>
    public virtual ICollection<TValue> Values
    {
      get
      {
        using (new ReadOnlyLock(_dictionaryLock))
        {
          return new List<TValue>(_dict.Values);
        }
      }
    }

    /// <summary>
    /// Clears this instance.
    /// </summary>
    public virtual void Clear()
    {
      using (new WriteLock(_dictionaryLock))
      {
        _dict.Clear();
      }
    }

    /// <summary>
    /// Gets the count.
    /// </summary>
    public virtual int Count
    {
      get
      {
        using (new ReadOnlyLock(_dictionaryLock))
        {
          return _dict.Count;
        }
      }
    }

    /// <summary>
    /// Determines whether [contains] [the specified item].
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns>
    ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
    /// </returns>
    public virtual bool Contains(KeyValuePair<TKey, TValue> item)
    {
      using (new ReadOnlyLock(_dictionaryLock))
      {
        return _dict.Contains(item);
      }
    }

    /// <summary>
    /// Adds the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    public virtual void Add(KeyValuePair<TKey, TValue> item)
    {
      using (new WriteLock(_dictionaryLock))
      {
        _dict.Add(item);
      }
    }

    /// <summary>
    /// Adds the specified key.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    public virtual void Add(TKey key, TValue value)
    {
      using (new WriteLock(_dictionaryLock))
      {
        _dict.Add(key, value);
      }
    }

    /// <summary>
    /// Removes the specified item.
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns></returns>
    public virtual bool Remove(KeyValuePair<TKey, TValue> item)
    {
      using (new WriteLock(_dictionaryLock))
      {
        return _dict.Remove(item);
      }
    }

    /// <summary>
    /// Copies to.
    /// </summary>
    /// <param name="array">The array.</param>
    /// <param name="arrayIndex">Index of the array.</param>
    public virtual void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      using (new ReadOnlyLock(_dictionaryLock))
      {
        _dict.CopyTo(array, arrayIndex);
      }
    }

    /// <summary>
    /// Gets a value indicating whether this instance is read only.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is read only; otherwise, <c>false</c>.
    /// </value>
    public virtual bool IsReadOnly
    {
      get
      {
        using (new ReadOnlyLock(_dictionaryLock))
        {
          return _dict.IsReadOnly;
        }
      }
    }

    /// <summary>
    /// Gets the enumerator.
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      throw new NotSupportedException("Cannot enumerate a threadsafe dictionary.  Instead, enumerate the keys or values collection");
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      throw new NotSupportedException("Cannot enumerate a threadsafe dictionary.  Instead, enumerate the keys or values collection");
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      _dictionaryLock.Dispose();
    }
  }
}
