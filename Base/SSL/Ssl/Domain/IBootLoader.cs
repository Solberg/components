﻿using System;

namespace SSL.Domain
{
  public interface IBootLoader : IDisposable
  {
    void Boot();
  }
}