﻿namespace SSL.Domain
{
  public interface IFilter<T> where T : class
  {
    T Filter(T item);
  }
}