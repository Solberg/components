﻿using System;
using System.Collections.Generic;

namespace SSL.Domain
{
    public interface ILogger<T> where T : LogEntry
    {
        void Log(T entry);

      /// <summary>
      /// List all entries from a period.
      /// </summary>
      /// <param name="from"></param>
      /// <param name="to"></param>
      /// <returns></returns>
      List<T> List(DateTime? from, DateTime? to);

      /// <summary>
      /// Select a single entry by id
      /// </summary>
      /// <param name="i"></param>
      /// <returns></returns>
      T Fetch(int i);

      /// <summary>
      /// Delete an entry
      /// </summary>
      /// <param name="id">id of the entry</param>
      /// <returns></returns>
      void Delete(int id);

    }
}
