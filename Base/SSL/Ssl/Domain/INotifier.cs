﻿namespace SSL.Domain
{
  public interface INotifier
  {

    /// <summary>
    /// Notify and wait for result
    /// </summary>
    /// <param name="notification"></param>
    /// <returns></returns>
    bool Notify(Notification notification);

    /// <summary>
    /// Notify don't wait for result.
    /// </summary>
    /// <param name="notification"></param>
    /// <returns></returns>
    bool NotifyAsync(Notification notification);

  }
}