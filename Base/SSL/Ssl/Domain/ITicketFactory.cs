﻿namespace SSL.Domain
{
  public interface ITicketFactory
  {

    string GetTicket();

  }
}