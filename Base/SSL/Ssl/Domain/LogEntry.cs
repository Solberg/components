﻿using System;
using System.Runtime.Serialization;

namespace SSL.Domain
{

  [DataContract]
  public class LogEntry
  {
    /// <summary>
    /// Unique id for entry
    /// </summary>
    [DataMember]
    public int? Id { get; set; }

    [DataMember]
    public string Message { get; private set; }

    [DataMember]
    public DateTime? Timestamp { get; private set; }

    public LogEntry(int? id, string message, DateTime? timestamp)
      : this(message, timestamp)
    {
      Id = id;
    }

    public LogEntry(string message, DateTime? timestamp)
    {
      Message = message;
      Timestamp = timestamp;
    }
  }
}
