﻿namespace SSL.Domain
{
  public class Notification
  {
    public Notification(string header, string body)
    {
      Body = body;
      Header = header;
    }

    public string Header { get; private set; }

    public string Body { get; private set; }


  }
}
