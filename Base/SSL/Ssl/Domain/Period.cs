﻿using System;

namespace SSL.Domain
{
  public class Period
  {
    public Period(DateTime? from, DateTime? to)
    {
      To = to;
      From = from;
    }

    public  DateTime? From { get; private set; }

    public DateTime? To { get; private set; }

    
  }
}
