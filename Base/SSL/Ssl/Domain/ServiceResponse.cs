﻿using System.Runtime.Serialization;

namespace SSL.Domain
{

  [DataContract]
  public class ServiceResponse<T>
  {

    [DataMember]
    public int Status { get; set; }

    [DataMember]
    public string Message { get; set; }

    [DataMember]
    public T Data { get; set; }

    public ServiceResponse(T data, int status, string message = null)
    {
      Status = status;
      Data = data;
      Message = message;
    }

  }
}
