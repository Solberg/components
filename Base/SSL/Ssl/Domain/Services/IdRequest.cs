﻿using System.Runtime.Serialization;

namespace SSL.Domain.Services
{

    /// <summary>
    /// Request with a Id and a ticket.
    /// </summary>
    [DataContract]
    public class IdRequest<T>
    {

        /// <summary>
        /// Id for an entry.
        /// </summary>
        [DataMember]
        public T Id { get; set; }

        /// <summary>
        /// Ticket from client
        /// </summary>
        [DataMember]
        public string Ticket { get; set; }
    }
}