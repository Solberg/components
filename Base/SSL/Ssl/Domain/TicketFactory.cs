﻿using System;
using SSL.State;

namespace SSL.Domain
{
  public class TicketFactory : ITicketFactory
  {

    private readonly IState _state;

    public TicketFactory(IState state)
    {
      _state = state;
    }

    public string GetTicket()
    {
      var ticket = _state.Get("Ticket")!= null ? Convert.ToString(_state.Get("Ticket")) : Guid.NewGuid().ToString();

      if (_state.Get("Ticket") == null)
      {
        _state.Set("Ticket", ticket);
      }

      return ticket;
    }

  }
}