﻿using System;
using System.Net.Mail;
using SSL.Domain;

namespace SSL
{
  public class EmailNotifier : INotifier
  {

    private readonly string _notifyTo;

    private readonly string _host;

    private readonly string _mailFrom;

    private readonly ILogger<LogEntry> _logger;

    public EmailNotifier(string mailFrom, string notifyTo, string host, ILogger<LogEntry> logger)
    {
      _notifyTo = notifyTo;
      _host = host;
      _logger = logger;
      _mailFrom = mailFrom;
    }


    public bool Notify(Notification notification)
    {
      try
      {
        var client = new SmtpClient(_host);
        client.Send(new MailMessage(_mailFrom, _notifyTo, notification.Header, notification.Body));
        return true;
      }
      catch (SmtpException exc)
      {
        _logger.Log(new LogEntry(exc.Message, DateTime.Now));
        return false;
      }
    }

    public bool NotifyAsync(Notification notification)
    {
      try
      {
        var client = new SmtpClient(_host);
        client.SendCompleted += client_SendCompleted;
        client.SendAsync(new MailMessage(_mailFrom, _notifyTo, notification.Header, notification.Body), notification.Header);
        return true;
      }
      catch (SmtpException exc)
      {
        _logger.Log(new LogEntry(exc.Message, DateTime.Now));
        return false;
      }
    }

    void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
      // Get the unique identifier for this asynchronous operation.
      var token = (string)e.UserState;

      if (e.Cancelled)
      {
        _logger.Log(new LogEntry(string.Format("[{0}] Send canceled.", token), DateTime.Now));
      }
      if (e.Error != null)
      {
        _logger.Log(new LogEntry(string.Format("[{0}] {1}", token, e.Error), DateTime.Now));
      }
    }
  }
}
