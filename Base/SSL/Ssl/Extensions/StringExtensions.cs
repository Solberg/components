﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSL.Extensions
{
  public static class StringExtensions
  {

    public static int? ToNullableInt32(this string s)
    {
      if (string.IsNullOrEmpty(s))
      {
        return null;
      }

      try
      {
        return Convert.ToInt32(s);
      }
      catch (Exception)
      {
        return null;
      }
    }

  }
}
