﻿using System;
using System.Globalization;
using System.Xml.Linq;

namespace SSL.Extensions
{
  public static class XElementExtension
  {

    public static DateTime? ToNullableDateTime(this XElement element)
    {
      if (element == null || string.IsNullOrEmpty(element.Value))
      {
        return null;
      }

      DateTime dateTime;

      if (
        ! DateTime.TryParseExact(element.Value, "dd.MM.yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None,
          out dateTime))
      {
        return null;
      }

      return dateTime;
    }

    public static string ToStr(this XElement element)
    {
      if (element == null)
      {
        return null;
      }
      return element.Value;
    }



    public static int? ToNullableInt32(this XElement element)
    {
      int value;

      if (element == null || string.IsNullOrEmpty(element.Value))
      {
        return null;
      }

      if (int.TryParse(element.Value, out value))
      {
        return value;
      }
      return null;
      
    }

    public static int ToInt32(this XElement element)
    {
      int value;

      if (element == null || string.IsNullOrEmpty(element.Value))
      {
        return 0;
      }

      if (int.TryParse(element.Value, out value))
      {
        return value;
      }
      return 0;
    }

  }
}
