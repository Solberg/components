﻿namespace SSL
{

  /// <summary>
  /// Map one path to another
  /// </summary>
  public interface IPathMapper
  {

    /// <summary>
    /// MapPath
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    string MapPath(string path);


  }
}