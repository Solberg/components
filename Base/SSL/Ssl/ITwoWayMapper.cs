﻿namespace SSL
{
  public interface ITwoWayMapper<TFrom, TTo>
  {

    TTo Map(TFrom data);

    TFrom Map(TTo data);

  }
}