﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SSL.Io
{
  public class FileService : IFileService
  {
    public bool Exists(string path)
    {
      return File.Exists(path);
    }

    public IEnumerable<string> ListFiles(string path, string mask = "*.*",  bool recursive = false)
    {
      return Directory.GetFiles(path, mask, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).ToList();
    }

    public void Copy(string from, string to)
    {
      File.Copy(from, to);

    }

  }
}
