﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSL.Io
{
  public interface IFileService
  {

    bool Exists(string path);

    IEnumerable<string> ListFiles(string path, string mask = "*.*",  bool recursive = false);

    void Copy(string from, string to);

  }
}
