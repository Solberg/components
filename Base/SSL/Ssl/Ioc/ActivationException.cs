﻿using System;

namespace SSL.Ioc
{
  /// <summary>
    /// 
    /// </summary>
    /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
    public class ActivationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivationException"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public ActivationException(Exception exception)
            : base("activation falied", exception)
        {}
    }
}
