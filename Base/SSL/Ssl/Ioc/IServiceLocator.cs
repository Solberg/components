﻿using System;
using System.Collections.Generic;

namespace SSL.Ioc
{
  /// <summary>
    /// ServiceLocator use to find and resolve services 
    /// </summary>
    /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
    public interface IServiceLocator
    {
        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        TService GetInstance<TService>();

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        TService GetInstance<TService>(string key);

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="serviceType">The type.</param>
        /// <returns></returns>
        object GetInstance(Type serviceType);

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="serviceType">The type.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        object GetInstance(Type serviceType, string key);

        /// <summary>
        /// Gets all instances.
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        IEnumerable<TService> GetAllInstances<TService>();

        /// <summary>
        /// Gets all instances.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        IEnumerable<object> GetAllInstances(Type serviceType);
    }
}
