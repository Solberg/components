﻿using System;
using System.Collections.Generic;

namespace SSL.Ioc
{
  /// <summary>
    /// ServiceLocator use to find and resolve services
    /// </summary>
    /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
    public static class ServiceLocator
    {
        private static IServiceLocator _locator;

        /// <summary>
        /// Sets the locator.
        /// </summary>
        /// <param name="serviceLocator">The service locator.</param>
        public static void SetLocator(IServiceLocator serviceLocator)
        {
            _locator = new ServiceLocatorAdapter(serviceLocator);
        }

        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>The current.</value>
        public static IServiceLocator Current
        {
            get { return _locator; }
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public static TService GetInstance<TService>()
        {
            return _locator.GetInstance<TService>();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static TService GetInstance<TService>(string key)
        {
            return _locator.GetInstance<TService>(key);
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="serviceType">The type.</param>
        /// <returns></returns>
        public static object GetInstance(Type serviceType)
        {
            return _locator.GetInstance(serviceType);
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static object GetInstance(Type serviceType, string key)
        {
            return _locator.GetInstance(serviceType, key);
        }

        /// <summary>
        /// Gets all instances.
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <returns></returns>
        public static IEnumerable<TService> GetAllInstances<TService>()
        {
            return _locator.GetAllInstances<TService>();
        }

        /// <summary>
        /// Gets all instances.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        public static IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _locator.GetAllInstances(serviceType);
        }
    }
}
