﻿using System;
using System.Collections.Generic;

namespace SSL.Ioc
{
  /// <summary>
  /// 
  /// </summary>
  /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
  internal class ServiceLocatorAdapter : IServiceLocator
    {
        private readonly IServiceLocator serviceLocator;

        public ServiceLocatorAdapter(IServiceLocator serviceLocator)
        {
            this.serviceLocator = serviceLocator;
        }

        public TService GetInstance<TService>()
        {
            try
            {
               return serviceLocator.GetInstance<TService>();
            }
            catch (Exception exception)
            {
                throw new ActivationException(exception);
            }

        }

        public TService GetInstance<TService>(string key)
        {
            try
            {
                return serviceLocator.GetInstance<TService>(key);
            }
            catch (Exception exception)
            {
                throw new ActivationException(exception);
            }
        }

        public object GetInstance(Type serviceType)
        {
            try
            {
                return serviceLocator.GetInstance(serviceType);
            }
            catch (Exception exception)
            {
                throw new ActivationException(exception);
            }
        }

        public object GetInstance(Type serviceType, string key)
        {
            try
            {
                return serviceLocator.GetInstance(serviceType, key);
            }
            catch (Exception exception)
            {
                throw new ActivationException(exception);
            }
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            try
            {
                return serviceLocator.GetAllInstances<TService>();
            }
            catch (Exception exception)
            {
                throw new ActivationException(exception);
            }
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            try
            {
                return serviceLocator.GetAllInstances(serviceType);
            }
            catch (Exception exception)
            {
                throw new ActivationException(exception);
            }
        }
    }
}
