﻿using System;
using System.Globalization;
using SSL.Reflection;

namespace SSL.Mvp
{
  /// <summary>
  /// Standard factory for presenters. Bruger ObjectFactory til at oprette instansen
  /// </summary>
  public class DefaultPresenterFactory : IPresenterFactory
  {
    // ReSharper disable PossibleNullReferenceException

    /// <summary>
    /// Creates the specified view.
    /// </summary>
    /// <typeparam name="TPresenter">The type of the presenter.</typeparam>
    /// <param name="view">The view.</param>
    /// <returns></returns>
    public TPresenter Create<TPresenter>(IView view) where TPresenter : class, IPresenter
    {
      if (view == null)
      {
        throw new ArgumentNullException("view");
      }

      Type presenterType = typeof(TPresenter);

      try
      {
        var presenter = ObjectFactory.CreateInstance<TPresenter>(view);

        if (presenter == null)
        {
          throw new Exception("creating the presenter failed");
        }

        return presenter;
      }
      catch (Exception ex)
      {
        var originalException = ex;

        throw new InvalidOperationException
        (
            string.Format(
                CultureInfo.InvariantCulture,
                "An exception was thrown whilst trying to create an instance of {0}. Check the InnerException for more information.",
                presenterType.FullName),
            originalException
        );
      }
    }
    // ReSharper restore PossibleNullReferenceException
  }
}
