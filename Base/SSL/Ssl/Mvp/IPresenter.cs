﻿namespace SSL.Mvp
{
  /// <summary>
  /// Presenter til mvp model.
  /// </summary>
  /// <programmer version="3.5.0." date="20121106" programmer="SSL" />
  public interface IPresenter
  {

    /// <summary>
    /// Gets the view.
    /// </summary>
    /// <value>The view.</value>
    IView View { get; }

    /// <summary>
    /// Hook in når presenter loades.
    /// </summary>
    /// <param name="initializing"></param>
    void OnLoad(bool initializing);

  }

  /// <summary>
  /// Gennerick Presenter interface
  /// </summary>
  /// <typeparam name="TView">The type of the view.</typeparam>
  public interface IPresenter<out TView> : IPresenter
  {
    /// <summary>
    /// Gets the view.
    /// </summary>
    new TView View { get; }

  }


}