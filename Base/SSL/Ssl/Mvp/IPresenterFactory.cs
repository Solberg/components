﻿namespace SSL.Mvp
{
  /// <summary>
  /// Presenter factory interface
  /// </summary>
  public interface IPresenterFactory
  {
    /// <summary>
    /// Opretter presenter der håndterer view.
    /// </summary>
    /// <typeparam name="TPresenter">Presenter type.</typeparam>
    /// <param name="view">View der håndteres af presenter.</param>
    /// <returns></returns>
    TPresenter Create<TPresenter>(IView view) where TPresenter : class, IPresenter;
  }
}