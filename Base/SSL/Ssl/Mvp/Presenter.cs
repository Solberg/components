﻿namespace SSL.Mvp
{
  /// <summary>
  /// Base for alle presenters der benyttes i forbindelse med mvp
  /// </summary>
  /// <programmer version="3.5.0." date="20121106" programmer="SSL" />
  public class Presenter : IPresenter
  {

    #region Variables

    /// <summary>
    /// View der håndteres af presenter.
    /// </summary>
    private readonly IView view;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Presenter"/> class.
    /// </summary>
    /// <param name="view">The view.</param>
    protected Presenter(IView view)
    {
      this.view = view;
    }

    #endregion

    #region IPresenter Members

    /// <summary>
    /// Gets the view.
    /// </summary>
    /// <value>The view.</value>
    public IView View
    {
      get { return view; }
    }

    public virtual void OnLoad(bool initializing)
    {
    }

    #endregion

  }

  /// <summary>
  /// En generisk abstract Presenter. Alle Presenters skal arve fra denne
  /// </summary>
  /// <typeparam name="TView">The type of the view.</typeparam>
  public abstract class Presenter<TView> : Presenter, IPresenter<TView> where TView : class, IView
  {

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Presenter&lt;TView&gt;"/> class.
    /// </summary>
    /// <param name="view">View der skal håndteres af presenter.</param>
    protected Presenter(TView view)
      : base(view)
    {
    }

    #endregion

    #region IPresenter<TView> Members

    /// <summary>
    /// Reference til view.
    /// </summary>
    /// <value>The view.</value>
    public new virtual TView View
    {
      get { return base.View as TView; }
    }

    #endregion

  }


}
