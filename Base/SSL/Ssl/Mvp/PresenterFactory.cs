﻿namespace SSL.Mvp
{
  /// <summary>
  /// En static Presenter factory. Bruges af alle view typer til at få et factory
  /// </summary>
  public static class PresenterFactory
  {
    private static IPresenterFactory _current;

    static PresenterFactory()
    {
      _current = new DefaultPresenterFactory();
    }

    /// <summary>
    /// Sets the presenter factory.
    /// </summary>
    /// <param name="presenterFactory">The presenter factory.</param>
    public static void SetPresenterFactory(IPresenterFactory presenterFactory)
    {
      _current = presenterFactory;
    }

    /// <summary>
    /// Gets the presenter factory.
    /// </summary>
    public static IPresenterFactory Current { get { return _current; } }
  }
}
