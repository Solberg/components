using System;

namespace SSL.Reflection
{
	/// <summary>
  /// Hent oplysninger fra AssemblyInfo.cs for et givent assembly
	/// </summary>
  /// <remarks>
  /// <programmer date="20030402">S�ren Solberg Larsen</programmer>
  /// <revision date=20040613>Property AssemblyCopyright tilf�jet.</revision>
  /// </remarks>
  public class Assembly
  {
    #region Variables
    private System.Reflection.Assembly a;
    #endregion

    #region Properties
    /// <summary>
    /// Assemblys versionid. Version angives i AssemblyInfo.cs for programmet.
    /// </summary>
    public string Version
		{
			get
			{
				System.Reflection.AssemblyName an = a.GetName();
				Version v = an.Version;
				return Convert.ToString(v);
			}
		}

    /// <summary>
    /// Assemblys navn
    /// </summary>
		public string Name
		{
			get
			{
				System.Reflection.AssemblyName an = a.GetName();
				return Convert.ToString(an.Name);
			}
		}

    /// <summary>
    /// Returner AssemblyProduct fra AssemblyInfo.cs
    /// </summary>
		public string AssemblyProduct
		{
			get
			{
				string s = String.Empty;

				object[] attrib = a.GetCustomAttributes(typeof(System.Reflection.AssemblyProductAttribute), false);
				foreach(object att in attrib) 
				{
					System.Reflection.AssemblyProductAttribute ast = (System.Reflection.AssemblyProductAttribute) att;
					s = ast.Product;
				}

				return s;
			}
		}


    /// <summary>
    /// Returner AssemblyCompany fra AssemblyInfo.cs
    /// </summary>
		public string AssemblyCompany
		{
			get
			{
				string s = String.Empty;

				object[] attrib = a.GetCustomAttributes(typeof(System.Reflection.AssemblyCompanyAttribute), false);

				foreach(object att in attrib) 
				{
					System.Reflection.AssemblyCompanyAttribute ast = (System.Reflection.AssemblyCompanyAttribute) att;
					s = ast.Company;
				}

				return s;
			}
		}

    /// <summary>
    /// Returner AssemblyDescription fra AssemblyInfo.
    /// </summary>
		public string AssemblyDescription
		{
			get
			{
				string s = "";

				object[] attrib = a.GetCustomAttributes(typeof(System.Reflection.AssemblyDescriptionAttribute), false);

				foreach(object att in attrib) 
				{
					System.Reflection.AssemblyDescriptionAttribute ast = (System.Reflection.AssemblyDescriptionAttribute) att;
					s = ast.Description;
				}

				return s;
			}
    }

    /// <summary>
    /// Returner AssemblyCopyright fra AssemblyInfo.
    /// </summary>
    public string AssemblyCopyright
    {
      get
      {
        string s = "";

        object[] attrib = a.GetCustomAttributes(typeof(System.Reflection.AssemblyCopyrightAttribute), false);

        foreach (object att in attrib)
        {
          System.Reflection.AssemblyCopyrightAttribute ast = (System.Reflection.AssemblyCopyrightAttribute)att;
          s = ast.Copyright;
        }

        return s;
      }
    }

    #endregion

    #region Constructors

    /// <summary>
    /// Constructor skal bruge object for det assembly der skal unders�ges.
    /// </summary>
    /// <param name="Ia"></param>
    public Assembly(System.Reflection.Assembly Ia)
		{
			a = Ia;
    }
    #endregion
  }
}
