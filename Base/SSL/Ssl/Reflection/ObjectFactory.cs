﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using SSL.Collections;

namespace SSL.Reflection
{
  /// <summary>
    /// A CompiledFactory
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parameters">The parameters.</param>
    /// <returns></returns>
    public delegate T CompiledFactory<out T>(params object[] parameters);

    /// <summary>
    /// Factory that can create all types off objects
    /// </summary>
    public static class ObjectFactory
    {
        private static readonly IDictionary<int, object> BuildMethodCache = new ThreadSafeDictionary<int, object>();
        private static readonly Type Argstypes = typeof(object[]);

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static object CreateInstance(Type type,params object[] parameters)
        {
            Type[] parameterTypes = parameters.Select(p => p.GetType()).ToArray();

            int hachcode = type.GetHashCode();

            unchecked
            {
                hachcode = parameterTypes.Aggregate(hachcode, (current, parameterType) => current ^ parameterType.GetHashCode());
            }

            return GetOrCreateValue(hachcode, () => CreateConstructorEmit<object>(type, parameterTypes)).Invoke(parameters); 
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static T CreateInstance<T>(params object[] parameters) where T : class
        {
            Type[] parameterTypes = parameters.Select(p => p.GetType()).ToArray();
            Type type = typeof(T);

            int hachcode = type.GetHashCode();

            unchecked
            {
                hachcode = parameterTypes.Aggregate(hachcode, (current, parameterType) => current ^ parameterType.GetHashCode());
            }

            return GetOrCreateValue(hachcode, () => CreateConstructorEmit<T>(type, parameterTypes)).Invoke(parameters);
        }

        /// <summary>
        /// Creates the factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parameterTypes">The parameter types.</param>
        /// <returns>A compiled factory delegate</returns>
        public static CompiledFactory<T> CreateFactory<T>(params Type[] parameterTypes) where T : class
        {
            return CreateConstructorEmit<T>(typeof(T), parameterTypes);
        }

        private static CompiledFactory<T> CreateConstructorEmit<T>(Type type, Type[] parameterTypes)
        {
            var dynMethod = new DynamicMethod("DM$OBJ_FACTORY_" + type.Name, type, new[] { Argstypes }, type);
            ILGenerator ilGen = dynMethod.GetILGenerator();

            var constructor = type.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance, null, parameterTypes, null);

            if (constructor == null)
            {
              throw new Exception("No constructor found");
            }

            for (int i = 0; i < parameterTypes.Length; i++)
            {
                ilGen.Emit(OpCodes.Ldarg_0);
                switch (i)
                {
                    case 0: ilGen.Emit(OpCodes.Ldc_I4_0); break;
                    case 1: ilGen.Emit(OpCodes.Ldc_I4_1); break;
                    case 2: ilGen.Emit(OpCodes.Ldc_I4_2); break;
                    case 3: ilGen.Emit(OpCodes.Ldc_I4_3); break;
                    case 4: ilGen.Emit(OpCodes.Ldc_I4_4); break;
                    case 5: ilGen.Emit(OpCodes.Ldc_I4_5); break;
                    case 6: ilGen.Emit(OpCodes.Ldc_I4_6); break;
                    case 7: ilGen.Emit(OpCodes.Ldc_I4_7); break;
                    case 8: ilGen.Emit(OpCodes.Ldc_I4_8); break;
                    default: ilGen.Emit(OpCodes.Ldc_I4, i); break;
                }
                ilGen.Emit(OpCodes.Ldelem_Ref);
                Type paramType = parameterTypes[i];
                ilGen.Emit(paramType.IsValueType ? OpCodes.Unbox_Any : OpCodes.Castclass, paramType);
            }
            // ReSharper disable AssignNullToNotNullAttribute
            ilGen.Emit(OpCodes.Newobj, constructor);
            // ReSharper restore AssignNullToNotNullAttribute
            ilGen.Emit(OpCodes.Ret);

            return (CompiledFactory<T>)dynMethod.CreateDelegate(typeof(CompiledFactory<T>));
        }

        private static TValue GetOrCreateValue<TValue>(int key, Func<TValue> createValueCallback)
        {
            object value;

            if (!BuildMethodCache.TryGetValue(key, out value))
            {
                value = createValueCallback();

                BuildMethodCache[key] = value;
            }

            return (TValue)value;
        }

    }
}
