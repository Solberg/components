﻿using System;
using System.Linq.Expressions;

namespace SSL.Reflection
{
  /// <summary>
  /// Diverse reflection utilities.
  /// </summary>
  public static class ReflectionUtility
  {

    /// <summary>
    /// Hent navn på en property via et lambda udtryk
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="expression"></param>
    /// <returns></returns>
    public static string GetPropertyName<T>(Expression<Func<T>> expression)
    {
      var body = (MemberExpression)expression.Body;
      return body.Member.Name;
    }
  }

}
