﻿using ALKA.Runtime.State;

namespace SSL.Runtime.State
{
    /// <summary>
    /// Interface implemented by application state providers that store and retrieve application state data.
    /// </summary>
    public interface IApplicationState : IState
    {
    }
}
