﻿using System;
using ALKA.Runtime.State;

namespace SSL.Runtime.State
{
    /// <summary>
    /// Interface implemented by cache state providers that store and retrieve state data for the cache.
    /// </summary>
    public interface ICacheState : IState
    {
        /// <summary>
        /// Puts state data into the cache state with the default key and absolute expiration policy.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        /// <param name="absoluteExpiration">The date and time when the data from the cache will be removed.</param>
        void Put<T>(T instance, DateTime absoluteExpiration);

        /// <summary>
        /// Puts state data into the cache state with the specified key with the specified absolute expiration.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="key">An object representing the unique key with which the data is stored.</param>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        /// <param name="absoluteExpiration">The date and time when the data from the cache will be removed.</param>
        void Put<T>(object key, T instance, DateTime absoluteExpiration);

        /// <summary>
        /// Puts state data into the cache state with the default key and sliding expiration policy.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        /// <param name="slidingExpiration">A time span representing the sliding expiration policy.</param>
        void Put<T>(T instance, TimeSpan slidingExpiration);

        /// <summary>
        /// Puts state data into the cache state with the specified key with the specified sliding expiration
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="key">An object representing the unique key with which the data is stored.</param>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        /// <param name="slidingExpiration">A <see cref="TimeSpan"/> specifying the sliding expiration policy.</param>
        void Put<T>(object key, T instance, TimeSpan slidingExpiration);
    }
}
