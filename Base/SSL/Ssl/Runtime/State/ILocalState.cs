﻿using ALKA.Runtime.State;

namespace SSL.Runtime.State
{
  /// <summary>
    /// Interface implemented by local state providers that store and retrieve state data for the current thread or operation context.
    /// </summary>
    /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
    public interface ILocalState : IState
    {
    }
}
