namespace SSL.Runtime.State
{
  /// <summary>
    /// Interface that is implemented by a custom selector that creates instances of <see cref="ILocalState"/>.
    /// </summary>
    /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
    public interface ILocalStateFactory
    {
        /// <summary>
        /// Gets the implementation of <see cref="ILocalState"/> to use.
        /// </summary>
        /// <returns></returns>
        ILocalState Create();
    }
}