﻿using ALKA.Runtime.State;

namespace SSL.Runtime.State
{
  /// <summary>
    /// Interface implemented by session state providers that store and retrieve state data for the current executing session.
    /// </summary>
    /// <programmer version="3.17.0" date="20140423" programmer="SSL" />
    public interface ISessionState : IState
    {
    }
}
