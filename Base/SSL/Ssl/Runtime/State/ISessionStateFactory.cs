using ALKA.Runtime.State;
using SSL.Runtime.State;

namespace Alka.Core.Runtime.State
{
    /// <summary>
    /// Interface that is implemented by a custom selector that creates instances of <see cref="ISessionState"/>.
    /// </summary>
    public interface ISessionStateFactory
    {
        /// <summary>
        /// Gets the implementation of <see cref="ISessionState"/> to use.
        /// </summary>
        /// <returns></returns>
        ISessionState Create();
    }
}