﻿using ALKA.Runtime.State;
using SSL.Runtime.State;

namespace Alka.Core.Runtime.State
{
    /// <summary>
    /// A State Provider.
    /// </summary>
    public interface IStateProvider
    {
        /// <summary>
        /// Gets the application specific state.
        /// </summary>
        IApplicationState Application { get; }

        /// <summary>
        /// Gets the session specific state.
        /// </summary>
        ISessionState Session { get; }

        /// <summary>
        /// Gets the cache specific state.
        /// </summary>
        ICacheState Cache { get; }

        /// <summary>
        /// Gets the thread local / request local specific state.
        /// </summary>
        ILocalState Local { get; }
    }
}
