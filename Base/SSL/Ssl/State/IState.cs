﻿namespace SSL.State
{
  public interface IState
  {

    object Get(string key);

    void Set(string key, object o);

  }
}