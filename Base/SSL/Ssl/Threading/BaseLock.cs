﻿using System;
using System.Threading;

namespace SSL.Threading
{
  /// <summary>
    /// Base class for all locks
    /// </summary>
    public abstract class BaseLock : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        protected ReaderWriterLockSlim Locks;


        /// <summary>
        /// Initializes a new instance of the <see cref="BaseLock"/> class.
        /// </summary>
        /// <param name="locks">The locks.</param>
        protected BaseLock(ReaderWriterLockSlim locks)
        {
            this.Locks = locks;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public abstract void Dispose();
    }
}
