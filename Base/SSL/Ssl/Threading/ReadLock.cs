﻿using System.Threading;

namespace SSL.Threading
{
  /// <summary>
    /// A ReadLock
    /// </summary>
    public class ReadLock : BaseLock
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadLock"/> class.
        /// </summary>
        /// <param name="locks">The locks.</param>
        public ReadLock(ReaderWriterLockSlim locks)
            : base(locks)
        {
            bool lockAcquired = false;
            while (!lockAcquired)
            {
                lockAcquired = locks.TryEnterUpgradeableReadLock(1);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            if (this.Locks.IsUpgradeableReadLockHeld)
            {
                this.Locks.ExitUpgradeableReadLock();
            }
        }
    }
}
