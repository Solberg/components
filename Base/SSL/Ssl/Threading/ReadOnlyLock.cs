﻿using System.Threading;

namespace SSL.Threading
{
  /// <summary>
    /// A ReadOnly Lock
    /// </summary>
    public class ReadOnlyLock : BaseLock
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyLock"/> class.
        /// </summary>
        /// <param name="locks">The locks.</param>
        public ReadOnlyLock(ReaderWriterLockSlim locks)
            : base(locks)
        {
            bool lockAcquired = false;
            while (!lockAcquired)
            {
                lockAcquired = locks.TryEnterReadLock(1);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            if (this.Locks.IsReadLockHeld)
            {
                this.Locks.ExitReadLock();
            }
        }
    }
}
