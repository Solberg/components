﻿using System.Threading;

namespace SSL.Threading
{
  /// <summary>
    /// A Write Lock
    /// </summary>
    public class WriteLock : BaseLock
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WriteLock"/> class.
        /// </summary>
        /// <param name="locks">The locks.</param>
        public WriteLock(ReaderWriterLockSlim locks)
            : base(locks)
        {
            bool lockAcquired = false;
            while (!lockAcquired)
            {
                lockAcquired = locks.TryEnterWriteLock(1);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            if (this.Locks.IsWriteLockHeld)
            {
                this.Locks.ExitWriteLock();
            }
        }
    }
}
