using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SSL.Xml
{

  [Obsolete("XmlDataDocument is obsolete")]
  public class XmlDataDocument : System.Xml.XmlDataDocument
  {
    #region Variables
    private string _XmlFileName = String.Empty;
    #endregion


    private string DataPath
    {
      get
      {
        return System.Configuration.ConfigurationManager.AppSettings["DataPath"];
      }
    }

    protected virtual string XmlFileName
    {
      get
      {
        return _XmlFileName;
      }

      set
      {
        _XmlFileName = value;
      }
    }

    /// <summary>
    /// Path to xmlfile could be c:\temp\data.xml
    /// </summary>
    protected string DataFilePath
    {
      get
      {
        return this.DataPath + this.XmlFileName + ".xml";
      }
    }

    /// <summary>
    /// Path to xmlschema could be c:\temp\data.xsd
    /// </summary>
    protected string SchemaFilePath
    {
      get
      {
        return this.DataPath + this.XmlFileName + ".xsd";
      }
    }

    /// <summary>
    /// Primary datatable in dataset.
    /// </summary>
    public virtual System.Data.DataTable MainTable
    {
      get
      {
        return this.DataSet.Tables[0];
      }
    }

    #region Constructors

    public XmlDataDocument(string IsFileName) : base()
    {
      _XmlFileName = IsFileName;
      using (StreamReader sr = new StreamReader(this.SchemaFilePath))
      {
        this.DataSet.ReadXmlSchema(sr);
        this.Load(this.DataFilePath);
      }

    }

    public XmlDataDocument() : base()
    {
      using (StreamReader sr = new StreamReader(this.SchemaFilePath))
      {
        this.DataSet.ReadXmlSchema(sr);
        this.Load(this.DataFilePath);
      }
    }
    #endregion

    #region Methods
    public void Save()
    {
      this.Save(this.DataFilePath);
    }
    #endregion


  }
}
