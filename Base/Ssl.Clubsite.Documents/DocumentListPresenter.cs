﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Documents
{
    using System.IO;
    using System.Web.UI.WebControls;

    public class DocumentListPresenter
    {
        #region Variables

        private IDocumentList view;

        #endregion

        #region Constructors

        public DocumentListPresenter(IDocumentList view)
        {
            this.view = view;
        }

        #endregion

        #region Public

        public void Initialize(TreeNodeEventArgs e)
        {
        }

        public void Populate(TreeNodeEventArgs e)
        {
            if (e.Node.ChildNodes.Count == 0)
            {
                LoadChildNode(e.Node);
            }
        }

        private void LoadChildNode(TreeNode node)
        {
            string docDir = view.DocumentPath;

            var directory = new DirectoryInfo(node.Value);

            foreach (DirectoryInfo sub in directory.GetDirectories())
            {
                var subNode = new TreeNode(sub.Name) { Value = sub.FullName };

                try
                {
                    if (sub.GetDirectories().Length > 0 || sub.GetFiles().Length > 0)
                    {
                        subNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                        subNode.PopulateOnDemand = true;
                        subNode.NavigateUrl = "#";
                    }
                }
                catch
                {
                    subNode.ImageUrl = "WebResource.axd?a=s&r=TreeView_XP_Explorer_ParentNode.gif&t=632242003305625000";
                }
                node.ChildNodes.Add(subNode);
            }

            foreach (FileInfo fi in directory.GetFiles())
            {
                var subNode = new TreeNode(fi.Name)
                {
                    Target = "_blank",
                    NavigateUrl =
                      String.Format("Showdocument.aspx?Path={0}", fi.FullName.Replace(docDir, String.Empty).Replace("\\", "/"))
                };

                if (fi.Extension == ".pdf")
                {
                    subNode.ImageUrl = "/images/icons/pdf.gif";
                }
                else if (fi.Extension == ".eml")
                {
                    subNode.ImageUrl = "/images/icons/eml.gif";
                }
                else if (fi.Extension == ".htm")
                {
                    subNode.ImageUrl = "/images/icons/htm.gif";
                }
                else if (fi.Extension == ".doc")
                {
                    subNode.ImageUrl = "/images/icons/doc.gif";
                }


                node.ChildNodes.Add(subNode);
            }
        }

        #endregion
    }
}
