﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Documents
{
    using System.Web;

    public interface IShowDocument
    {
        HttpResponse Response { get; }

        HttpRequest Request { get; }

        string DocumentPath { get; }
    }
}
