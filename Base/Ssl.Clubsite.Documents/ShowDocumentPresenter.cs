﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Documents
{
    public class ShowDocumentPresenter
    {
        #region Variables

        private IShowDocument view;

        #endregion

        #region Constructors

        public ShowDocumentPresenter(IShowDocument view)
        {
            this.view = view;
        }

        #endregion

        #region Public
        public void OnLoad()
        {
            if (view.Request.QueryString["Path"] != null)
            {
                string sDocumentPath =view.DocumentPath;
                sDocumentPath += view.Request.QueryString["Path"].Replace('/', '\\');

                if (System.IO.File.Exists(sDocumentPath))
                {
                    switch (new System.IO.FileInfo(sDocumentPath).Extension)
                    {
                        case ".pdf": view.Response.ContentType = "application/pdf";
                            break;
                        case ".doc": view.Response.ContentType = "application/msword";
                            break;
                        default: view.Response.ContentType = "text/plain";
                            break;
                    }
                    view.Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}", new System.IO.FileInfo(sDocumentPath).Name));
                    view.Response.ContentEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                    view.Response.Clear();
                    view.Response.TransmitFile(sDocumentPath);
                    view.Response.End();
                }
                else
                {
                    view.Response.Write("Document does not exist");
                }
            }

            
        }
        #endregion
    }
}
