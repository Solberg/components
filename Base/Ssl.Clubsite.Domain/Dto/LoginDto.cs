﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Domain.Dto
{
    public class LoginDto
    {
        public string LoginName { get; private set; }

        public int MemberId { get; private set; }

        public string MemberName { get; private set; }

        public bool Remember { get; private set; }

        public int Role { get; private set; }



        public LoginDto(int memberId, string memberName, string loginName, bool remember, int role)
        {
            this.LoginName = loginName;
            this.MemberId = memberId;
            this.Remember = remember;
            this.Role = role;
            this.MemberName = memberName;
        }

    }
}
