﻿using System;

namespace Ssl.Clubsite.Domain.Entity
{
    public class CalendarEntry
    {
        public int Id { get; private set; }
        public DateTime EventDate{ get; private set;}
        public DateTime StartTime{ get; private set;}
        public DateTime EndTime{ get; private set;}
        public string Description{ get; private set;}
        public int MemberId{ get; private set;}

        public CalendarEntry(int id, DateTime eventDate, DateTime startTime, DateTime endtime, string description, int memberId)
        {
            this.Id = id;
            this.EventDate = eventDate;
            this.StartTime = startTime;
            this.EndTime = endtime;
            this.Description = description;
            this.MemberId = memberId;
        }
    }
}
