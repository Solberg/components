﻿namespace Ssl.Clubsite.Domain.Entity
{
    public class Entity
    {

        #region Constructors
        public Entity(int id)
        {
            this.Id = id;
        }
        #endregion

        #region Properties
        public int Id
        {get; private set;
        }
        #endregion
    }
}
