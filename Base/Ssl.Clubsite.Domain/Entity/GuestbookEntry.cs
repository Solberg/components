﻿namespace Ssl.Clubsite.Domain.Entity
{
    using System;

    public class GuestbookEntry
    {
        #region Constructors
        public GuestbookEntry(DateTime stamp, string name, string email, string homepagetitle, string homepageUrl, int referredBy, string location, string addOn, string comment, bool privateEntry)
        {
            this.Stamp = stamp;
            this.Name = name;
            this.Email = email;
            this.HomepageTitle = homepagetitle;
            this.HomepageUrl = homepageUrl;
            this.ReferredBy = referredBy;
            this.Location = location;
            this.AddOn = addOn;
            this.Comment = comment;
            this.Private = privateEntry;
        }

        #endregion

        #region Properties

        public DateTime Stamp
        {
            get; private set;}

        public string Name { get; private set; }

        public string Email { get; private set; }

        public string HomepageTitle { get; private set; }

        public string HomepageUrl { get; private set; }

        public int ReferredBy { get; private set; }

        public string Location { get; private set; }

        public string AddOn { get; private set; }

        public string Comment { get; private set; }

        public bool Private { get; private set; }

        #endregion
    }
}
