﻿namespace Ssl.Clubsite.Domain.Entity
{
  public class Image
  {
    public int AlbumId { get; private set; }

    public int Id { get; private set;}

    public string IndexSource { get; private set; }

    public string Source { get; private set; }

    public string Description { get; private set; }

    #region Constructors
    public Image(int albumId, int id, string source, string indexSource, string description)
    {
      this.AlbumId = albumId;
      this.Id = id;
      this.Source = source;
      this.IndexSource = indexSource;
      this.Description = description;
    }
    #endregion
  }
}