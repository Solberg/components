﻿namespace Ssl.Clubsite.Domain.Entity
{
    using System;

    using Types;

    public class ImageAlbum
  {
    #region Constructors
    public ImageAlbum(int id, string name, string indexImage, string fullSizeImage, AccessLevel accessLevel, DateTime? date, int numberOfImages)
    {
      this.Id = id;
      this.Name = name;
      this.IndexImage = indexImage;
      this.AccessLevel = accessLevel;
      this.Date = date;
      this.NumberOfImages = numberOfImages;
      this.FullSizeImage = fullSizeImage;
    }
    #endregion

    public int Id { get; private set; }

    public string Name { get; private set; }

    public string IndexImage { get; private set; }

    public string FullSizeImage { get; private set; }

    public DateTime? Date { get; private set; }

    public AccessLevel AccessLevel { get; private set; }

    public int NumberOfImages { get; private set; }

  }
}