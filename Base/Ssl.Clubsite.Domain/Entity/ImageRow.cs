﻿namespace Ssl.Clubsite.Domain.Entity
{
    using System.Collections.Generic;

    public class ImageRow
  {
    public List<Image> Row { get; set; }

    #region Constructors
    public ImageRow()
    {
      this.Row = new List<Image>();
    }
    #endregion
  }
}