﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Domain.Entity
{
    public class Invitation
    {
        #region Properties
        public string Text { get; private set; }

        public DateTime? EventDate { get; private set; }

        public DateTime? PublishDate { get; private set; }

        public string Image { get; private set; }

        public string Document{get; private set;}

        #endregion

        #region Constructors
        public Invitation(string text, DateTime? eventDate, DateTime? publishDate, string image, string document)
        {
            Text = text;
            EventDate = eventDate;
            PublishDate = publishDate;
            Image = image;
            Document = document;
        }
        #endregion
    }
}
