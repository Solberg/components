﻿namespace Ssl.Clubsite.Domain.Entity
{
    public class Link : Entity
    {
        public string Name { get; private set; }

        public string Url { get; private set; }

        public string Description { get; private set; }

        public bool Active { get; private set; }

        public Link(int id, string name, string url, string description, bool active) : base(id)
        {
            this.Name = name;
            this.Url = url;
            this.Description = description;
            this.Active = active;
        }
    }
}
