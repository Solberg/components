﻿namespace Ssl.Clubsite.Domain.Entity
{
    using System;

    public class Member
    {
        public string LoginName { get; private set; }

        public int MemberId { get; private set; }

        public string MemberName { get; private set; }

        public bool Remember { get; private set; }

        public string Password{get; private set;}

        public int Role { get; private set; }

        public string Phone { get; private set; }

        public string Mail { get; private set; }

        public DateTime LastLogin { get; private set; }

        public string Address { get; private set; }

        public string PostCode { get;private set; }

        public string City { get; private set; }

        
        public Member()
        {
            this.LoginName = string.Empty;
            this.MemberId = 0;
            this.Remember = false;
            this.Role = 0;
            this.MemberName = string.Empty;
            this.Address = string.Empty;
            this.PostCode = string.Empty;
            this.City = string.Empty;
        }


        public Member(int memberId, string memberName, string address, string postCode, string city, string loginName, string password,  string phone, string mail, int role, DateTime lastLogin)
        {
            this.LoginName = loginName;
            this.MemberId = memberId;
            this.LastLogin = lastLogin;
            this.Role = role;
            this.MemberName = memberName;
            this.Address = address;
            this.PostCode = postCode;
            this.City = city;
            this.Mail = mail;
            this.Phone = phone;
            this.Password = password;

        }

    }
}
