﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Domain.Entity
{
    public class NewsEntry : Entity
    {
        #region Constructors
        public NewsEntry(int id, DateTime timeStamp, string text) : base(id)
        {
            this.TimeStamp = timeStamp;
            this.Text = text;
        }
        #endregion

        #region Properties
        public DateTime? TimeStamp { get; private set; }

        public string Text { get; private set; }

        #endregion
    }
}
