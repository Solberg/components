﻿using System.Collections.Generic;

namespace Ssl.Clubsite.Domain
{
    using Entity;

    using Types;

    public interface IAlbumRepository
    {
        IEnumerable<ImageAlbum> List(AccessLevel accessLevel);

        ImageAlbum Fetch(int id);

        IEnumerable<Image> ListImages(int album);
    }
}
