﻿using System;
using System.Collections.Generic;

namespace Ssl.Clubsite.Domain
{
    using Entity;

    public interface ICalendarRepository : ICreateRepository<CalendarEntry>
    {
        
        void Update(CalendarEntry calendarEntry);

        void Delete(int id);

        CalendarEntry Fetch(int id);

        List<CalendarEntry> List(DateTime startDate);
    }
}
