﻿namespace Ssl.Clubsite.Domain
{
  public interface ICreateRepository<in T> where T : class
  {
    void Create(T entry);
  }
}