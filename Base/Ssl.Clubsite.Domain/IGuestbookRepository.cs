﻿namespace Ssl.Clubsite.Domain
{
    using Entity;

    public interface IGuestbookRepository : IListRepository<GuestbookEntry>, ICreateRepository<GuestbookEntry>
    {
      
        int RecordCount();
    }
}
