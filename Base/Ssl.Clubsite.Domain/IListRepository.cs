﻿using System.Collections.Generic;

namespace Ssl.Clubsite.Domain
{
  public interface IListRepository<T> where T : class
  {

    /// <summary>
    /// List items in repository
    /// </summary>
    /// <returns></returns>
    List<T> List();

  }
}