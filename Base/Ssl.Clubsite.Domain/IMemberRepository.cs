﻿using System;
using System.Collections.Generic;

namespace Ssl.Clubsite.Domain
{
    using Entity;

    public interface IMemberRepository : ICreateRepository<Member>, IListRepository<Member>
    {

        Member FetchById(int id);

        bool FetchByLogin(string login, string password, out int id);

        void FetchIdFromLogin(string login, out int id);

        void FetchLastForumLogin(int id, out DateTime lastForumLogin);

        bool FetchPasswordFromMail(string mail, out string password);

        void UpdateLastLogin(int id, DateTime lastLogin);

        void UpdateLastForumLogin(int id);

        void Update(Member member);
      
    }
}
