﻿using System.Collections.Generic;

namespace Ssl.Clubsite.Domain.Interfaces
{
  public interface IListView<T> :IView where T : class 
  {
    List<T> ListItems { set; }
  }
}