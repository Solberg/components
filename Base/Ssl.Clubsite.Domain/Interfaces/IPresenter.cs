﻿namespace Ssl.Clubsite.Domain.Interfaces
{
  public interface IPresenter<T> where T : IView
  {

    #region Properties

    T View { get; }

    #endregion

    #region Methods

    void Onload(bool initializing);

    #endregion

  }
}
