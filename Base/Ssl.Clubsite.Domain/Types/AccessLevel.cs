﻿namespace Ssl.Clubsite.Domain.Types
{
    public enum AccessLevel
  {
    Private = 1,
    Public = 2,
  }
}