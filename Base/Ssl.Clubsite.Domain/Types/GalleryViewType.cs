﻿namespace Ssl.Clubsite.Domain.Types
{
  public enum GalleryViewType
  {
    Index,
    Album
  }
}