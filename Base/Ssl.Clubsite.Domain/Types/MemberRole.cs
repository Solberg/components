﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Domain.Types
{
    public enum MemberRole
    {
        Member = 0,
        Administrator = 1,
        Editor = 2
    }
}
