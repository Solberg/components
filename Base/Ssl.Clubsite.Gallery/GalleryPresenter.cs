﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GalleryPresenter.cs" company="Tripple S">
// 2011  
// </copyright>
// <summary>
//   Presenter class for a Gallery
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ssl.Clubsite.Gallery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    using Domain;
    using Domain.Entity;
    using Domain.Types;

    using HDERNE.Presentation;

    using Image = Domain.Entity.Image;

    /// <summary>
  /// Presenter class for a Gallery
  /// </summary>
  public class GalleryPresenter
  {
    #region Const

    /// <summary>
    /// Number of rows on indexpage for images
    /// </summary>
    private const int ImagePageSize = 8;
    #endregion

    #region Variables

    /// <summary>
    /// View to handle
    /// </summary>
    private readonly IGallery view;

      /// <summary>
      /// Repository with imagealbum information.
      /// </summary>
        private readonly IAlbumRepository albumRepository;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="GalleryPresenter"/> class. 
    /// Default constructor
    /// </summary>
    /// <param name="view">
    /// View to handle
    /// </param>
    /// <param name="albumRepository">
    /// Respository with album information.
    /// </param>
    public GalleryPresenter(IGallery view, IAlbumRepository albumRepository)
    {
      this.view = view;
      this.albumRepository = albumRepository;
    }

    #endregion

    #region Public

    /// <summary>
    /// Initialize control first time it's called
    /// </summary>
    public void Initialize()
    {
      int pagesize;

      if (this.view.ViewType == GalleryViewType.Index)
      {
        pagesize = 5;
        this.FetchAlbumData(pagesize, 0);
      }
      else
      {
        pagesize = ImagePageSize;
        this.FetchAlbumImages(pagesize, 0);
        ImageAlbum album = this.albumRepository.Fetch(this.view.SelectedAlbum);
        this.view.SetAlbumHeader(album);
      }

      this.InitPaging();
    }

    /// <summary>
    /// Page index is changed in the index
    /// </summary>
    /// <param name="pageIndex">New page index</param>
    public void PageChanged(int pageIndex)
    {
      if (this.view.ViewType == GalleryViewType.Index)
      {
        this.FetchAlbumData(5, pageIndex);
      }
      else
      {
        this.FetchAlbumImages(ImagePageSize, pageIndex);
      }
    }

    /// <summary>
    /// Init control used for paging
    /// </summary>
    public void InitPaging()
    {
      int count = (this.view.ViewType == GalleryViewType.Index) ? this.Folders.Count() : NumberOfImageRows(this.view.SelectedAlbum);

      this.view.CreatePagingControl(count, (this.view.ViewType == GalleryViewType.Index) ? 5 : ImagePageSize);
    }

    #endregion

    #region Private

    /// <summary>
    /// Calculate number of image rows for a album
    /// </summary>
    /// <param name="albumId">
    /// The album id.
    /// </param>
    /// <returns>
    /// Number of image rows
    /// </returns>
    private int NumberOfImageRows(int albumId)
    {
      int numberOfImages = this.albumRepository.Fetch(albumId).NumberOfImages;

      // ReSharper disable PossibleLossOfFraction
      return Convert.ToInt32(Decimal.Floor(numberOfImages / 3)) + Convert.ToInt32(numberOfImages % 3 != 0);
      
      // ReSharper restore PossibleLossOfFraction
    }

    /// <summary>
    /// Fetch folder data
    /// </summary>
    /// <param name="pageSize">
    /// Number of albums on one page
    /// </param>
    /// <param name="pageIdx">
    /// The page idx.
    /// </param>
    private void FetchAlbumData(int pageSize, int pageIdx)
    {
      var query = from p in this.Folders.Skip(pageIdx * pageSize).Take(pageSize) select p;

      var albums = new PagedDataSource
        {
            AllowCustomPaging = true, AllowPaging = true, DataSource = query, PageSize = pageSize 
        };

      this.view.Albums = (IEnumerable<ImageAlbum>)albums.DataSource;

      if (query.Count() > 0)
      {
        this.view.BigImageUrl = query.ToList()[0].FullSizeImage;
      }
    }

    /// <summary>
    /// Fetch folder data
    /// </summary>
    /// <param name="take">
    /// The pageSize.
    /// </param>
    /// <param name="pageIdx">
    /// The page idx.
    /// </param>
    private void FetchAlbumImages(int take, int pageIdx)
    {
      var query = from p in this.ImageRows(this.view.SelectedAlbum).Skip(pageIdx * take).Take(take) select p;

      var page = new PagedDataSource
        {
            AllowCustomPaging = true, AllowPaging = true, DataSource = query, PageSize = take 
        };

      this.view.AlbumImages = (IEnumerable<ImageRow>)page.DataSource;

      // Show first image in first row
      if (query.Count() > 0)
      {
        this.view.BigImageUrl = query.ToList()[0].Row[0].Source;        
      }
    }

    /// <summary>
    /// List image folders
    /// </summary>
    private IEnumerable<ImageAlbum> Folders
    {
      get
      {
        return this.albumRepository.List(this.view.AccessLevel);
      }
    }


    private IEnumerable<ImageRow> ImageRows(int albumId)
    {
      var imageRows = new List<ImageRow>();

      IEnumerable<Image> images = this.albumRepository.ListImages(albumId);

      int i = 0;

      var row = new ImageRow();

      foreach (var image in images)
      {
        row.Row.Add(image);

        if (i == 2)
        {
          imageRows.Add(row);
          row = new ImageRow();
          i = 0;
        }
        else
        {
          i++;
        }
      }

      if (row.Row.Count > 0)
      {
        imageRows.Add(row);        
      }

      return imageRows;
    }


    #endregion
  }
}