﻿namespace HDERNE.Presentation
{
  using System.Collections.Generic;

  using Ssl.Clubsite.Domain.Entity;
  using Ssl.Clubsite.Domain.Types;

    /// <summary>
  /// Interface to gallery usercontrol
  /// </summary>
  public interface IGallery
  {
    /// <summary>
    /// Index over image album
    /// </summary>
      IEnumerable<ImageAlbum> Albums { get; set; }

    /// <summary>
    /// Images in a album.
    /// </summary>
    IEnumerable<ImageRow> AlbumImages { get; set; }

    /// <summary>
    /// Viewtype album or index
    /// </summary>
    GalleryViewType ViewType { get; set; }

    /// <summary>
    /// Selected Album
    /// </summary>
    int SelectedAlbum { get; set; }

    AccessLevel AccessLevel { get; }

    string BigImageUrl { set; }
    
    /// <summary>
    /// Create control to handle paging
    /// </summary>
    void CreatePagingControl(int recordCount, int pagesize);

    /// <summary>
    /// Update header on album control
    /// </summary>
    /// <param name="imageFolder">Data for the header</param>
    void SetAlbumHeader(ImageAlbum imageFolder);
  }
}
