<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GuestBookSign.ascx.cs" Inherits="SSL.UserControls.GuestBook.GuestBookSign" %>

<table class="ContentArea">
  <tr>
    <td style="padding-top: 15px" align="center">
      <table style="background-image: url(/images/layout/table_back.gif); border: 1px solid black" cellpadding="4">
        <tr>
          <td class="FieldLabel">
            <asp:Label id="lblName" runat="server">Navn:</asp:Label>
          </td>
          <td class="FieldInput">
            <asp:TextBox CssClass="FieldInput" runat="server" ID="tbName"></asp:TextBox>
          </td>
          <td class="FieldError">
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="SignGuestbook"></asp:RequiredFieldValidator>
           </td>
        </tr>
        <tr>
          <td class="FieldLabel"><asp:Label id="lblEMail" runat="server"></asp:Label></td>
          <td class="FieldInput">
            <asp:TextBox CssClass="FieldInput" runat="server" ID="tbEMail"></asp:TextBox>
          </td>
          <td class="FieldError">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td class="FieldLabel"><asp:Label id="lblHomePage" runat="server">Hjemmeside titel:</asp:Label></td>
          <td class="FieldInput">
            <asp:TextBox CssClass="FieldInput" runat="server" ID="tbHomepageTitle"></asp:TextBox>
          </td>
          <td class="FieldError">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td class="FieldLabel"><asp:Label id="lblHomePageUrl" runat="server">Hjemmeside url:</asp:Label></td>
          <td class="FieldInput">
            <asp:TextBox CssClass="FieldInput" runat="server" ID="tbHomepageUrl"></asp:TextBox>
          </td>
          <td class="FieldError">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td class="FieldLabel"><asp:Label id="lblReferrer" runat="server">Hvordan fandt du hjemmeside:</asp:Label></td>
          <td class="FieldInput">
            <asp:DropDownList CssClass="FieldInput" runat="server" ID="cboReferredBy">
              <asp:ListItem Text="Google" Value="1"></asp:ListItem>
              <asp:ListItem Text="Bike page" Value="2"></asp:ListItem>
              <asp:ListItem Text="Friend" Value="3"></asp:ListItem>
              <asp:ListItem Text="Search engine" Value="4"></asp:ListItem>
              <asp:ListItem Text="Other" Value="5"></asp:ListItem>
            </asp:DropDownList>
          </td>
          <td class="FieldError">
            &nbsp;
          </td>
        </tr>
        <tr>
          <td class="FieldLabel"><asp:Label id="lblLocation" runat="server">Hvor kommer du fra:</asp:Label></td>
          <td class="FieldInput">
            <asp:TextBox CssClass="FieldInput" runat="server" ID="tbLocation"></asp:TextBox></td>
          <td class="FieldError">
            &nbsp;</td>
        </tr>
        <tr>
          <td class="FieldLabel"><asp:Label id="lblAddOn" runat="server">Interesse:</asp:Label></td>
          <td class="FieldInput">
            <asp:TextBox CssClass="FieldInput" runat="server" ID="tbAddOn"></asp:TextBox></td>
          <td class="FieldError">
            &nbsp;</td>
        </tr>
        <tr>
          <td class="FieldLabel" style="vertical-align: top; padding-top: 6px"><asp:Label id="lblComment" runat="server">Kommentar:</asp:Label>
          </td>
          <td class="FieldInput"><asp:TextBox id="tbComment" CssClass="FieldInput" runat="server" TextMode="MultiLine" Height="80px"></asp:TextBox></td>
          <td class="FieldError" style="vertical-align: top; padding-top: 6px">
            <asp:RequiredFieldValidator ID="rfvComment" runat="server" ControlToValidate="tbComment" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="SignGuestbook"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
          <td class="FieldLabel" style="vertical-align: top; padding-top: 6px"><asp:Label id="Label1" runat="server">Privat:</asp:Label>
          </td>
          <td class="FieldInput"><asp:CheckBox id="chkPrivate" runat="server"></asp:CheckBox></td>
          <td class="FieldError">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" align="center">
            <asp:Button Width="60" CssClass="Button" ID="btnSubmit" runat="server" Text="Sign" OnClick="btnSubmit_Click" ValidationGroup="SignGuestbook"  />&nbsp;
            <button class="Button" type="reset" style="width: 60px">Cancel</button></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

