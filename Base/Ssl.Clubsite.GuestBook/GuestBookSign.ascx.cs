using System;
using System.Web.UI;

namespace SSL.UserControls.GuestBook
{
  public partial class GuestBookSign : UserControl, ISignGuestbookView
  {
      #region Variables

      private SignGuestbookPresenter signGuestbookPresenter;
      #endregion

      #region Properties
      protected virtual string ConnectionString
    {
      get { return String.Empty;}

    }
    #endregion

      #region Protected

      protected override void OnInit(EventArgs e)
    {
        this.signGuestbookPresenter = new SignGuestbookPresenter(this, this.ConnectionString);
        base.OnInit(e);
    }

      /// <summary>
      /// Add entry to guestbook.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      protected void btnSubmit_Click(object sender, EventArgs e)
      {
          this.signGuestbookPresenter.AddEntry();
      }

  #endregion


    #region ISignGuestbookView

    public string Name
    {
        get
        {
            return tbName.Text;
        }
    }

    public string Email
    {
        get 
        {
            return tbEMail.Text;
        }
    }

    public string HomepageTitle
    {
        get { return tbHomepageTitle.Text; }
    }

    public string HomepageUrl
    {
        get {
            return tbHomepageUrl.Text; }
    }

    public int ReferredBy
    {
        get {
            return Convert.ToInt32(cboReferredBy.SelectedValue); }
    }

    public string Location
    {
        get { return tbLocation.Text; }
    }

    public string AddOn
    {
        get { return tbAddOn.Text; }
    }

    public string Comment
    {
        get {
            return tbComment.Text; }
    }

    public bool Valid
    {
        get {
            return Page.IsValid; 
        }
    }

    public bool Private
    {
        get { return chkPrivate.Checked; }
    }

    public void ShowMessage(string message)
    {
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Alert", string.Format("alert('{0}');", message), true);
    }
    #endregion

  }
}