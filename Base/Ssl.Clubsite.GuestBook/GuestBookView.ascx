<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GuestBookView.ascx.cs" Inherits="SSL.UserControls.GuestBook.GuestBookView" %>
      <div  id="GuestView" class="ContentArea Scroll">
				<div class="ButtonColumn01">
					<asp:Button CssClass="Button" runat="server" ID="btnPrevious" Text="Previous" Width="80px" OnClick="btnPrevious_Click" />
					<asp:Button CssClass="Button" runat="server" ID="btnNext" Text="Next" Width="80px" OnClick="btnNext_Click" />				
				</div>
				<div class="ClearFloat"></div>
        <table>
          <tr>
            <td align="center" style="vertical-align: middle;">
              <asp:DataList ID="dlGuestBook" runat="server" Width="470" RepeatColumns="1" EnableViewState="False" OnItemDataBound="dlGuestBook_ItemDataBound">
                <AlternatingItemStyle HorizontalAlign="Left"></AlternatingItemStyle>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                <ItemTemplate>
                  <table style="width: 300px">
                    <tr>
                      <td class="Frame FramePadding">
                        <table style="width: 100%" cellspacing="2" cellpadding="4">
                          <tr>
                            <td id="tdImg" style="vertical-align: top; height: 30px;" colspan="2" runat="server">
                              <asp:Image ID="imgPin" runat="server"></asp:Image></td>
                          </tr>
                          <tr>
                            <td class="FieldLabel">
                              Tidspunkt:
                            </td>
                            <td class="ContentText">
                              <asp:Literal ID="litTime" runat="server" /></td>
                          </tr>
                          <tr>
                            <td class="FieldLabel">
                              Navn:
                            </td>
                            <td class="ContentText">
                              <asp:HyperLink ID="hlName" runat="server"></asp:HyperLink></td>
                          </tr>
                          <tr runat="server" id="trHomePage">
                            <td class="FieldLabel">
                              Hjemmeside:
                            </td>
                            <td class="ContentText">
                              <asp:HyperLink ID="hlHomepage" runat="server" Target="_Blank"></asp:HyperLink></td>
                          </tr>
                          <tr runat="server" id="trLocation">
                            <td class="FieldLabel">
                              Hjemsted:
                            </td>
                            <td class="ContentText">
                              <asp:Literal ID="litLocation" runat="server" /></td>
                          </tr>
                          <tr runat="server" id="trRide">
                            <td class="FieldLabel">
                              Tilføjelse:
                            </td>
                            <td class="ContentText">
                              <asp:Literal ID="litAddOn" runat="server" /></td>
                          </tr>
                          <tr>
                            <td class="FieldLabel" style="vertical-align: top; padding-top: 4px">
                              Kommentar:
                            </td>
                            <td class="ContentText">
                              <asp:Literal ID="litComment" runat="server" /></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </ItemTemplate>
              </asp:DataList>
            </td>
          </tr>
        </table>
				<div class="ButtonColumn01">
					<asp:Button CssClass="Button" runat="server" ID="btnPreviousBottom" Text="Previous" Width="80px" OnClick="btnPrevious_Click" />
					<asp:Button CssClass="Button" runat="server" ID="btnNextBottom" Text="Next" Width="80px" OnClick="btnNext_Click" />
				</div>
      </div>
