using System;
using System.Data;
using System.Web.UI.WebControls;

namespace SSL.UserControls.GuestBook
{
  public partial class GuestBookView : System.Web.UI.UserControl
  {
      #region Variables

      private ViewGuestbookPresenter presenter;
      #endregion
      #region Consts
      const int C_PageSize = 10;
    #endregion

    #region Properties
    private int PageIndex
    {
      get { return ViewState["PageIndex"] != null ? Convert.ToInt32(ViewState["PageIndex"]) : 1;}
      set { ViewState["PageIndex"] = value;}

    }

    protected virtual string ConnectionString
    {
      get { return String.Empty;}
    }

    #endregion


    #region Event-handlers

      protected override 

    public override void DataBind()
    {


      base.DataBind();

      DataView dv = new dboGuestBook(ConnectionString).List("Id DESC");

      int iStartRec = ((PageIndex - 1) * C_PageSize);

       iStartRec = iStartRec > (dv.Count - C_PageSize) && iStartRec > 0 ? (dv.Count - C_PageSize) : iStartRec;

       int iEnd = iStartRec + C_PageSize > dv.Count - 1 ? dv.Count - 1 : iStartRec + C_PageSize;

       DataTable dt = dv.ToTable();

       dv.RowFilter = String.Format("Id >= {0} and Id <= {1}", dt.Rows[iEnd]["Id"], dt.Rows[iStartRec]["Id"]);

       dlGuestBook.DataSource = dv;
       dlGuestBook.DataBind();

      btnPrevious.Enabled = PageIndex > 1;
      btnPreviousBottom.Enabled = btnPrevious.Enabled;

      int iRowCount = new dboGuestBook(ConnectionString).RecordCount();
      int iMaxPage = Convert.ToInt32(iRowCount / C_PageSize) + Convert.ToInt32((iRowCount % C_PageSize) > 0);
      btnNext.Enabled = PageIndex < iMaxPage;
      btnNextBottom.Enabled = btnNext.Enabled;

    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      if (! Page.IsPostBack)
      {
        DataBind();
      }
    }
    
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
      PageIndex--;
 
      DataBind();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
      PageIndex++;

      DataBind();
    }

    /// <summary>
    /// Bind fields from the dataset to a row on the DataList
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlGuestBook_ItemDataBound(object sender, DataListItemEventArgs e)
    {
      if (e.Item.DataItem != null)
      {
        switch (e.Item.ItemType)
        {
          case ListItemType.Header:
            break;
          case ListItemType.Footer:
            break;
          default:
            object[] aFields = ((DataRowView)e.Item.DataItem).Row.ItemArray;

            ((Literal)e.Item.FindControl("litTime")).Text = Convert.ToString(aFields[10]);
            ((Image)e.Item.FindControl("imgPin")).ImageUrl = e.Item.ItemType == ListItemType.Item ? "/images/layout/pinright.gif" : "/images/layout/pinleft.gif";
            ((Image)e.Item.FindControl("imgPin")).ImageAlign = e.Item.ItemType == ListItemType.Item ? ImageAlign.Right : ImageAlign.Left;

            if (Convert.ToInt32(aFields[9]) == 0)
            {
              ((HyperLink)e.Item.FindControl("hlName")).Text = Convert.ToString(aFields[1]);
              ((HyperLink)e.Item.FindControl("hlName")).NavigateUrl = aFields[2].ToString() != String.Empty ? "mailto:" + aFields[2] : String.Empty;

              (e.Item.FindControl("pnlHomePage")).Visible = ! String.IsNullOrEmpty(aFields[3].ToString());
              (e.Item.FindControl("pnlLocation")).Visible = ! String.IsNullOrEmpty(aFields[6].ToString());
              (e.Item.FindControl("pnlAddOn")).Visible    = ! String.IsNullOrEmpty(aFields[7].ToString());

              ((HyperLink)e.Item.FindControl("hlHomepage")).Text = Convert.ToString(aFields[3]);
              ((HyperLink)e.Item.FindControl("hlHomepage")).NavigateUrl = Convert.ToString(aFields[4]);

              ((Literal)e.Item.FindControl("litLocation")).Text = Convert.ToString(aFields[6]);
              ((Literal)e.Item.FindControl("litAddOn")).Text = Convert.ToString(aFields[7]);
              ((Literal)e.Item.FindControl("litComment")).Text = Convert.ToString(aFields[8]);
            }
            else
            {
              ((HyperLink)e.Item.FindControl("hlName")).Text = "Private message";
            }
            break;
        }

      }

    }


    #endregion


  }
}