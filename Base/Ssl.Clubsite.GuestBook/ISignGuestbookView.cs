﻿namespace Ssl.Clubsite.Guestbook
{
    public interface ISignGuestbookView
    {
        string Name{ get;}

        string Email { get;}

        string HomepageTitle { get;}

        string HomepageUrl{ get; }

        int ReferredBy{ get; }

        string Location { get;}

        string AddOn{ get; }

        string Comment{ get; }

        bool Valid { get; }

        bool Private { get; }

        void ShowMessage(string message);

    }
}
