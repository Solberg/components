﻿using Ssl.Clubsite.Domain.Interfaces;

namespace Ssl.Clubsite.Guestbook
{
  using Domain.Entity;

    public interface IViewGuestbookView : IListView<GuestbookEntry>
    {
        
        int Pagesize { get; }

        int PageIdx { get; set; }

        bool NextEnabled { set; }

        bool PreviousEnabled { set; }
    }
}
