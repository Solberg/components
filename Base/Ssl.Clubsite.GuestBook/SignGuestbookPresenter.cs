﻿using SSL.Domain;

namespace Ssl.Clubsite.Guestbook
{
    using System;

    using Domain;
    using Domain.Entity;

    public class SignGuestbookPresenter
    {
        private readonly ISignGuestbookView _view;

        private readonly IGuestbookRepository _guestbookRepository;

        private readonly INotifier _notifier;

        public event EventHandler OnAfterPost;

        

        public SignGuestbookPresenter(ISignGuestbookView view, IGuestbookRepository guestbookRepository, INotifier notifier )
        {
            this._view = view;
            this._guestbookRepository = guestbookRepository;
            this._notifier = notifier;
        }

        public void AddEntry()
        {
            if (_view.Valid)
            {
               _guestbookRepository.Create(
                   new GuestbookEntry(DateTime.Now,
                                    _view.Name, 
                                    _view.Email, 
                                    _view.HomepageTitle,
                                    _view.HomepageUrl,
                                    _view.ReferredBy,
                                    _view.Location,
                                    _view.AddOn, 
                                    _view.Comment, 
                                    _view.Private));

              _notifier.Notify("Guestbook signed",
                string.Format("By: {0}{1}Message:{2}", _view.Name, System.Environment.NewLine, _view.Comment));

                if (OnAfterPost != null)
                {
                    OnAfterPost(this, EventArgs.Empty);
                }
            }
            else
            {
                _view.ShowMessage("Der er fejl i et eller flere felter");
            }
        }


    }
}
