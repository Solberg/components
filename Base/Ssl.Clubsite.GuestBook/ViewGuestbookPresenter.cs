﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Ssl.Clubsite.Domain;
using Ssl.Clubsite.Domain.Entity;
using Ssl.Clubsite.Interfaces;

namespace Ssl.Clubsite.Guestbook
{
  public class ViewGuestbookPresenter : Presenter<IViewGuestbookView>
  {
    #region Variables

    private readonly IGuestbookRepository _guestbookRepository;

    #endregion

    #region Constructors

    public ViewGuestbookPresenter(IViewGuestbookView view, IGuestbookRepository guestbookRepository) : base(view)
    {
      _guestbookRepository = guestbookRepository;
      View.PageIdx = 0;
    }

    #endregion

    #region Public

    public override void Onload(bool initializing)
    {
      base.Onload(initializing);
      if (initializing)
      {
        Initialize();
      }
    }

    public void NextPage()
    {
      View.PageIdx++;
      Initialize();
    }

    public void PreviousPage()
    {
      View.PageIdx--;
      Initialize();
    }

    #endregion

    #region Private

    private void Initialize()
    {
      List<GuestbookEntry> entries = _guestbookRepository.List();

      int iRowCount = _guestbookRepository.RecordCount();
      int iMaxPage = Convert.ToInt32(iRowCount/View.Pagesize) + Convert.ToInt32((iRowCount%View.Pagesize) > 0);


      IEnumerable<GuestbookEntry> query = from p in entries.Skip(View.Pagesize*View.PageIdx).Take(View.Pagesize)
        select p;


      var page = new PagedDataSource
      {
        AllowCustomPaging = true,
        AllowPaging = true,
        DataSource = query.ToList(),
        PageSize = View.Pagesize
      };

      View.ListItems = (List<GuestbookEntry>) page.DataSource;
      View.PreviousEnabled = View.PageIdx > 0;
      View.NextEnabled = (View.PageIdx + 1) < iMaxPage;
    }

    #endregion
  }
}