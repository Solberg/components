namespace Ssl.Clubsite.Guestbook.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using Ssl.Clubsite.Domain;
    using Ssl.Clubsite.Domain.Entity;

    using SSL.Data.SqlServer;

    using SqlParameterCollection = SSL.Data.SqlServer.SqlParameterCollection;

    /// <summary>
  /// Dataobject for Guestbook table.
  /// </summary>
  public class GuestbookRepository : SqlClient, IGuestbookRepository
  {
      #region Variables
      private readonly string connectionString;
      #endregion

      public GuestbookRepository(string connectionString)
      {
          this.connectionString = connectionString;
      }


      public override SqlConnection Connect()
      {
          return new SqlConnection(this.connectionString);
      }

    /// <summary>
    /// List all entries from guestbook.
    /// </summary>
    /// <returns></returns>
    public List<GuestbookEntry> List()
    {
        DataView dv = StandardList("spGuestbookList");

        return (from DataRow dr in dv.Table.Rows orderby dr["Id"] descending 
                select new GuestbookEntry(Convert.ToDateTime(dr["TimeStamp"]), dr["Name"].ToString(), dr["Mail"].ToString(), dr["HomepageTitle"].ToString(), dr["Homepageurl"].ToString(), Convert.ToInt32(dr["ReferredBy"]), dr["Location"].ToString(), dr["AddOn"].ToString(), dr["Comment"].ToString(), Convert.ToBoolean(dr["Private"]))).ToList();
    }

    public int RecordCount()
    {
      return StandardList("spGuestBookList").Table.Rows.Count;
    }


    public void Create(GuestbookEntry guestbookEntry)
    {
        var spc = new SqlParameterCollection
            {
                { new SqlParameter("@Name", SqlDbType.VarChar, 50), guestbookEntry.Name },
                { new SqlParameter("@Mail", SqlDbType.VarChar, 100), guestbookEntry.Email },
                { new SqlParameter("@HomepageTitle", SqlDbType.VarChar, 50), guestbookEntry.HomepageTitle },
                { new SqlParameter("@HomepageUrl", SqlDbType.VarChar, 100), guestbookEntry.HomepageUrl },
                { new SqlParameter("@ReferredBy", SqlDbType.Int), guestbookEntry.ReferredBy },
                { new SqlParameter("@Location", SqlDbType.VarChar, 50), guestbookEntry.Location },
                { new SqlParameter("@Ride", SqlDbType.VarChar, 50), guestbookEntry.AddOn },
                { new SqlParameter("@Comment", SqlDbType.VarChar, 4000), guestbookEntry.Comment },
                { new SqlParameter("@Private", SqlDbType.Bit), guestbookEntry.Private },
                { new SqlParameter("@TimeStamp", SqlDbType.SmallDateTime), DateTime.Now }
            };

        StandardCreate("spGuestbookCreate", ref spc);
    }
  }
}