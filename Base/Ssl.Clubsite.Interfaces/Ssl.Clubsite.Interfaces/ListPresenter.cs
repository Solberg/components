﻿using Ssl.Clubsite.Domain;
using Ssl.Clubsite.Domain.Interfaces;

namespace Ssl.Clubsite.Interfaces
{
  public class ListPresenter<T> : Presenter<IListView<T>> where T : class 
  {
    #region Variables

        private readonly IListRepository<T> _repository;
        #endregion

        #region Constructors
        public ListPresenter(IListView<T> view, IListRepository<T> repository) : base(view)
        {
            _repository = repository;
        }
        #endregion

        #region Public

        public void OnLoad(bool initializing)
        {
          if (initializing)
          {
            View.ListItems = _repository.List();
          }
        }

        #endregion
  }
}
