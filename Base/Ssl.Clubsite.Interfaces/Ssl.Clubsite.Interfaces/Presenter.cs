﻿using Ssl.Clubsite.Domain.Interfaces;

namespace Ssl.Clubsite.Interfaces
{
  public class Presenter<T> : IPresenter<T> where T : IView
  {
    public Presenter(T view)
    {
      View = view;
    }

    #region IPresenter implementation

    public T View { get; private set; }

    public virtual void Onload(bool initializing)
    {
    }

    #endregion

  }
}
