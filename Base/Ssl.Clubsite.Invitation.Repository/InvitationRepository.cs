﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ssl.Clubsite.Domain;
using System.Xml.Linq;

namespace Ssl.Clubsite.Invitation.Repository
{
  public class InvitationRepository : IListRepository<Domain.Entity.Invitation>
    {
        #region Constants and Fields

        /// <summary>
        /// The doc.
        /// </summary>
        private XDocument doc;

        /// <summary>
        /// Path to image indexfile
        /// </summary>
        private string indexFile;

        #endregion

        #region Properties

        /// <summary>
        /// Gets Doc.
        /// </summary>
        private XDocument Doc
        {
            get
            {
                return this.doc ??
                       (this.doc = XDocument.Load(this.indexFile));
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="indexFile">Path to the index file</param>
        public InvitationRepository(string indexFile)
        {
            this.indexFile = indexFile;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// The list.
        /// </summary>
        /// <param name="accessLevel">
        /// The access level.
        /// </param>
        /// <returns>
        /// </returns>
        public List<Domain.Entity.Invitation> List()
        {
            IEnumerable<Domain.Entity.Invitation> invitations = from invitation in this.Doc.Descendants("invitation")
                                             let textElement = invitation.Element("text")
                                             let eventDateElement = invitation.Element("eventdate")
                                             let publishDateElement = invitation.Element("publishdate")
                                             let imageElement = invitation.Element("image")
                                             let documentElement = invitation.Element("document")
                                             orderby publishDateElement.Value descending
                                             select
                                                 new Domain.Entity.Invitation(
                                                 textElement != null ? textElement.Value : string.Empty, 
                                                 eventDateElement != null && ! string.IsNullOrEmpty(eventDateElement.Value) 
                                                     ? (DateTime?) DateTime.ParseExact(eventDateElement.Value, "yyyyMMdd", null) : null,
                                                 publishDateElement != null && ! string.IsNullOrEmpty(publishDateElement.Value) 
                                                     ? (DateTime?) DateTime.ParseExact(publishDateElement.Value, "yyyyMMdd", null) : null,
                                                 imageElement != null ? imageElement.Value : string.Empty,
                                                 documentElement != null ? documentElement.Value : string.Empty);

            return invitations.ToList();
        }


        #endregion
    }
}
