namespace Ssl.Clubsite.Link.Repository
{
    using SSL.Data.SqlServer;

    using Domain;

    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using Domain.Entity;

    /// <summary>
    /// Dataobjekt for tabellen Link.
    /// </summary>
    public class LinkRepository : SqlClient, IListRepository<Link>
    {

        #region Constructors
        public LinkRepository()
        {
        }

        public LinkRepository(bool IbAutoClose)
            : base(IbAutoClose)
        {
        }

        /// <summary>
        /// Forbind med connectionstring angivet i parameter.
        /// </summary>
        /// <param name="IsConnectionString"></param>
        public LinkRepository(string IsConnectionString)
        {
            ConnectionString = IsConnectionString;
        }

        #endregion


        //Select From Database
        public List<Link> List()
        {
            DataView dv = StandardList("spLinkList", string.Empty);

            return (from DataRow dr in dv.Table.Rows select new Link(DbConvert.ToInt32(dr["Id"]), dr["Name"].ToString(), dr["Url"].ToString(), dr["Description"].ToString(), DbConvert.ToBoolean(dr["Active"]))).ToList();
        }

    }
}
