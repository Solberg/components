﻿namespace Ssl.Clubsite.Link
{
    using System.Collections.Generic;

    using Ssl.Clubsite.Domain.Entity;

    public interface ILinkListView
    {
        List<Link> Items { set; }
    }
}
