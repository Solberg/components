﻿namespace Ssl.Clubsite.Link
{
    using Domain;

    public class LinkListPresenter
    {
        #region Variables

        private readonly ILinkListView view;

        private readonly ILinkRepository repository;
        #endregion

        #region Constructors
        public LinkListPresenter(ILinkListView view, ILinkRepository repository)
        {
            this.view = view;
            this.repository = repository;
        }
        #endregion

        #region Public
        public void Initialize()
        {
            this.view.Items = this.repository.List();
        }
        #endregion
    }
}
