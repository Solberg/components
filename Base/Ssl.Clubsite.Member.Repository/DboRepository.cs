namespace Ssl.Clubsite.Member.Dbo
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections.Generic;
    using System.Linq;

    using Domain;
    using Domain.Entity;

    using SSL.Data.SqlServer;

    using SqlParameterCollection = SSL.Data.SqlServer.SqlParameterCollection;

    public class DboMember : SqlClient, IMemberRepository
    {

        #region Variables

        private readonly string connectionString;
        #endregion


        #region Constructors
        public DboMember(string connectionString)
    {
            this.connectionString = connectionString;
    }

    public DboMember(string connectionString, bool IbAutoClose)
      : base(IbAutoClose)
    {
        this.connectionString = connectionString;
    }
    #endregion


    #region Public
    public override SqlConnection Connect()
    {
        return new SqlConnection(this.connectionString);
    }
    #endregion

    //Select From Database
    public List<Member> List(string IsSortOrder)
    {
      DataView dv = StandardList("spMember_select", IsSortOrder);

      return (from DataRow dr in dv.Table.Rows
              select new Member(Convert.ToInt32(dr["Id"]), dr["Name"].ToString(), dr["Address"].ToString(),dr["PostCode"].ToString(),dr["City"].ToString(), dr["Login"].ToString(), dr["Password"].ToString(), dr["Phone"].ToString(), dr["Mail"].ToString(), DbConvert.ToInt32(dr["Role"]), DbConvert.ToDateTime(dr["LastLogin"]))).ToList();
      

    }

    //Search From Database
    public Member FetchById(int id)
    {
    	var spc = new SqlParameterCollection
    	    {
    	        { new SqlParameter("@Id", SqlDbType.Int), id },
    	        { new SqlParameter("@Login", SqlDbType.VarChar, 50), ParameterDirection.Output },
    	        { new SqlParameter("@Name", SqlDbType.VarChar, 50), ParameterDirection.Output },
    	        { new SqlParameter("@Phone", SqlDbType.VarChar, 8), ParameterDirection.Output },
    	        { new SqlParameter("@Mail", SqlDbType.VarChar, 100), ParameterDirection.Output },
    	        { new SqlParameter("@Password", SqlDbType.VarChar, 50), ParameterDirection.Output },
    	        { new SqlParameter("@Address", SqlDbType.VarChar, 50), ParameterDirection.Output },
    	        { new SqlParameter("@PostCode", SqlDbType.VarChar, 4), ParameterDirection.Output },
    	        { new SqlParameter("@City", SqlDbType.VarChar, 50), ParameterDirection.Output },
    	        { new SqlParameter("@Role", SqlDbType.Int), ParameterDirection.Output },
    	        { new SqlParameter("@LastLogin", SqlDbType.DateTime), ParameterDirection.Output }
    	    };

      StandardFetchIfExists("spMemberFetch", ref spc);

      return new Member(id,
          DbConvert.ToString(spc[2].Value),

          DbConvert.ToString(spc[6].Value),
          DbConvert.ToString(spc[7].Value),
          DbConvert.ToString(spc[8].Value),

          DbConvert.ToString(spc[1].Value),
          DbConvert.ToString(spc[5].Value),

          DbConvert.ToString(spc[3].Value),
          DbConvert.ToString(spc[4].Value),
          DbConvert.ToInt32(spc[9].Value),
          DbConvert.ToDateTime(spc[10].Value));
    }

    /// <summary>
    /// Search on login/password
    /// </summary>
    /// <returns></returns>
    public bool FetchByLogin(string login,
      string password,
      out int id)
    {
    	var spc = new SqlParameterCollection
    	    {
    	        { new SqlParameter("@IsLogin", SqlDbType.VarChar, 50), login },
    	        { new SqlParameter("@IsPassword", SqlDbType.VarChar, 50), password },
    	        { new SqlParameter("@OiId", SqlDbType.Int), ParameterDirection.Output }
    	    };

        bool ok = StandardFetchIfExists("spMemberFetchLogin", ref spc);

      id = ok ? Convert.ToInt32(spc[2].Value) : 0;
      return ok;
    }


    /// <summary>
    /// Search on login/password
    /// </summary>
    /// <returns></returns>
    public void FetchIdFromLogin(string IsLogin,
      out int OiId)
    {
    	var spc = new SqlParameterCollection
    	    {
    	        { new SqlParameter("@IsLogin", SqlDbType.VarChar, 50), IsLogin },
    	        { new SqlParameter("@IiId", SqlDbType.Int), ParameterDirection.Output }
    	    };

        StandardFetch("sp_MemberFetchIdFromLogin", ref spc);

      OiId = Convert.ToInt32(spc[1].Value);
    }

    //Search From Database
    public void FetchLastForumLogin(int IiMemberId,
      out DateTime OdLastForumLogin)
    {
      var spc = new SqlParameterCollection
          {
              { new SqlParameter("@IiId", SqlDbType.Int), IiMemberId },
              { new SqlParameter("@OdLastForumLogin", SqlDbType.DateTime), ParameterDirection.Output }
          };

        StandardFetch("spMemberFetchLastForumLogin", ref spc);

      OdLastForumLogin = DbConvert.ToDateTime(spc[1].Value);
    }




    /// <summary>
    /// Search on mail
    /// </summary>
    /// <returns></returns>
    public bool FetchPasswordFromMail(string IsMail,
      out string OsPassword)
    {
        var spc = new SqlParameterCollection
          {
              { new SqlParameter("@IsMail", SqlDbType.VarChar, 100), IsMail },
              { new SqlParameter("@OsPassword", SqlDbType.VarChar, 50), ParameterDirection.Output }
          };

        bool ok = this.StandardFetchIfExists("spMemberFetchPasswordFromMail", ref spc);

      OsPassword = ok ? Convert.ToString(spc[1].Value) : String.Empty;

      return ok;
    }


    //Update Database
    public void UpdateLastLogin(int IiId,
      DateTime IdLastLogin)
    {
      var spc = new SqlParameterCollection
          {
              { new SqlParameter("@IiId", SqlDbType.Int), IiId },
              { new SqlParameter("@IdLastLogin", SqlDbType.DateTime), IdLastLogin }
          };

        StandardUpdate("spMemberUpdateLastLogin", ref spc);
    }

      public void UpdateLastForumLogin(int IiId)
      {
          var spc = new SqlParameterCollection { { new SqlParameter("@IiId", SqlDbType.Int), IiId } };

          StandardUpdate("spMemberUpdateLastForumLogin", ref spc);
      }

    //Update Database
    public void Update(Member member)
    {
      var spc = new SqlParameterCollection
          {
              { new SqlParameter("@IiId", SqlDbType.Int), member.MemberId },
              { new SqlParameter("@IsLogin", SqlDbType.VarChar, 150), member.LoginName },
              { new SqlParameter("@IsName", SqlDbType.VarChar, 50), member.MemberName },
              { new SqlParameter("@IsPhone", SqlDbType.VarChar, 8), member.Phone },
              { new SqlParameter("@IsMail", SqlDbType.VarChar, 100), member.Mail },
              { new SqlParameter("@IsPassword", SqlDbType.VarChar, 50), member.Password }
          };

        StandardUpdate("spMemberUpdate", ref spc);
    }




    public void Create(Member member)
    {
        var spc = new SqlParameterCollection
          {
              { new SqlParameter("@Login", SqlDbType.VarChar, 150), member.LoginName },
              { new SqlParameter("@Name", SqlDbType.VarChar, 50), member.MemberName },
              { new SqlParameter("@Phone", SqlDbType.VarChar, 8), member.Phone },
              { new SqlParameter("@Mail", SqlDbType.VarChar, 100), member.Mail },
              { new SqlParameter("@Password", SqlDbType.VarChar, 50), member.Password },
              { new SqlParameter("@Address", SqlDbType.VarChar, 50), member.Address },
              { new SqlParameter("@PostCode", SqlDbType.VarChar, 4), member.PostCode },
              { new SqlParameter("@City", SqlDbType.VarChar, 50), member.City },
              { new SqlParameter("@Role", SqlDbType.Int), member.Role }

          };

        StandardUpdate("spMemberCreate", ref spc);
    }
    }
}
