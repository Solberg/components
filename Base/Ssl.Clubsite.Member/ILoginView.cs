﻿using Ssl.Security.Cryptography;
namespace Ssl.Clubsite.Member
{
    public interface ILoginView : ICryptoView
    {
        string MemberName { set;  }

        string LoginName { get; set; }

        string Password { get; }

        bool Remember { get; set; }

        void OnMemberLoggedIn(Domain.Dto.LoginDto loginDto);

    }
}
