﻿using System.Collections.Generic;
using Ssl.Security.Cryptography;

namespace Ssl.Clubsite.Member
{
    public interface IMemberListView : ICryptoView
    {
        /// <summary>
        /// List of members
        /// </summary>
        List<Domain.Entity.Member> Members { set; }

        string Sortorder { get; }
    }
}
