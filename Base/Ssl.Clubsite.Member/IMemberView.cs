﻿namespace Ssl.Clubsite.Member
{
    public interface IMemberView
    {
        int MemberId { get; set; }

        string Name { get; set; }

        string Login { get; set; }

        string Password { get; set; }

        string PasswordNew { get; }

        string PasswordNewAgain { get; }

        string Mail { get; set; }

        string Phone { get; set; }

        int Role { get; set; }

        string CryptoKey { get; }


    }
}
