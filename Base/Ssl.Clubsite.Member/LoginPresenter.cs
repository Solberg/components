﻿using System;
using Ssl.Clubsite.Domain;
using Ssl.Clubsite.Domain.Dto;
using Ssl.Security.Cryptography;

namespace Ssl.Clubsite.Member
{
  public class LoginPresenter
  {
    #region Variables

    private readonly ICryptographyProvider _cryptographyProvider;
    private readonly IMemberRepository _memberRepository;
    private readonly ILoginView _view;

    #endregion

    #region Constructors

    public LoginPresenter(ILoginView loginView, IMemberRepository memberRepository,
      ICryptographyProvider cryptographyProvider)
    {
      _view = loginView;
      _memberRepository = memberRepository;
      _cryptographyProvider = cryptographyProvider;
    }

    #endregion

    #region Public

    /// <summary>
    ///   Can user log in
    /// </summary>
    public bool CanAuthenticate
    {
      get
      {
        int id;
        return _memberRepository.FetchByLogin(_view.LoginName,
          _cryptographyProvider.Encrypting(_view.Password, _view.CryptoKey), out id);
      }
    }

    /// <summary>
    ///   When user logs in
    /// </summary>
    public void OnLogin()
    {
      int id;

      if (_memberRepository.FetchByLogin(_view.LoginName, _cryptographyProvider.Encrypting(_view.Password, _view.CryptoKey),
        out id))
      {
        Domain.Entity.Member member = _memberRepository.FetchById(id);

        if (member != null)
        {
          _view.OnMemberLoggedIn(new LoginDto(id, member.MemberName, _view.LoginName, _view.Remember, member.Role));

          _memberRepository.UpdateLastLogin(id, DateTime.Now);
        }
      }
    }

    #endregion
  }
}