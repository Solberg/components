﻿namespace Ssl.Clubsite.Member
{
    using Domain;
    using Ssl.Security.Cryptography;
    using Dynamite.Extensions;

    public class MemberListPresenter
    {
        #region Variables

        private readonly IMemberListView _memberListView;

        private readonly IMemberRepository _memberRepository;

      readonly ICryptographyProvider cryptographyProvider;
        
        #endregion

        #region Constructors
        public MemberListPresenter(IMemberListView memberListView, IMemberRepository memberRepository, ICryptographyProvider cryptographyProvider)
        {
            this._memberListView = memberListView;
            this._memberRepository = memberRepository;
            this.cryptographyProvider = cryptographyProvider;
        }
        #endregion

        #region Public

        public void Initialize()
        {
          var items = this._memberRepository.List();
          items.Sort(_memberListView.Sortorder);
          this._memberListView.Members = items;
        }


        public string GetPassword(int id)
        {
            return cryptographyProvider.Decrypting(this._memberRepository.FetchById(id).Password, _memberListView.CryptoKey);
        }

        #endregion


    }
}
