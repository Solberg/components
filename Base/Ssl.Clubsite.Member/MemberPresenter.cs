﻿namespace Ssl.Clubsite.Member
{
    using System;

    using Domain;
    using Domain.Entity;

    using Security.Cryptography;

    public class MemberPresenter
    {
        #region Variables

        private readonly IMemberView memberView;

        private readonly IMemberRepository memberRepository;

        private readonly ICryptographyProvider cryptographyProvider;

        #endregion

        #region Constructors
        public MemberPresenter(IMemberView memberView, IMemberRepository memberRepository, ICryptographyProvider cryptographyProvider)
        {
            this.memberView = memberView;
            this.memberRepository = memberRepository;
            this.cryptographyProvider = cryptographyProvider;
        }
        #endregion

        #region Public

        /// <summary>
        /// Load info for a member
        /// </summary>
        public void Load()
        {
            if (this.memberView.MemberId != 0)
            {
                Member member = this.memberRepository.FetchById(this.memberView.MemberId);

                this.memberView.Name = member.MemberName;
                this.memberView.Login = member.LoginName;
                this.memberView.Mail = member.Mail;
                this.memberView.Phone = member.Phone;
                this.memberView.Role = member.Role;

                try
                {
                    this.memberView.Password = this.cryptographyProvider.Decrypting(
                        member.Password, this.memberView.CryptoKey);
                }
                catch (Exception)
                {
                }
            }
        }


        /// <summary>
        /// Update memberrecord
        /// </summary>
        public void Update()
        {
            var pwd = !string.IsNullOrEmpty(memberView.PasswordNew) ? memberView.PasswordNew : memberView.Password;

            var member = new Member(this.memberView.MemberId, this.memberView.Name, string.Empty, string.Empty, string.Empty, this.memberView.Login, this.cryptographyProvider.Encrypting(pwd, this.memberView.CryptoKey), this.memberView.Phone, this.memberView.Mail, this.memberView.Role, DateTime.MinValue);

            if (member.MemberId == 0)
            {
                this.memberRepository.Create(member);
            }
            else
            {
                this.memberRepository.Update(member);                
            }
        }


        #endregion
    }
}
