﻿namespace Ssl.Member
{
    using System.Data.SqlClient;

    public class MemberService : SSL.Data.SqlServer.Dbo.dboMember
    {
        #region Constructors
        public MemberService()
        {
        }

        public MemberService(bool IbAutoClose)
            : base(IbAutoClose)
        {
        }
        #endregion

        #region Methods
        public override SqlConnection Connect()
        {
            return new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnSqlServer"].ConnectionString);
        }
        #endregion
    }
}
