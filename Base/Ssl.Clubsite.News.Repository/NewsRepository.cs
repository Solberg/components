﻿namespace Ssl.Clubsite.News.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data;
    using System.Data.SqlClient;

    using Domain;
    using Domain.Entity;

    using SSL.Data.SqlServer;

    using SqlParameterCollection = SSL.Data.SqlServer.SqlParameterCollection;

    public class NewsRepository : SqlClient, IListRepository<NewsEntry>
    {

        #region Variables

        private readonly string _connectionString;
        #endregion


        #region Constructors
        public NewsRepository(string connectionString)
    {
            _connectionString = connectionString;
    }

    public NewsRepository(string connectionString, bool autoClose)
      : base(autoClose)
    {
        _connectionString = connectionString;
    }
    #endregion


    #region Public
    public override SqlConnection Connect()
    {
        return new SqlConnection(_connectionString);
    }
    #endregion



        #region Public
        public List<NewsEntry> List()
        {
            DataView dv = StandardList("spNews_Select");

            return (from DataRow dr in dv.Table.Rows
                    select new NewsEntry(Convert.ToInt32(dr["Id"]), DbConvert.ToDateTime(dr["EntryDate"]), dr["Text"].ToString())).ToList();

        }

        public void Create(NewsEntry newsEntry)
        {
            var spc = new SqlParameterCollection
                {
                    { new SqlParameter("@IsText", SqlDbType.VarChar, 255), newsEntry.Text },
                    { new SqlParameter("@IdEntryDate", SqlDbType.DateTime), newsEntry.TimeStamp }
                };

            StandardCreate("spNewsCreate", ref spc);
        }

        #endregion



    }
}
