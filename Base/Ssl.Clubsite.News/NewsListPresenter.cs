﻿namespace Ssl.Clubsite.News
{
    using Domain;

    public class NewsListPresenter
    {
        #region Variables

        private readonly INewsListView view;

        private readonly INewsRepository newsRepository;
        #endregion

        #region Constructors
        public NewsListPresenter(INewsListView view, INewsRepository newsRepository)
        {
            this.view = view;
            this.newsRepository = newsRepository;
        }
        #endregion

        #region Public
        public void Initialize()
        {
            this.view.Items = this.newsRepository.List();
        }
        #endregion
    }
}
