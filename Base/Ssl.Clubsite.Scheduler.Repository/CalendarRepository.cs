﻿namespace Ssl.Clubsite.Calendar.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using Ssl.Clubsite.Domain;
    using Ssl.Clubsite.Domain.Entity;

    using SSL.Data.SqlServer;

    public class CalendarRepository : SqlClient, ICalendarRepository
    {
        #region Variables

        private readonly string connectionString;

        #endregion

        #region Constructors
        public CalendarRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public CalendarRepository(string connectionString, bool IbAutoClose) : base(IbAutoClose)
        {
            this.connectionString = connectionString;
        }

        #endregion

        #region Protected
        /// <summary>
        /// Connect to the database.
        /// </summary>
        /// <returns></returns>
        public override SqlConnection Connect()
        {
            return new SqlConnection(this.connectionString);
        }
        #endregion

        /// <summary>
        /// Setup inputparameters for Create/Update Stored Procedures.
        /// </summary>
        /// <returns></returns>
        protected virtual void SetupParameters(DateTime IdEventDate,
          DateTime IdStartTime,
          DateTime IdEndTime,
          string IsDescription,
          int IiUserId,
          ref SSL.Data.SqlServer.SqlParameterCollection DaParams)
        {
            DaParams.Add(new SqlParameter("@IdEventDate", SqlDbType.SmallDateTime), IdEventDate);
            DaParams.Add(new SqlParameter("@IdStartTime", SqlDbType.SmallDateTime), IdStartTime);
            DaParams.Add(new SqlParameter("@IdEndTime", SqlDbType.SmallDateTime), IdEndTime);
            DaParams.Add(new SqlParameter("@IsDescription", SqlDbType.VarChar, 255), IsDescription);
            DaParams.Add(new SqlParameter("@IiUserId", SqlDbType.Int), IiUserId);
        }



        public List<CalendarEntry> List(DateTime startDate)
        {
            var pt = new SSL.Data.SqlServer.SqlParameterCollection
                { { new SqlParameter("@IdStartDate", SqlDbType.SmallDateTime), startDate } };

            DataView dv = StandardList("spSchedule_Select", pt, String.Empty);

            return (from DataRow dr in dv.Table.Rows
                    select new CalendarEntry(Convert.ToInt32(dr["Id"]), Convert.ToDateTime(dr["EventDate"]), Convert.ToDateTime(dr["StartTime"]), Convert.ToDateTime(dr["EndTime"]), dr["Description"].ToString(), Convert.ToInt32(dr["Userid"] ))).ToList();
        }

        public void Create(CalendarEntry CalendarEntry)
        {
            var pt = new SSL.Data.SqlServer.SqlParameterCollection();

            SetupParameters(CalendarEntry.EventDate,
              CalendarEntry.StartTime,
              CalendarEntry.EndTime,
              CalendarEntry.Description,
              CalendarEntry.MemberId,
              ref pt);

            StandardCreate("spSchedule_Insert", ref pt);
        }

        public void Update(CalendarEntry calendarEntry)
        {
            var pt = new SSL.Data.SqlServer.SqlParameterCollection
                { { new SqlParameter("@IiId", SqlDbType.Int), calendarEntry.Id } };

            SetupParameters(calendarEntry.EventDate,
              calendarEntry.StartTime,
              calendarEntry.EndTime,
              calendarEntry.Description,
              calendarEntry.MemberId,
              ref pt);

            StandardCreate("spSchedule_Update", ref pt);
        }

        public void Delete(int id)
        {
            var pt = new SSL.Data.SqlServer.SqlParameterCollection { { new SqlParameter("@IiId", SqlDbType.Int), id } };

            StandardDelete("spSchedule_Delete", pt);
        }

        public CalendarEntry Fetch(int id)
        {
            var pt = new SSL.Data.SqlServer.SqlParameterCollection
                {
                    { new SqlParameter("@IiId", SqlDbType.Int), id },
                    { new SqlParameter("@OdEventDate", SqlDbType.SmallDateTime), ParameterDirection.Output },
                    { new SqlParameter("@OdStartTime", SqlDbType.SmallDateTime), ParameterDirection.Output },
                    { new SqlParameter("@OdEndTime", SqlDbType.SmallDateTime), ParameterDirection.Output },
                    { new SqlParameter("@OsDescription", SqlDbType.VarChar, 255), ParameterDirection.Output },
                    { new SqlParameter("@OiUserId", SqlDbType.Int), ParameterDirection.Output }
                };

            StandardFetch("spScheduleFetch", ref pt);

            return new CalendarEntry(id,
                DbConvert.ToDateTime(pt[1].Value),
                DbConvert.ToDateTime(pt[2].Value),
                DbConvert.ToDateTime(pt[3].Value),
                DbConvert.ToString(pt[4].Value),
                DbConvert.ToInt32(pt[5].Value));
        }


    }
}
