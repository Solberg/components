﻿using System;

namespace Ssl.Clubsite.Scheduler
{
    using Domain;
    using Domain.Entity;

    public class CalendarEntryPresenter
    {
        #region Variables

        private readonly ICalendarEntryView view;

        private readonly ICalendarRepository repository;
        #endregion

        #region Constructor

        public CalendarEntryPresenter(ICalendarEntryView view, ICalendarRepository repository)
        {
            this.view = view;
            this.repository = repository;
        }

        #endregion

        #region Event-handlers
        public EventHandler OnButtonClicked;
        #endregion


        #region public

        public void Update()
        {
            if (this.view.IsValid)
            {

                if (this.view.Id == 0)
                {
                    this.repository.Create(new CalendarEntry(0, this.view.EventDate, this.view.StartTime, this.view.EndTime, this.view.Description, this.view.MemberId));
                }
                else
                {
                    this.repository.Update(new CalendarEntry(this.view.Id, this.view.EventDate, this.view.StartTime, this.view.EndTime, this.view.Description, this.view.MemberId));
                }

                if (OnButtonClicked != null)
                    OnButtonClicked(this, EventArgs.Empty);
                
            }
        }

        public void Delete()
        {
            this.repository.Delete(this.view.Id);

            if (OnButtonClicked != null)
                OnButtonClicked(this, EventArgs.Empty);
            
        }
        
        #endregion

    }
}
