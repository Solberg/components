﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Scheduler
{
    using Domain;

    public class CalendarListPresenter
    {
        #region Variables

        private readonly ICalendarListView view;

        private readonly ICalendarRepository repository;
        #endregion

        #region Constructor

        public CalendarListPresenter(ICalendarListView view, ICalendarRepository repository)
        {
            this.view = view;
            this.repository = repository;
        }

        #endregion

        #region Public
        public void Initialize()
        {
            this.view.StartDate = GetMonday(DateTime.Now);
            this.view.EntryDate = DateTime.Now;
            this.RefreshData();
        }

        public void RefreshData()
        {
            this.view.Entries = this.repository.List(this.view.StartDate);            
        }

        public void Jump(DateTime startDate)
        {
            this.view.StartDate = GetMonday(startDate);
            this.RefreshData();
        }

        public void GetEntry(int id)
        {
            this.view.CurrentEntry = this.repository.Fetch(id);
        }

        #endregion

        #region Private

        private static DateTime GetMonday(DateTime IdDate)
        {
            int iDay = Convert.ToInt32(IdDate.DayOfWeek);
            if (iDay == 0)
                return IdDate.AddDays(-6);
            else
                return IdDate.AddDays(-(iDay - 1));

        }

        #endregion

    }
}
