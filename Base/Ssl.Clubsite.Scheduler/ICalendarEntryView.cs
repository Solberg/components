﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Scheduler
{
    public interface ICalendarEntryView
    {
        bool IsValid { get; }

        int Id { get; }

        DateTime EventDate { get; }

        DateTime StartTime { get; }

        DateTime EndTime { get; }

        int MemberId { get; }
        string Description { get; }
    }
}
