﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ssl.Clubsite.Scheduler
{
    using Domain.Entity;

    public interface ICalendarListView
    {
        List<CalendarEntry> Entries { set; }

        DateTime StartDate { get; set; }

        DateTime EntryDate { get; set; }

        CalendarEntry CurrentEntry { set; }
    }
}
