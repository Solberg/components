﻿namespace Ssl.Security.Cryptography
{
    public interface ICryptoView
    {
        string CryptoKey { get; }
    }
}
