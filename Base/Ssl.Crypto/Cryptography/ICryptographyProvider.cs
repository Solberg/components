﻿namespace Ssl.Security.Cryptography
{
    public interface ICryptographyProvider
    {
        string Decrypting(string source, string key);

        string Encrypting(string source, string key);
    }
}
