﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SymmCrypto.cs" company="Alka Forsikring">
// 2011  
// </copyright>
// <summary>
//   SymmCrypto is a wrapper of System.Security.Cryptography.SymmetricAlgorithm classes
//   and simplifies the interface. It supports customized SymmetricAlgorithm as well.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Ssl.Security.Cryptography
{
    using System;
    using System.Text;
    using System.IO;
    using System.Security.Cryptography;

    /// <summary>
    /// SymmCrypto is a wrapper of System.Security.Cryptography.SymmetricAlgorithm classes
    /// and simplifies the interface. It supports customized SymmetricAlgorithm as well.
    /// </summary>
    public class SymmCrypto : ICryptographyProvider
    {
        #region Constants and Fields

        /// <summary>
        /// The mobj crypto service.
        /// </summary>
        private readonly SymmetricAlgorithm mobjCryptoService;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmCrypto"/> class. 
        /// The symm crypto.
        /// </summary>
        /// <param name="NetSelected">
        /// The Net Selected.
        /// </param>
        /// <remarks>
        /// Constructor for using an intrinsic .Net SymmetricAlgorithm class.
        /// </remarks>
        public SymmCrypto(SymmProvEnum NetSelected)
        {
            switch (NetSelected)
            {
                case SymmProvEnum.Des:
                    this.mobjCryptoService = new DESCryptoServiceProvider();
                    break;
                case SymmProvEnum.Rc2:
                    this.mobjCryptoService = new RC2CryptoServiceProvider();
                    break;
                case SymmProvEnum.Rijndael:
                    this.mobjCryptoService = new RijndaelManaged();
                    break;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SymmCrypto"/> class. 
        /// The symm crypto.
        /// </summary>
        /// <param name="ServiceProvider">
        /// The Service Provider.
        /// </param>
        /// <remarks>
        /// Constructor for using a customized SymmetricAlgorithm class.
        /// </remarks>
        public SymmCrypto(SymmetricAlgorithm ServiceProvider)
        {
            this.mobjCryptoService = ServiceProvider;
        }

        #endregion

        #region Enums

        /// <summary>
        /// The symm prov enum.
        /// </summary>
        /// <remarks>
        /// Supported .Net intrinsic SymmetricAlgorithm classes.
        /// </remarks>
        public enum SymmProvEnum
        {
            /// <summary>
            /// DES cryptography method
            /// </summary>
            Des,

            /// <summary>
            /// The r c 2.
            /// </summary>
            Rc2,

            /// <summary>
            /// The rijndael.
            /// </summary>
            Rijndael
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The decrypting.
        /// </summary>
        /// <param name="Source">
        /// The source.
        /// </param>
        /// <param name="Key">
        /// The key.
        /// </param>
        /// <returns>
        /// The decrypting.
        /// </returns>
        public string Decrypting(string Source, string Key)
        {
            // convert from Base64 to binary
            byte[] bytIn = Convert.FromBase64String(Source);

            // create a MemoryStream with the input
            var ms = new MemoryStream(bytIn, 0, bytIn.Length);

            byte[] bytKey = this.GetLegalKey(Key);

            // set the private key
            this.mobjCryptoService.Key = bytKey;
            this.mobjCryptoService.IV = bytKey;

            // create a Decryptor from the Provider Service instance
            ICryptoTransform encrypto = this.mobjCryptoService.CreateDecryptor();

            // create Crypto Stream that transforms a stream using the decryption
            var cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Read);

            // read out the result from the Crypto Stream
            var sr = new StreamReader(cs);

            return sr.ReadToEnd();
        }

        /// <summary>
        /// The encrypting.
        /// </summary>
        /// <param name="Source">
        /// The source.
        /// </param>
        /// <param name="Key">
        /// The key.
        /// </param>
        /// <returns>
        /// The encrypting.
        /// </returns>
        public string Encrypting(string Source, string Key)
        {
            byte[] bytIn = Encoding.ASCII.GetBytes(Source);

            // create a MemoryStream so that the process can be done without I/O files
            var ms = new MemoryStream();

            byte[] bytKey = this.GetLegalKey(Key);

            // set the private key
            this.mobjCryptoService.Key = bytKey;
            this.mobjCryptoService.IV = bytKey;

            // create an Encryptor from the Provider Service instance
            ICryptoTransform encrypto = this.mobjCryptoService.CreateEncryptor();

            // create Crypto Stream that transforms a stream using the encryption
            var cs = new CryptoStream(ms, encrypto, CryptoStreamMode.Write);

            // write out encrypted content into MemoryStream
            cs.Write(bytIn, 0, bytIn.Length);
            cs.FlushFinalBlock();

            // get the output and trim the '\0' bytes
            byte[] bytOut = ms.GetBuffer();
            int i;

            for (i = bytOut.Length - 1; i >= 0; i--)
            {
                if (bytOut[i] != 0)
                {
                    break;
                }
            }

            i++;

            // convert into Base64 so that the result can be used in xml
            return Convert.ToBase64String(bytOut, 0, i);
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get legal key.
        /// </summary>
        /// <param name="Key">
        /// The Key.
        /// </param>
        /// <remarks>
        /// Depending on the legal key size limitations of a specific CryptoService provider
        /// and length of the private key provided, padding the secret key with space character
        /// to meet the legal size of the algorithm.
        /// </remarks>
        private byte[] GetLegalKey(string Key)
        {
            string temp = Key;
            if (this.mobjCryptoService.LegalKeySizes.Length > 0)
            {
                int moreSize = this.mobjCryptoService.LegalKeySizes[0].MinSize;

                // key sizes are in bits
                if (temp.Length * 8 > this.mobjCryptoService.LegalKeySizes[0].MaxSize)
                {
                    // get the left of the key up to the max size allowed
                    temp = temp.Substring(0, this.mobjCryptoService.LegalKeySizes[0].MaxSize / 8);
                }
                else if (temp.Length * 8 < moreSize)
                {
                    if (this.mobjCryptoService.LegalKeySizes[0].SkipSize == 0)
                    {
                        // simply pad the key with spaces up to the min size allowed
                        temp = temp.PadRight(moreSize / 8, ' ');
                    }
                    else
                    {
                        while (temp.Length * 8 > moreSize)
                        {
                            moreSize += this.mobjCryptoService.LegalKeySizes[0].SkipSize;
                        }

                        temp = temp.PadRight(moreSize / 8, ' ');
                    }
                }
            }

            // convert the secret key to byte array
            return Encoding.ASCII.GetBytes(temp);
        }

        #endregion
    }
}
