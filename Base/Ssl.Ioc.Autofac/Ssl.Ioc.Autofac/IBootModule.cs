﻿using System.Reflection;
using Autofac;

namespace Ssl.Ioc.Autofac
{
  public interface IBootModule
  {
    void Configure(ContainerBuilder builder, Assembly executingAssembly);

    void SetResolver(IContainer container);


  }
}