﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace Ssl.Ioc.Autofac
{
  public class MvcModule : IBootModule
  {

    public void Configure(ContainerBuilder builder, Assembly executingAssembly)
    {
      // Register dependencies in controllers for MVC
      builder.RegisterControllers(executingAssembly);

      // Register dependencies in filter attributes MVC
      builder.RegisterFilterProvider();

      // Register dependencies in custom views MVC
      builder.RegisterSource(new ViewRegistrationSource());

    }


    public void SetResolver(IContainer container)
    {
      // Set MVC dependencyresolver
      DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
      
    }


  }
}