﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;

namespace Ssl.Ioc.Autofac
{

  /// <summary>
  /// Set autofac configuration for web api
  /// </summary>
  public class WebApiModule : IBootModule
  {

    private readonly HttpConfiguration _config;

    public WebApiModule(HttpConfiguration config)
    {
      _config = config;
    }

    public void Configure(ContainerBuilder builder, Assembly executingAssembly)
    {
      // Register your Web API controllers.
      builder.RegisterApiControllers(executingAssembly);

      // OPTIONAL: Register the Autofac filter provider for Web API.
      builder.RegisterWebApiFilterProvider(_config);

    }

    public void SetResolver(IContainer container)
    {
      // set DependencyResolver for Web Api
      _config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

    }
  }
}