﻿using System.Threading;
using Autofac;
using Autofac.Integration.Wcf;
using Autofac.Util;
using SSL.Domain;
using SSL.Ioc;
using Ssl.Ioc.Lifetime;
using Ssl.Ioc.Mvp;
using SSL.Mvp;
using SSL.Runtime.State;
using SSL.Web.Runtime.Context;
using SSL.Web.Runtime.Context.Impl;
using SSL.Web.Runtime.State;

namespace Ssl.Ioc.Web.BootStrapping
{
  /// <summary>
  /// Bootloader der opsætter IOC og MVP.
  /// </summary>
  public class BootLoader : Disposable, IBootLoader
  {

    #region Variables

    private IContainer container;

    #endregion

    #region IBootLoader implementation
    
    /// <summary>
    ///Boot og sæt op.
    /// </summary>
    /// <revision version="4.1.0" date="20140122" programmer="SSL">Support for Wcf</revision>
    /// <revision version="4.2.0" date="20140328" programmer="SSL">Registrering af ServiceLocator</revision>
    public void Boot()
    {
      container = BuildContainer();

      AutofacHostFactory.Container = container;

      var requestLifetimeScopeProvider = new RequestLifetimeProvider(container);

      // MVP integration
      PresenterFactory.SetPresenterFactory(new AutofacPresenterFactory(requestLifetimeScopeProvider));

      var trackingLifetimeScopeContext = new TrackingLifetimeContext(container, container.Resolve<ILocalStateFactory>());

      // opret autofac servicelocator'en med den angivne scope context
      var locator = new AutofacServiceLocator(trackingLifetimeScopeContext);

      //tilknytter servicelocator'en til den statiske kontrakt
      ServiceLocator.SetLocator(locator);
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        var lt = container;
        Interlocked.CompareExchange(ref container, null, lt);

        if (container != null)
        {
          container.Dispose();
        }
      }

      base.Dispose(disposing);
    }

    #endregion

    #region Protected


    /// <summary>
    /// Bygger boot sequencen. Registres alle moduler
    /// </summary>
    /// <param name="builder">IOC Containerbuilder.</param>
    protected virtual void BuildBootSequence(ContainerBuilder builder)
    {
    }

    #endregion


    #region Private

    private IContainer BuildContainer()
    {
      var builder = new ContainerBuilder();

      builder.RegisterInstance(new DefaultEnvironmentContext()).As<IEnvironmentContext>();

      // State
      builder.RegisterType<DefaultLocalStateFactory>().As<ILocalStateFactory>().SingleInstance();

      BuildBootSequence(builder);

      return builder.Build();
    }

    #endregion

  }

}