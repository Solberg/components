﻿using System;
using System.Web;
using Ssl.Ioc.Lifetime;

namespace Ssl.Ioc.Web
{
  /// <summary>
  /// An <see cref="IHttpModule"/> and <see cref="ILifetimeContext"/> implementation 
  /// that creates a nested lifetime scope for each HTTP request.
  /// </summary>
  /// <remarks>
  /// Registreres i web.config under HttpModules.
  /// </remarks>
  public class RequestLifetimeHttpModule : IHttpModule
  {
    private static ILifetimeProvider _lifetimeProvider;

    /// <summary>
    /// Initializes a module and prepares it to handle requests.
    /// </summary>
    /// <param name="context">An <see cref="T:System.Web.HttpApplication"/> that provides access to the 
    /// methods, properties, and events common to all application objects within an ASP.NET application</param>
    public void Init(HttpApplication context)
    {
      context.BeginRequest += OnBeginRequest;
      context.EndRequest += OnEndRequest;
    }

    /// <summary>
    /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
    /// </summary>
    public void Dispose()
    {
    }

    private static void OnBeginRequest(object sender, EventArgs e)
    {
      var controllerProvider = _lifetimeProvider;
      if (controllerProvider != null)
      {
        controllerProvider.BeginLifetime();
      }
    }

    /// <summary>
    /// Sets the lifetime provider.
    /// </summary>
    /// <param name="lifetimeProvider">The lifetime provider.</param>
    public static void SetLifetimeProvider(ILifetimeProvider lifetimeProvider)
    {
      _lifetimeProvider = lifetimeProvider;
    }

    private static void OnEndRequest(object sender, EventArgs e)
    {
      var controllerProvider = _lifetimeProvider;
      if (controllerProvider != null)
      {
        controllerProvider.EndLifetime();
      }
    }
  }
}