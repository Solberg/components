﻿using System;
using System.Web;
using Autofac;
using Ssl.Ioc.Lifetime;

namespace Ssl.Ioc.Web
{
  public class RequestLifetimeProvider : ILifetimeProvider
  {
    /// <summary>
    /// Tag used to identify registrations that are scoped to the HTTP request level.
    /// </summary>
    internal static readonly object HttpRequestTag = "httpRequest";

    private readonly Action<ContainerBuilder> _configurationAction;
    private readonly ILifetimeScope _container;

    /// <summary>
    /// Initializes a new instance of the <see cref="RequestLifetimeProvider"/> class.
    /// </summary>
    /// <param name="container">The parent container, usually the application container.</param>
    public RequestLifetimeProvider(ILifetimeScope container)
      : this(container, null)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="RequestLifetimeProvider"/> class.
    /// </summary>
    /// <param name="container">The parent container, usually the application container.</param>
    /// <param name="configurationAction">Action on a <see cref="ContainerBuilder"/>
    /// that adds component registations visible only in HTTP request lifetime scopes.</param>
    public RequestLifetimeProvider(ILifetimeScope container, Action<ContainerBuilder> configurationAction)
    {
      if (container == null)
      {
        throw new ArgumentNullException("container");
      }

      _container = container;
      _configurationAction = configurationAction;

      RequestLifetimeHttpModule.SetLifetimeProvider(this);
    }

    /// <summary>
    /// Gets the configuration action that adds component registations 
    /// visible only in HTTP request lifetime scopes.
    /// </summary>
    protected Action<ContainerBuilder> ConfigurationAction
    {
      get { return _configurationAction; }
    }

    /// <summary>
    /// Gets a lifetime scope that services can be resolved from.
    /// </summary>
    /// <value></value>
    /// <returns>A existing lifetime scope for the current HTTP request.</returns>
    public ILifetimeScope Current
    {
      get
      {
        if (HttpContext.Current == null)
        {
          throw new InvalidOperationException("HttpContext is not available");
        }
        return (ILifetimeScope)HttpContext.Current.Items[typeof(ILifetimeScope)];
      }
      private set { HttpContext.Current.Items[typeof(ILifetimeScope)] = value; }
    }

    /// <summary>
    /// Gets the global, application-wide container.
    /// </summary>
    public ILifetimeScope Application
    {
      get { return _container; }
    }

    /// <summary>
    /// Create a new HTTP request lifetime scope that services can be resolved from.
    /// </summary>
    public void BeginLifetime()
    {
      if (Current == null)
      {
        if ((Current = CreateLifetime()) == null)
        {
          throw new InvalidOperationException(
              string.Format("NullLifetimeScopeReturned {0}", GetType().FullName));
        }
      }
    }

    /// <summary>
    /// Ends the current HTTP request lifetime scope.
    /// </summary>
    public void EndLifetime()
    {
      var lifetimeScope = Current;
      if (lifetimeScope != null)
      {
        lifetimeScope.Dispose();
        Current = null;
      }
    }

    /// <summary>
    /// Gets a lifetime scope for the current HTTP request. This method can be overridden
    /// to alter the way that the life time scope is constructed.
    /// </summary>
    /// <returns>A new lifetime scope for the current HTTP request.</returns>
    protected virtual ILifetimeScope CreateLifetime()
    {
      return (ConfigurationAction == null)
                 ? Application.BeginLifetimeScope(HttpRequestTag)
                 : Application.BeginLifetimeScope(HttpRequestTag, ConfigurationAction);
    }
  }
}