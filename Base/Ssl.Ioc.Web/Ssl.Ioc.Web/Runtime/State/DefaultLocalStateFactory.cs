using ALKA.Runtime.State;
using SSL.Runtime.State;
using SSL.Web.Runtime.Context;

namespace SSL.Web.Runtime.State
{
  /// <summary>
    /// Default implementation of <see cref="ILocalStateFactory"/>.
    /// </summary>
    public class DefaultLocalStateFactory : ILocalStateFactory
    {
        private readonly IEnvironmentContext _environmentContext;

        /// <summary>
        /// Default Constructor.
        /// Creates an instance of <see cref="DefaultLocalStateFactory"/> class.
        /// </summary>
        /// <param name="environmentContext">An instance of <see cref="IEnvironmentContext"/>.</param>
        public DefaultLocalStateFactory(IEnvironmentContext environmentContext)
        {
            _environmentContext = environmentContext;
        }

        #region ILocalStateSelector Members

        /// <summary>
        /// Gets the <see cref="ILocalState"/> instance to use.
        /// </summary>
        /// <returns></returns>
        public ILocalState Create()
        {
            if (_environmentContext.IsWcfApplication)
            {
                return new WcfLocalState(_environmentContext);
            }
            if (_environmentContext.IsWebApplication)
            {
                return new HttpLocalState(_environmentContext);
            }
            return new ThreadLocalState();
        }

        #endregion
    }
}