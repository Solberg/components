using Alka.Core.Runtime.State;
using SSL.Runtime.State;
using SSL.Web.Runtime.Context;

namespace SSL.Web.Runtime.State
{
    /// <summary>
    /// Default implementation of <see cref="ISessionStateFactory"/>.
    /// </summary>
    public class DefaultSessionStateFactory : ISessionStateFactory
    {
        private readonly IEnvironmentContext _environmentContext;

        /// <summary>
        /// Default Constructor.
        /// Creates a new instance of <see cref="DefaultLocalStateFactory"/> class.
        /// </summary>
        /// <param name="environmentContext">An instance of <see cref="DefaultLocalStateFactory"/>.</param>
        public DefaultSessionStateFactory(IEnvironmentContext environmentContext)
        {
            _environmentContext = environmentContext;
        }

        #region ISessionStateSelector Members

        /// <summary>
        /// Gets the implementation of <see cref="ISessionState"/> to use.
        /// </summary>
        /// <returns></returns>
        public ISessionState Create()
        {
            if (_environmentContext.IsWcfApplication)
            {
                if (_environmentContext.IsAspNetCompatEnabled)
                {
                    return new HttpSessionState(_environmentContext);
                }
                return new WcfSessionState(_environmentContext);
            }
            if (_environmentContext.IsWebApplication)
            {
                return new HttpSessionState(_environmentContext);
            }
            return null;
        }

        #endregion
    }
}