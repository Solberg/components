using ALKA.Runtime.State;
using SSL.Runtime.State;
using SSL.Web.Runtime.Context;

namespace SSL.Web.Runtime.State
{
    /// <summary>
    /// Implementation of <see cref="ISessionState"/> that uses the current HttpContext's session.
    /// </summary>
    public class HttpSessionState : ISessionState
    {
        private readonly IEnvironmentContext _environmentContext;

        /// <summary>
        /// Default Constructor.
        /// Creates a new instance of the <see cref="HttpSessionState"/> class.
        /// </summary>
        /// <param name="environmentContext">An instance of <see cref="IEnvironmentContext"/>.</param>
        public HttpSessionState(IEnvironmentContext environmentContext)
        {
            _environmentContext = environmentContext;
        }

        #region ISessionState Members

        /// <summary>
        /// Gets state data stored with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to retrieve.</typeparam>
        /// <returns>An instance of <typeparamref name="T"/> or null if not found.</returns>
        public T Get<T>()
        {
            return Get<T>(null);
        }

        /// <summary>
        /// Gets state data stored with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to retrieve.</typeparam>
        /// <param name="key">An object representing the unique key with which the data was stored.</param>
        /// <returns>An instance of <typeparamref name="T"/> or null if not found.</returns>
        public T Get<T>(object key)
        {
            T result = default(T);
            var fullKey = key.BuildFullKey<T>();
            if (_environmentContext.HttpContext.Session != null)
            {
                result = (T)_environmentContext.HttpContext.Session[fullKey];
            }
            return result;
        }

        /// <summary>
        /// Puts state data into the session state with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        public void Put<T>(T instance)
        {
            Put(null, instance);
        }

        /// <summary>
        /// Puts state data into the session state with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="key">An object representing the unique key with which the data is stored.</param>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        public void Put<T>(object key, T instance)
        {
            var fullKey = key.BuildFullKey<T>();
            if (_environmentContext.HttpContext.Session != null)
            {
                _environmentContext.HttpContext.Session[fullKey] = instance;
            }
        }

        /// <summary>
        /// Removes state data stored in the session state with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to remove.</typeparam>
        public void Remove<T>()
        {
            Remove<T>(null);
        }

        /// <summary>
        /// Removes state data stored in the session state with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to remove.</typeparam>
        /// <param name="key">An object representing the unique key with which the data was stored.</param>
        public void Remove<T>(object key)
        {
            var fullKey = key.BuildFullKey<T>();
            if (_environmentContext.HttpContext.Session != null)
            {
                _environmentContext.HttpContext.Session.Remove(fullKey);
            }
        }

        /// <summary>
        /// Clears all state data stored in the session.
        /// </summary>
        public void Clear()
        {
            if (_environmentContext.HttpContext.Session != null)
            {
                _environmentContext.HttpContext.Session.Clear();
            }
        }

        #endregion
    }
}