using Alka.Core.Runtime.State;
using ALKA.Runtime.State;
using SSL.Runtime.State;

namespace SSL.Web.Runtime.State
{
    /// <summary>
    /// Implementation of <see cref="ISessionState"/> that wraps a <see cref="ISessionState"/> instance
    /// from a <see cref="ISessionStateFactory"/>.
    /// </summary>
    public class SessionStateWrapper : ISessionState
    {
        private readonly ISessionState _sessionState;

        /// <summary>
        /// Default Constructor.
        /// Creates a new instance of the <see cref="SessionStateWrapper"/> class.
        /// </summary>
        /// <param name="sessionStateFactory">A <see cref="ISessionStateFactory"/> instance used to retrieve the underlying
        /// <see cref="ISessionState"/> instance.</param>
        public SessionStateWrapper(ISessionStateFactory sessionStateFactory)
        {
            _sessionState = sessionStateFactory.Create();
        }

        #region ISessionState Members

        /// <summary>
        /// Gets state data stored with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to retrieve.</typeparam>
        /// <returns>An instance of <typeparamref name="T"/> or null if not found.</returns>
        public T Get<T>()
        {
            return Get<T>(null);
        }

        /// <summary>
        /// Gets state data stored with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to retrieve.</typeparam>
        /// <param name="key">An object representing the unique key with which the data was stored.</param>
        /// <returns>An instance of <typeparamref name="T"/> or null if not found.</returns>
        public T Get<T>(object key)
        {
            return _sessionState.Get<T>(key);
        }

        /// <summary>
        /// Puts state data into the session state with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        public void Put<T>(T instance)
        {
            Put(null, instance);
        }

        /// <summary>
        /// Puts state data into the session state with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="key">An object representing the unique key with which the data is stored.</param>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        public void Put<T>(object key, T instance)
        {
            _sessionState.Put(key, instance);
        }

        /// <summary>
        /// Removes state data stored in the session state with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to remove.</typeparam>
        public void Remove<T>()
        {
            Remove<T>(null);
        }

        /// <summary>
        /// Removes state data stored in the session state with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to remove.</typeparam>
        /// <param name="key">An object representing the unique key with which the data was stored.</param>
        public void Remove<T>(object key)
        {
            _sessionState.Remove<T>(key);
        }

        /// <summary>
        /// Clears all state data stored in the session.
        /// </summary>
        public void Clear()
        {
            _sessionState.Clear();
        }

        #endregion
    }
}