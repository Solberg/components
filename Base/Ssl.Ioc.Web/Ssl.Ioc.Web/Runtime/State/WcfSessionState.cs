using System.Collections;
using System.ServiceModel;
using ALKA.Runtime.State;
using SSL.Runtime.State;
using SSL.Web.Runtime.Context;

namespace SSL.Web.Runtime.State
{
  /// <summary>
    /// Implementation of <see cref="ISessionState"/> that stores session data in the current wcf session.
    /// </summary>
    public class WcfSessionState : ISessionState
    {
        private readonly WcfSessionStatExtension _wcfSessionStatExtension;

        /// <summary>
        /// Default Constructor.
        /// Creates a new instance of <see cref="WcfSessionState"/> class.
        /// </summary>
        /// <param name="environmentContext">An instance of <see cref="IEnvironmentContext"/>.</param>
        public WcfSessionState(IEnvironmentContext environmentContext)
        {
            _wcfSessionStatExtension = environmentContext.OperationContext.InstanceContext.Extensions.Find<WcfSessionStatExtension>();
            if (_wcfSessionStatExtension == null)
            {
                lock (environmentContext.OperationContext.InstanceContext)
                {
                    _wcfSessionStatExtension = environmentContext.OperationContext.InstanceContext.Extensions.Find<WcfSessionStatExtension>();
                    if (_wcfSessionStatExtension == null)
                    {
                        _wcfSessionStatExtension = new WcfSessionStatExtension();
                        environmentContext.OperationContext.InstanceContext.Extensions.Add(_wcfSessionStatExtension);
                    }
                }
            }
        }

        #region ISessionState Members

        /// <summary>
        /// Gets state data stored with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to retrieve.</typeparam>
        /// <returns>An instance of <typeparamref name="T"/> or null if not found.</returns>
        public T Get<T>()
        {
            return Get<T>(null);
        }

        /// <summary>
        /// Gets state data stored with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to retrieve.</typeparam>
        /// <param name="key">An object representing the unique key with which the data was stored.</param>
        /// <returns>An instance of <typeparamref name="T"/> or null if not found.</returns>
        public T Get<T>(object key)
        {
            return (T)_wcfSessionStatExtension.Get(key.BuildFullKey<T>());
        }

        /// <summary>
        /// Puts state data into the session state with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        public void Put<T>(T instance)
        {
            Put(null, instance);
        }

        /// <summary>
        /// Puts state data into the session state with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to put.</typeparam>
        /// <param name="key">An object representing the unique key with which the data is stored.</param>
        /// <param name="instance">An instance of <typeparamref name="T"/> to store.</param>
        public void Put<T>(object key, T instance)
        {
            _wcfSessionStatExtension.Add(key.BuildFullKey<T>(), instance);
        }

        /// <summary>
        /// Removes state data stored in the session state with the default key.
        /// </summary>
        /// <typeparam name="T">The type of data to remove.</typeparam>
        public void Remove<T>()
        {
            Remove<T>(null);
        }

        /// <summary>
        /// Removes state data stored in the session state with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of data to remove.</typeparam>
        /// <param name="key">An object representing the unique key with which the data was stored.</param>
        public void Remove<T>(object key)
        {
            _wcfSessionStatExtension.Remove(key.BuildFullKey<T>());
        }

        /// <summary>
        /// Clears all state data stored in the session.
        /// </summary>
        public void Clear()
        {
            _wcfSessionStatExtension.Clear();
        }

        #endregion

        #region Nested type: WcfSessionStatExtension

        /// <summary>
        /// Implementation of <see cref="IExtension{T}"/> of type <see cref="InstanceContext"/> that stores
        /// session state data in the current <see cref="InstanceContext"/>.
        /// </summary>
        private class WcfSessionStatExtension : IExtension<InstanceContext>
        {
            private Hashtable _state = new Hashtable();

            #region IExtension<InstanceContext> Members

            public void Attach(InstanceContext owner)
            {
            }

            public void Detach(InstanceContext owner)
            {
                _state.Clear();
                _state = null;
            }

            #endregion

            /// <summary>
            /// Adds state data with the given key.
            /// </summary>
            /// <param name="key">string. The unique key.</param>
            /// <param name="instance">object. The state data to store.</param>
            public void Add(string key, object instance)
            {
                lock (_state.SyncRoot)
                {
                    _state.Add(key, instance);
                }
            }

            /// <summary>
            /// Gets state data stored with the specified unique key.
            /// </summary>
            /// <param name="key">string. The unique key.</param>
            /// <returns>object. A non-null reference if the data is found, else null.</returns>
            public object Get(string key)
            {
                lock (_state.SyncRoot)
                {
                    return _state[key];
                }
            }

            /// <summary>
            /// Removes state data stored with the specified unique key.
            /// </summary>
            /// <param name="key">string. The unique key.</param>
            public void Remove(string key)
            {
                lock (_state.SyncRoot)
                {
                    _state.Remove(key);
                }
            }

            /// <summary>
            /// Clears all state data.
            /// </summary>
            public void Clear()
            {
                lock (_state.SyncRoot)
                {
                    _state.Clear();
                }
            }
        }

        #endregion
    }
}