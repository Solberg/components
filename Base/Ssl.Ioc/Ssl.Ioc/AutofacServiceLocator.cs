﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using SSL.Ioc;
using Ssl.Ioc.Lifetime;

namespace Ssl.Ioc
{
  /// <summary>
    /// A Autofac ServiceLocator.
    /// </summary>
    public class AutofacServiceLocator : IServiceLocator
    {
        private readonly ILifetimeContext _lifetimeContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutofacServiceLocator"/> class.
        /// </summary>
        /// <param name="lifetimeContext">The lifetime scope resolver.</param>
        public AutofacServiceLocator(ILifetimeContext lifetimeContext)
        {
            if (lifetimeContext == null)
            {
                throw new ArgumentNullException("lifetimeContext");
            }
            _lifetimeContext = lifetimeContext;
        }

        /// <summary>
        /// The lifetime containing components for processing the current HTTP request.
        /// </summary>
        private ILifetimeScope Lifetime
        {
            get { return _lifetimeContext.Current; }
        }

        #region IServiceLocator Members

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        public TService GetInstance<TService>()
        {
            return Lifetime.Resolve<TService>();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public TService GetInstance<TService>(string key)
        {
            return Lifetime.ResolveNamed<TService>(key);
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="serviceType">The type.</param>
        /// <returns></returns>
        public object GetInstance(Type serviceType)
        {
            return Lifetime.Resolve(serviceType);
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="serviceType">The type.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public object GetInstance(Type serviceType, string key)
        {
            return Lifetime.ResolveNamed(key, serviceType);
        }

        /// <summary>
        /// Gets all instances.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        public IEnumerable<TService> GetAllInstances<TService>()
        {
            return Lifetime.Resolve<IEnumerable<TService>>();
        }

        /// <summary>
        /// Gets all instances.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            var enumerableServiceType = typeof(IEnumerable<>).MakeGenericType(serviceType);
            var instance = Lifetime.Resolve(enumerableServiceType);
            return ((IEnumerable)instance).Cast<object>();
        }

        #endregion
    }
}