﻿using Autofac;

namespace Ssl.Ioc.Lifetime
{
  /// <summary>
    /// A Lifetime Context
    /// </summary>
    public interface ILifetimeContext
    {
        /// <summary>
        /// Gets a lifetime scope that services can be resolved from.
        /// </summary>
        /// <returns>A The current lifetime scope.</returns>
        ILifetimeScope Current { get; }

        /// <summary>
        /// Gets the global, application-wide container.
        /// </summary>
        ILifetimeScope Application { get; }
    }
}
