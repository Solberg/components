﻿namespace Ssl.Ioc.Lifetime
{
  /// <summary>
    /// En Lifetime Provider
    /// </summary>
    public interface ILifetimeProvider : ILifetimeContext
    {
        /// <summary>
        /// Begins the lifetime scope.
        /// </summary>
        void BeginLifetime();

        /// <summary>
        /// Ends the lifetime scope.
        /// </summary>
        void EndLifetime();
    }
}
