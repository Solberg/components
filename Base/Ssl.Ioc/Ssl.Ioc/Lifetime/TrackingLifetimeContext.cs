﻿using System.Collections.Generic;
using ALKA.Runtime.State;
using Autofac;
using Autofac.Core.Lifetime;
using SSL.Runtime.State;

namespace Ssl.Ioc.Lifetime
{
  /// <summary>
    /// Track every lifetime created
    /// </summary>
    public class TrackingLifetimeContext : ILifetimeContext
    {
        private readonly ILifetimeScope _container;
        private readonly ILocalStateFactory _localStateFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackingLifetimeContext"/> class.
        /// </summary>
        /// <param name="container">The parent container, usually the application container.</param>
        /// <param name="localStateFactory">The local state selector.</param>
        public TrackingLifetimeContext(ILifetimeScope container, ILocalStateFactory localStateFactory)
        {
            _container = container;
            _localStateFactory = localStateFactory;

            _container.ChildLifetimeScopeBeginning += ContainerChildLifetimeScopeBeginning;
            _container.CurrentScopeEnding += LifetimeScopeCurrentScopeEnding;
        }

        private Stack<ILifetimeScope> Tracked
        {
            get
            {
                var state = _localStateFactory.Create();
                var tracked = state.Get<Stack<ILifetimeScope>>();
                if (tracked == null)
                {
                    tracked = new Stack<ILifetimeScope>();
                    state.Put(tracked);
                }
                return tracked;
            }
        }

        private void ContainerChildLifetimeScopeBeginning(object sender, LifetimeScopeBeginningEventArgs e)
        {
            Tracked.Push(e.LifetimeScope);

            e.LifetimeScope.CurrentScopeEnding += LifetimeScopeCurrentScopeEnding;
            e.LifetimeScope.ChildLifetimeScopeBeginning += ContainerChildLifetimeScopeBeginning;
        }

        private void LifetimeScopeCurrentScopeEnding(object sender, LifetimeScopeEndingEventArgs e)
        {
            Tracked.Pop();

            e.LifetimeScope.CurrentScopeEnding -= LifetimeScopeCurrentScopeEnding;
            e.LifetimeScope.ChildLifetimeScopeBeginning -= ContainerChildLifetimeScopeBeginning;
        }

        /// <summary>
        /// Gets a lifetime scope that services can be resolved from.
        /// </summary>
        /// <value></value>
        /// <returns>A The current lifetime scope.</returns>
        public ILifetimeScope Current
        {
            get { return Tracked.Count == 0 ? _container : Tracked.Peek(); }
        }

        /// <summary>
        /// Gets the global, application-wide container.
        /// </summary>
        /// <value></value>
        public ILifetimeScope Application
        {
            get { return _container;  }
        }
    }
}
