﻿using System;
using Autofac;
using Autofac.Core;
using Ssl.Ioc.Lifetime;
using SSL.Mvp;

namespace Ssl.Ioc.Mvp
{
  /// <summary>
  /// En Autofac PresenterFactory
  /// </summary>
  public class AutofacPresenterFactory : IPresenterFactory
  {
    private readonly ILifetimeContext lifetimeContext;

    /// <summary>
    /// Initializes a new instance of the <see cref="AutofacPresenterFactory"/> class.
    /// </summary>
    /// <param name="lifetimeContext">The lifetime context.</param>
    public AutofacPresenterFactory(ILifetimeContext lifetimeContext)
    {
      if (lifetimeContext == null)
      {
        throw new Exception("Lifetimecontext is null");
      }

      this.lifetimeContext = lifetimeContext;
    }

    #region Implementation of IPresenterFactory

    /// <summary>
    /// Instantier en forekomst af presenter ved at søge i lifetimeContext.
    /// </summary>
    /// <typeparam name="TPresenter">The type of the presenter.</typeparam>
    /// <param name="view">The view.</param>
    /// <returns></returns>
    public TPresenter Create<TPresenter>(IView view) where TPresenter : class, IPresenter
    {
      return lifetimeContext.Current.Resolve<TPresenter>(new ResolvedParameter((a, b) => a.ParameterType.IsAssignableFrom(view.GetType()), (a, b) => view));
    }

    #endregion
  }
}