﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Xml.Linq;
using SSL.Domain;

namespace Ssl.Logging.Tests
{
    
    [TestFixture]
    public class LoggerTests
    {

      private XDocument _xdoc;

      [TestFixtureSetUp]
      public void Init()
      {
        _xdoc = XDocument.Parse("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><Log></Log>");
      }

        [Test]
        public void be_able_to_log()
        {
            _xdoc.Descendants("Entry").Remove();
            
            var logger = new Logger<LogEntry>(_xdoc, new LogEntryMapper()); 
          
            logger.Log(new LogEntry(1, "message", DateTime.Now));

            Assert.IsNotNull(_xdoc.Descendants("Message").FirstOrDefault());
            Assert.IsNotNull(_xdoc.Descendants("Timestamp").FirstOrDefault());
        }

     

      [Test]
      public void be_able_to_list()
      {
        _xdoc.Descendants("Entry").Remove();        

        var logger = new Logger<LogEntry>(_xdoc, new LogEntryMapper());

        logger.Log(new LogEntry(1, "message",  DateTime.Now));
        logger.Log(new LogEntry(2, "message",  DateTime.Now.AddMonths(-2)));

        var result = logger.List(DateTime.Now.Date, DateTime.Now.Date);

        Assert.IsNotNull(result);
        Assert.IsTrue(result.Count == 1);
      }
 

        [Test]
      public void be_able_to_fetch()
        {
          _xdoc.Descendants("Entry").Remove();

        var logger = new Logger<LogEntry>(_xdoc, new LogEntryMapper());

        logger.Log(new LogEntry(1, "message",  DateTime.Now));
        logger.Log(new LogEntry(2, "message",  DateTime.Now.AddMonths(-2)));

        var result = logger.Fetch(1);

        Assert.IsNotNull(result);
        
      }

        [Test]
        public void be_able_to_delete()
        {
          _xdoc.Descendants("Entry").Remove();

          var logger = new Logger<LogEntry>(_xdoc, new LogEntryMapper());

          logger.Log(new LogEntry(1, "message", DateTime.Now));
          logger.Log(new LogEntry(2, "message", DateTime.Now.AddMonths(-2)));

          logger.Delete(1);

          Assert.IsTrue(_xdoc.Descendants("Entry").Count()==1);
        }

        [Test]
        public void return_null_on_fetch_not_found()
        {
          _xdoc.Descendants("Entry").Remove();

          var logger = new Logger<LogEntry>(_xdoc, new LogEntryMapper());

          logger.Log(new LogEntry(1, "message", DateTime.Now));
          logger.Log(new LogEntry(2, "message",  DateTime.Now.AddMonths(-2)));

          var result = logger.Fetch(3);

          Assert.IsNull(result);
        }
 

    }

  
}
