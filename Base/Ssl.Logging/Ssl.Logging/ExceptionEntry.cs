﻿using System;
using System.Runtime.Serialization;

namespace Ssl.Logging
{

    [DataContract]
    public class ExceptionEntry : SSL.Domain.LogEntry
    {

        [DataMember]
        public string Stacktrace { get; private set; }

        public ExceptionEntry(int? id, string message, string stacktrace, DateTime? timestamp) : base(id, message, timestamp)
        {
          Stacktrace = stacktrace;
        }

      public ExceptionEntry(string message, string stacktrace, DateTime? timestamp) : base(message, timestamp)
      {
        Stacktrace = stacktrace;
      }
    }
}
