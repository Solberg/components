﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SSL.Domain;
using SSL.Extensions;

namespace Ssl.Logging
{
  public class ExceptionEntryMapper : IEntryMapper<ExceptionEntry>
  {
    public ExceptionEntry Map(XElement element)
    {
      return new ExceptionEntry(element.Element("Id").ToInt32(),
                element.Element("Message").ToStr(),
                element.Element("Stacktrace").ToStr(),
                element.Element("Timestamp").ToNullableDateTime());
    }

    public XElement Map(ExceptionEntry entry)
    {
      return new XElement("Entry",
        new XElement("Id", entry.Id),
        new XElement("Message", entry.Message),
        new XElement("Stacktrace", entry.Stacktrace),
        new XElement("Timestamp",  entry.Timestamp == null ? DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") : ((DateTime)entry.Timestamp).ToString("dd.MM.yyyy HH:mm:ss")));
    }
  }
}
