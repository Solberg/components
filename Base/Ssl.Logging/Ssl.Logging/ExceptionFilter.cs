﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SSL.Domain;

namespace Ssl.Logging
{

  /// <summary>
  /// Filter exceptions
  /// </summary>
  public class ExceptionFilter : IFilter<Exception>
  {
    /// <summary>
    /// List of filters from the provider.
    /// </summary>
    protected List<FilterEntry> Filters { get; set; }

    public ExceptionFilter(IFilterProvider filterProvider)
    {
      Filters = filterProvider.List();
    }

    /// <summary>
    /// Determine if a exception should be filtered out
    /// </summary>
    /// <param name="item"></param>
    /// <returns>Null if exception is caught by the filter else teh exception it self.</returns>
    public virtual Exception Filter(Exception item)
    {
      
      // Match on name of exception
      if (Filters.Any(c => c.FilterType == FilterType.ClassName && c.FilterText == item.GetType().ToString()))
      {
        return null;
      }

      // Match message text regular expression
      if (Filters.Any(c => c.FilterType == FilterType.RegEx && new Regex(c.FilterText, RegexOptions.IgnoreCase).IsMatch(item.Message)))
      {
        return null;
      }

      return item;
    }
  }
}
