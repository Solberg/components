﻿namespace Ssl.Logging
{
  public class FilterEntry
  {
    public FilterEntry(FilterType filterType, string filterText)
    {
      FilterText = filterText;
      FilterType = filterType;
    }

    /// <summary>
    /// Class name, regular expression etc.
    /// </summary>
    public FilterType FilterType { get; private set; }

    /// <summary>
    /// Filter by this could be a class name og a string to match
    /// </summary>
    public string FilterText { get; private set; }

  }
}
