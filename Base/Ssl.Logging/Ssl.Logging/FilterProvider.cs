﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SSL.Extensions;

namespace Ssl.Logging
{
  public class FilterProvider : IFilterProvider
  {

    private readonly XDocument _xdoc;

    public FilterProvider(XDocument xdoc)
    {
      _xdoc = xdoc;
    }

    public List<FilterEntry> List()
    {

      return (from element in _xdoc.Descendants("filter")
        select
          new FilterEntry((FilterType) element.Element("filtertype").ToInt32(), element.Element("filtertext").ToStr()))
        .ToList();
    }
  }
}
