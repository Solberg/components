﻿namespace Ssl.Logging
{
  public enum FilterType
  {
    ClassName = 1,
    RegEx = 2,
    Browser = 3,
    Ip = 4
  }
}