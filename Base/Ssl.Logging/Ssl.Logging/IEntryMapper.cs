﻿using System.Xml.Linq;

namespace Ssl.Logging
{

  /// <summary>
  /// Maps an logentry to and from xml.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IEntryMapper<T> where T : class
  {

    /// <summary>
    /// Map xml to an entry
    /// </summary>
    /// <param name="element"></param>
    /// <returns></returns>
    T Map(XElement element);

    /// <summary>
    /// Map entry to xml
    /// </summary>
    /// <param name="entry"></param>
    /// <returns></returns>
    XElement Map(T entry);

  }
}