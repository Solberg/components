﻿using System.Collections.Generic;

namespace Ssl.Logging
{

  /// <summary>
  /// Provider which gets filters from a datastore
  /// </summary>
  public interface IFilterProvider
  {

    /// <summary>
    /// List all filters
    /// </summary>
    /// <returns></returns>
    List<FilterEntry> List();

  }
}