﻿using System.Collections.Generic;
using SSL.Domain;
using SSL.Mvp;

namespace Ssl.Logging
{
  public interface ILogViewer<T> : IView where T : ExceptionEntry
  {

    List<T> Items { set; }

    Period Period { get; }
 
    string Ticket { set; }

  }
}