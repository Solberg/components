﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SSL.Domain;
using SSL.Extensions;

namespace Ssl.Logging
{
  public class LogEntryMapper : IEntryMapper<LogEntry>
  {
    public LogEntry Map(XElement element)
    {
      return new LogEntry(element.Element("Id").ToInt32(),
                element.Element("Message").ToStr(),
                element.Element("Timestamp").ToNullableDateTime());
    }

    public XElement Map(LogEntry entry)
    {
      return new XElement("Entry",
        new XElement("Id", entry.Id),
        new XElement("Message", entry.Message),
        new XElement("Timestamp",  entry.Timestamp == null ? DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") : ((DateTime)entry.Timestamp).ToString("dd.MM.yyyy HH:mm:ss")));
    }
  }
}
