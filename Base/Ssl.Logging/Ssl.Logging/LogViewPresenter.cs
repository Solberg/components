﻿using SSL.Domain;
using Dynamite.Extensions;
using SSL.Mvp;

namespace Ssl.Logging
{
  public class LogViewPresenter<T, TView> : Presenter<TView> where T : ExceptionEntry where TView : class, ILogViewer<T>
  {
    
    private readonly ILogger<T> _logger;

    private readonly ITicketFactory _ticketFactory;

    public LogViewPresenter(ILogViewer<T> view, ILogger<T> logger, ITicketFactory ticketFactory) : base((TView)view)
    {
      _logger = logger;
      _ticketFactory = ticketFactory;
    }


    public override void OnLoad(bool initializing)
    {
      if (initializing)
      {
        View.Ticket = _ticketFactory.GetTicket();
        GetEntries();
      }
    }
    
    public void GetEntries()
    {
      var period = View.Period;
      var items = _logger.List(period.From, period.To);
      items.Sort("Timestamp desc");
      View.Items = items;
    }

  }
}
