﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SSL.Domain;
using SSL.Extensions;

namespace Ssl.Logging
{
  public class Logger<T> : ILogger<T> where T : LogEntry
  {

    private object _lock = new object();

    private readonly XDocument _xdoc;

    private readonly IEntryMapper<T> _entryMapper;

    private readonly string _filePath;

    public Logger(string filePath, XDocument xdoc, IEntryMapper<T> entryMapper)
    {
      _xdoc = xdoc;
      _entryMapper = entryMapper;
      _filePath = filePath;
    }

    public void Log(T entry)
    {
      if (entry.Id == null)
      {
        entry.Id = GetNextId();
      }

      lock (_lock)
      {
        _xdoc.Descendants("Log").First().Add(_entryMapper.Map(entry));

        _xdoc.Save(_filePath); 
      }
    }


    public List<T> List(DateTime? fromTime, DateTime? toTime)
    {

      fromTime = fromTime ?? DateTime.Now.Date;

      toTime = toTime ?? DateTime.Now.AddDays(1).Date;

      return (from XElement element in _xdoc.Descendants("Entry")
              let timestamp = element.Element("Timestamp").ToNullableDateTime()
              where timestamp.Value.Date >= fromTime && timestamp.Value.Date <= toTime
              select _entryMapper.Map(element)).ToList();
    }

    public T Fetch(int id)
    {
      return (from XElement element in _xdoc.Descendants("Entry")
              let idValue = element.Element("Id").ToNullableInt32()
              where idValue == id
              select _entryMapper.Map(element)).FirstOrDefault();

    }

    public void Delete(int id)
    {
      lock (_lock)
      {
        _xdoc.Descendants("Entry").Where(c => c.Element("Id").ToNullableInt32() == id).Remove();

        _xdoc.Save(_filePath);
      }

    }


    private int GetNextId()
    {
      int? max = List(DateTime.MinValue, DateTime.MaxValue).Max(c => c.Id);

      return max == null ? 0 : (int)max + 1;
    }
    
  }
}
