﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ForumDetailPresenter.cs" company="">
//   
// </copyright>
// <summary>
//   The forum detail presenter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SSL.Forum
{
  using System;
  using System.Configuration;
  using System.Data.SqlClient;

  using SSL.Data.SqlServer;
  using SSL.Forum.ViewModel;

  /// <summary>
  /// The forum detail presenter.
  /// </summary>
  internal class ForumDetailPresenter
  {
    #region Constants and Fields

    /// <summary>
    /// View being presented
    /// </summary>
    private readonly IForumDetail view;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="ForumDetailPresenter"/> class.
    /// </summary>
    /// <param name="view">
    /// View being presented
    /// </param>
    public ForumDetailPresenter(IForumDetail view)
    {
      this.view = view;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// The load comment.
    /// </summary>
    public void LoadComment()
    {
      var myC = new SqlConnection
        {
          ConnectionString = ConfigurationManager.ConnectionStrings["ConnForum"].ConnectionString
        };

      string myQuery = "SELECT * FROM  CP_FORUM_Comments WHERE ID=" + this.view.CommentId + " and ArticleID=" +
                       this.view.ArticleId;

      myC.Open();
      var myCommand = new SqlCommand(myQuery, myC);

      SqlDataReader myReader = myCommand.ExecuteReader();
      try
      {
        if (myReader != null && myReader.HasRows)
        {
          while (myReader.Read())
          {
            this.view.AnswerTo = new ViewDetail(
              myReader["Title"].ToString(),
              myReader["Username"].ToString(),
              myReader["Description"].ToString(),
              myReader["UserEmail"].ToString(),
              Convert.ToDateTime(myReader["DateAdded"]));

            this.view.Subject = "Re: " + myReader["Title"];

            this.view.Indent = Convert.ToInt32(myReader["Indent"].ToString()) + 1;
          }
        }
      }
      finally
      {
        if (myReader != null)
        {
          myReader.Close();          
        }
      }
    }

    /// <summary>
    /// The post.
    /// </summary>
    /// <returns>
    /// The post.
    /// </returns>
    public bool Post()
    {
      try
      {
        if (this.view.Valid)
        {
          int mParentId = this.view.CommentId;
          int mArticleId = this.view.ArticleId;
          int mIndent = this.view.Indent;

          ViewDetail entry = this.view.NewEntry;

          string mTitle = entry.Title;
          string mUserName = entry.Name;
          string mUserEmail = entry.EMail ?? String.Empty;
          string mDescription = entry.Comment;
          
          string mProfile = this.view.Profile != null ? this.view.Profile : String.Empty;
          int mCommentType = this.view.CommentType;

          var myC = new SqlConnection
            {
              ConnectionString = ConfigurationManager.ConnectionStrings["ConnForum"].ConnectionString
            };

          string sqlQuery =
            "INSERT into CP_FORUM_Comments(ParentId,ArticleId,Title,UserName,UserEmail,Description,Indent,UserProfile,CommentType,UserId) VALUES ('" +
            mParentId + "','" + mArticleId + "','" + mTitle + "','" + mUserName + "','" + mUserEmail + "','" +
            DbConvert.ToSqlString(mDescription) + "','" + mIndent + "','" + mProfile + "','" + mCommentType + "','" +
            this.view.UserId + "')";
          myC.Open();
          var myCommand = new SqlCommand();
          myCommand.CommandText = sqlQuery;
          myCommand.Connection = myC;
          myCommand.ExecuteNonQuery();
          myC.Close();
          this.view.ShowMessage("Status: Success");
          return true;
        }
      }
      catch (Exception exc)
      {
        this.view.ShowError("Error: " + exc.Message);
      }

      return false;
    }

    #endregion
  }
}