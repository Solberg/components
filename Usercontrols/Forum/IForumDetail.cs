﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SSL.Forum
{
  using SSL.Forum.ViewModel;

  interface IForumDetail
  {
    bool Valid { get; }
    int ArticleId { get; set; }
    int CommentId { get; set; }
    int Indent { get; set; }
    string Profile { get; set; }
    ViewDetail AnswerTo { set; }
    ViewDetail NewEntry { set; get; }
    string Subject { get; set; }
    int UserId { get; set; }
    int CommentType { get; set; }

    void ShowError(string message);

    void ShowMessage(string message);
  }
}
