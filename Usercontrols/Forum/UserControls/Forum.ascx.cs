// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Forum.ascx.cs" company="Tripple S">
// 2011  
// </copyright>
// <summary>
//   The forum.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SSL.Forum.UserControls
{
  using System;
  using System.Data.SqlClient;
  using System.Text;
  using System.Web.UI;

  /// <summary>
  /// The forum.
  /// </summary>
  public partial class Forum : UserControl
  {
    #region Properties

    /// <summary>
    /// Gets or sets a value indicating whether Administrator.
    /// </summary>
    public bool Administrator { get; set; }

    /// <summary>
    /// Gets or sets LastForumLogin.
    /// </summary>
    public DateTime LastForumLogin { get; set; }

    /// <summary>
    /// Gets or sets PageSize.
    /// </summary>
    public int PageSize
    {
      get
      {
        object o = this.ViewState["PageSize"];
        if (o == null)
        {
          return 20;
        }

        return int.Parse(o.ToString());
      }

      set
      {
        this.ViewState["PageSize"] = value;
      }
    }

    /// <summary>
    /// Gets or sets ArticleId.
    /// </summary>
    private int ArticleId
    {
      get
      {
        if (this.ViewState["ArticleId"] == null)
        {
          this.ViewState["ArticleId"] = 0;
        }

        return Convert.ToInt32(this.ViewState["ArticleId"]);
      }

      set
      {
        this.ViewState["ArticleId"] = value;
      }
    }

    /// <summary>
    /// Gets or sets CurrentCount.
    /// </summary>
    private int CurrentCount
    {
      get
      {
        if (this.ViewState["CurrentCount"] == null)
        {
          this.ViewState["CurrentCount"] = 1;
        }

        return Convert.ToInt32(this.ViewState["CurrentCount"]);
      }

      set
      {
        this.ViewState["CurrentCount"] = value;
      }
    }

    #endregion

    #region Methods

    /// <summary>
    /// The on init.
    /// </summary>
    /// <param name="e">
    /// The e.
    /// </param>
    protected override void OnInit(EventArgs e)
    {
      // CODEGEN: This call is required by the ASP.NET Web Form Designer.
      InitializeComponent();
      base.OnInit(e);
    }

    /// <summary>
    /// The on load.
    /// </summary>
    /// <param name="e">
    /// The e.
    /// </param>
    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      if (this.Request.QueryString["Action"] == "Delete")
      {
        this.DeleteData();
      }

      if (!this.Page.IsPostBack)
      {
        this.PageSize = this.Request.QueryString["pagesize"] != null
                          ? Convert.ToInt32(this.Request.QueryString["pagesize"])
                          : 20;
        this.ArticleId = this.Request.QueryString["id"] != null ? Convert.ToInt32(this.Request.QueryString["id"]) : 1;
        this.CurrentCount = this.Request.QueryString["current"] != null
                              ? Convert.ToInt32(this.Request.QueryString["current"])
                              : 1;
      }
      else
      {
        this.PageSize = Convert.ToInt32(this.ddlPageSize.SelectedItem.Text);
      }

      this.LoadData();
    }

    /// <summary>
    /// The btn first_ click.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void btnFirst_Click(object sender, EventArgs e)
    {
      this.CurrentCount = 1;
      this.LoadData();
    }

    /// <summary>
    /// The btn last_ click.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void btnLast_Click(object sender, EventArgs e)
    {
      this.CurrentCount = Convert.ToInt32(this.btnLast.CommandArgument);
      this.LoadData();
    }

    /// <summary>
    /// The btn next_ click.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
      this.CurrentCount = this.CurrentCount + this.PageSize;
      this.LoadData();
    }

    /// <summary>
    /// The btn previous_ click.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
      this.CurrentCount = this.CurrentCount - this.PageSize;
      this.LoadData();
    }

    /// <summary>
    /// The ddl forums_ selected index changed.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void ddlForums_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.CurrentCount = 1;
      this.ArticleId = Convert.ToInt32(this.ddlForums.SelectedValue);
      this.LoadData();
    }

    /// <summary>
    /// The ddl page size_ selected index changed.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.LoadData();
    }

    /// <summary>
    /// The delete data.
    /// </summary>
    private void DeleteData()
    {
      int iArticleId = 0;
      int iCommentId = 0;

      // Put user code to initialize the page here
      if (this.Request.QueryString["id"] != null)
      {
        iArticleId = Convert.ToInt32(this.Request.QueryString["id"]);
      }

      if (this.Request.QueryString["CId"] != null)
      {
        iCommentId = Convert.ToInt32(this.Request.QueryString["CId"]);
      }

      try
      {
        var myclass = new clsDataAccess();
        myclass.openConnection();
        myclass.DeleteForumData(iArticleId, iCommentId);
        myclass.closeConnection();
      }
      catch (Exception)
      {
        this.Response.Write("<h2> Unexpected error ! Try slamming your head into your computer monitor :)</h2>");
      }
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
    }

    /// <summary>
    /// The load data.
    /// </summary>
    private void LoadData()
    {
      DateTime lastVisit = DateTime.Now;
      var sb = new StringBuilder();

      this.lblnewmessage.Text =
        String.Format(
          "<A title='Tilf�j nyt indl�g til aktuelt forum' href='NewMessage.aspx?id={0}'>Nyt indl�g</A>&nbsp;", 
          this.ArticleId);

      var myclass = new clsDataAccess();
      myclass.openConnection();

      SqlDataReader myReader = myclass.getForumData(this.ArticleId);

      int mycount = 1;

      while (myReader.Read())
      {
        DateTime dt1 = DateTime.Now;
        DateTime dt2 = Convert.ToDateTime(myReader["DateAdded"].ToString());
        if (mycount == 1)
        {
          lastVisit = Convert.ToDateTime(myReader["DateAdded"].ToString());
        }
        else
        {
          if (DateTime.Compare(lastVisit, dt2) < 0)
          {
            lastVisit = dt2;
          }
        }

        TimeSpan ts = dt1.Subtract(dt2);

        string mytimeago;
        if (Convert.ToInt32(ts.TotalDays) != 0)
        {
          mytimeago = string.Empty + Math.Abs(Convert.ToInt32(ts.TotalDays)) + " Dage siden";
        }
        else
        {
          if ((Convert.ToInt32(ts.TotalMinutes) < 5) && (Convert.ToInt32(ts.TotalHours) == 0))
          {
            mytimeago = "Lige tilf�jet";
          }
          else if ((Convert.ToInt32(ts.TotalMinutes) > 5) && (Convert.ToInt32(ts.TotalHours) == 0))
          {
            mytimeago = Convert.ToInt32(ts.TotalMinutes) % 60 + " Min siden";
          }
          else if (Convert.ToInt32(ts.TotalHours) != 0)
          {
            mytimeago = string.Empty + Convert.ToInt32(ts.TotalHours) + " Timer " + Convert.ToInt32(ts.TotalMinutes) % 60 +
                        " Mins ago";
          }
          else
          {
            mytimeago = Convert.ToInt32(ts.TotalMinutes) % 60 + " Min siden";
          }
        }

        string newimg = string.Empty;

        if (Convert.ToDateTime(myReader["DateAdded"]) > this.LastForumLogin)
        {
          newimg = "<img src='Forum/images/new.gif' border='0' alt=''>";
        }

        int myMaxCount = this.CurrentCount + Convert.ToInt32(this.PageSize);
        int myStartCount = this.CurrentCount;

        if (this.CurrentCount == -1)
        {
          myStartCount = 0;
          myMaxCount = 999;
        }

        if (mycount < myMaxCount && mycount >= myStartCount)
        {
          string sThreadClass = (mycount % 2) == 0 ? "ForumEven" : "ForumOdd";

          sb.Append(String.Format("<tr class='{0}' id='K1745932k" + mycount + "kOFF'>", sThreadClass));

          sb.Append("<td width='100%' colspan='1' style='padding:4px'>");
          sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%'>");
          sb.Append("<tr>");

          int myindent = 4;
          if (Convert.ToInt32(myReader["Indent"]) <= 4)
          {
            myindent = 16 * Convert.ToInt32(myReader["Indent"]);
          }
          else if (Convert.ToInt32(myReader["Indent"]) <= 8)
          {
            myindent = 15 * Convert.ToInt32(myReader["Indent"]);
          }
          else if (Convert.ToInt32(myReader["Indent"]) <= 16)
          {
            myindent = 14 * Convert.ToInt32(myReader["Indent"]);
          }
          else if (Convert.ToInt32(myReader["Indent"]) <= 20)
          {
            myindent = Convert.ToInt32(13.5 * Convert.ToDouble(myReader["Indent"]));
          }
          else if (Convert.ToInt32(myReader["Indent"]) <= 24)
          {
            myindent = 13 * Convert.ToInt32(myReader["Indent"]);
          }
          else if (Convert.ToInt32(myReader["Indent"]) <= 28)
          {
            myindent = Convert.ToInt32(12.7 * Convert.ToDouble(myReader["Indent"]));
          }
          else if (Convert.ToInt32(myReader["Indent"]) <= 32)
          {
            myindent = Convert.ToInt32(12.4 * Convert.ToDouble(myReader["Indent"]));
          }

          sb.Append(
            "<td><a name='xxK1745932k" + mycount + "kxx'></a><img height='1' width='" + myindent +
            "' src='Forum/images/ind.gif' alt=''>");

          if (Convert.ToInt32(myReader["CommentType"].ToString()) == 1)
          {
            sb.Append("<img align='middle' src='Forum/images/general.gif' alt=''>&nbsp;</td>");
          }

          if (Convert.ToInt32(myReader["CommentType"].ToString()) == 2)
          {
            sb.Append("<img align='middle' src='Forum/images/info.gif' alt=''>&nbsp;</td>");
          }

          if (Convert.ToInt32(myReader["CommentType"].ToString()) == 3)
          {
            sb.Append("<img align='middle' src='Forum/images/turn-on_16x16.gif' alt=''>&nbsp;</td>");
          }

          if (Convert.ToInt32(myReader["CommentType"].ToString()) == 4)
          {
            sb.Append("<img align='middle' src='Forum/images/question.gif' alt=''>&nbsp;</td>");
          }

          if (Convert.ToInt32(myReader["CommentType"].ToString()) == 5)
          {
            sb.Append("<img align='middle' src='Forum/images/game.gif' alt=''>&nbsp;</td>");
          }

          sb.Append(
            "<td width='100%' class='ForumOutput' style='padding-left:4px'><a  id='LinkTrigger" + mycount +
            "' name='K1745932k" + mycount + "k' href='K1745932#xxK1745932k" + mycount + "kxx'>");

          if (Convert.ToInt32(myReader["Indent"]) == 0)
          {
            sb.Append("<b>&nbsp;" + myReader["Title"] + "</b></a>" + newimg + "</td>");
          }
          else
          {
            sb.Append(
              "&nbsp;" + myReader["Title"] + "<!-- : " + myindent + "::" + Convert.ToInt32(myReader["Indent"]) +
              "--></a>" + newimg + "</td>");
          }

          // DateTime dt = DateTime.Now.CompareTo(Convert.ToDateTime(myReader["DateAdded"].ToString()));
          sb.Append(
            "<td valign='bottom' nowrap><a href='" + myReader["UserProfile"] +
            "'> <img src='Forum/images/userinfo.gif'  alt='' title='Click for User Profile' border='0' width='14' height='15'></a>&nbsp;</td>");
          sb.Append("<td width='100' nowrap class='ForumOutput'><b>" + myReader["UserName"] + "</b>&nbsp;</td>");
          sb.Append("<td nowrap align='right' width='100' class='ForumOutput'><b>" + mytimeago);
          sb.Append("</b>&nbsp;</td>");
          sb.Append("</tr>");
          sb.Append("</table>");
          sb.Append("</td>");
          sb.Append("</tr>");

          sb.Append("<tr id='K1745932k" + mycount + "kON' style='DISPLAY:none'>");

          sb.Append(String.Format("<td colspan='1' width='100%' class='{0}'>", sThreadClass));
          sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%'>");
          sb.Append("<tr>");
          sb.Append(
            "<td><img height='1' width='" + myindent +
            "' src='Forum/images/ind.gif' alt=''><img align='middle' src='Forum/images/blank.gif' height='30' width='28' alt=''>&nbsp;</td>");
          sb.Append("<td width='100%'><table border='0' cellspacing='5' cellpadding='0' width='100%'>");
          sb.Append("<tr>");
          sb.Append("<td>");
          sb.Append("<table border='0' cellspacing='0' cellpadding='0' width='100%'>");
          sb.Append("<tr>");
          sb.Append("<td colspan='2' class='ForumOutput'>");
          sb.Append(myReader["Description"].ToString().Replace(Environment.NewLine, "<br />"));
          sb.Append("<br>");
          sb.Append("&nbsp;</td>");
          sb.Append("</tr>");
          sb.Append("<tr valign='top'>");
          sb.Append(
            "<td colspan='3' style='text-align:left'><a href='Reply.aspx?id=" + this.ArticleId + "&amp;CID=" +
            myReader["ID"] +
            "' title='Svar p� den nuv�rende tr�d'><img width='16px' height='16px' src='forum/images/forum_newmsg.gif' border='0' /></a>");
          if (this.Administrator)
          {
            sb.Append(
              "&nbsp;<a href='Forum.aspx?id=" + this.ArticleId + "&amp;CID=" + myReader["ID"] + "&Action=Delete' ");
            sb.Append(
              "title='Slet aktuel tr�d'><img width='16px' height='16px' src='forum/images/trash_16x16.gif' border='0' /></a>");
          }

          sb.Append("</td>");
          sb.Append("</tr>");
          sb.Append("</table>");
          sb.Append("</td>");
          sb.Append("</tr>");
          sb.Append("</table>");
          sb.Append("</td>");
          sb.Append("</tr>");
          sb.Append("</table>");
          sb.Append("</td>");
          sb.Append("</tr>");
        }

        mycount++;
      }

      myReader.Close();
      myclass.closeConnection();

      this.btnFirst.Visible = this.CurrentCount > 1;
      this.btnPrevious.Visible = this.CurrentCount > 1;
      this.btnNext.Visible = this.CurrentCount + this.PageSize < mycount;
      this.btnLast.Visible = this.CurrentCount + this.PageSize < mycount;
      this.btnLast.CommandArgument = Convert.ToString(mycount - this.PageSize);

      this.ltlPost.Text = sb.ToString();

      this.lbldate.Text = "Sidste indl�g: " + lastVisit.ToShortTimeString() + ",  " + lastVisit.ToLongDateString();
      this.lblRecCount.Text = "<b>" + (mycount - 1) + "</b> records";
    }

    #endregion
  }
}