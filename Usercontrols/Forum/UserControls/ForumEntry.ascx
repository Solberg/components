<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForumEntry.ascx.cs" Inherits="SSL.Forum.UserControls.ForumEntry" %>
<%@ Register Assembly="SSL.Web" Namespace="SSL.Web.UI.WebControls" TagPrefix="SSL" %>

<script language="Javascript1.2"><!-- // load htmlarea
_editor_url = "Forum/htmlarea/";                     // URL to htmlarea files
var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }
if (win_ie_ver >= 5.5) {
 document.write('<scr' + 'ipt src="' +_editor_url+ 'editor.js"');
 document.write(' language="Javascript1.2"></scr' + 'ipt>');  
} else { document.write('<scr'+'ipt>function editor_generate() { return false; }</scr'+'ipt>'); }
// --></script>

<asp:panel runat="Server" ID="pnlComment">
<table id="Table1" border="0" width="100%">
  <tr>
    <td valign="top">
        <table width="100%" border="0">
          <tr>
            <td colspan="2" class="ForumHeader">
              Se kommentar
            </td>
          </tr>
          <tr>
            <td width="90px" class="ForumLabel">
              Titel:</td>
            <td class="ForumOutput">
              <asp:Label ID="lbltitle" runat="server">Label</asp:Label></td>
          </tr>
          <tr>
            <td class="ForumLabel" style="vertical-align:top">
              Kommentar:</td>
            <td class="ForumOutput" style="vertical-align:top">
              <asp:Label ID="lblComment" runat="server">Label</asp:Label></td>
          </tr>
          <tr>
            <td class="ForumLabel">
              Navn:</td>
            <td class="ForumOutput">
              <asp:Label ID="lblname" runat="server">Label</asp:Label></td>
          </tr>
          <tr>
            <td class="ForumLabel">
              Email:</td>
            <td class="ForumOutput">
              <asp:Label ID="lblemail" runat="server">Label</asp:Label></td>
          </tr>
          <tr>
            <td class="ForumLabel">
              Tilf�jet dato:</td>
            <td class="ForumOutput">
              <asp:Label ID="lblDate" runat="server">Label</asp:Label></td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td>
      &nbsp;
    </td>
    <td>
    </td>
  </tr>
  </table>
 </asp:panel>      
  <table style="width:100%" border="0">
  <tr>
    <td>
      <table id="Table2" width="100%" border="0">
        <tr>
          <td colspan="2" class="ForumHeader">
            Indl�g</td>
        </tr>
        <tr valign="middle">
          <td valign="middle" nowrap align="left" class="ForumLabel">
            Type:</td>
          <td valign="top" class="ForumInput">
            <input type="radio" name="MsgType" id="MsgType_1" value="1" runat="server"><label for="MsgType_1"><img title='General Comment' src='/images/Forum/general.gif' /> Generelt &nbsp;</label>
            <input type="radio" name="MsgType" id="MsgType_2" value="2" runat="server"><label for="MsgType_2"><img title='News' src='/Images/Forum/info.gif' /> Nyhed &nbsp;</label>
            <input type="radio" name="MsgType" id="MsgType_3" value="3" runat="server"><label for="MsgType_3"><img title='Forslag' src='/Images/Forum/turn-on_16x16.gif' /> Forslag &nbsp;</label>
            <input type="radio" name="MsgType" id="MsgType_4" value="4" runat="server"><label for="MsgType_4"><img title='Question' src='/Images/Forum/question.gif' /> Sp�rgsm�l &nbsp;</label>
          </td>
        </tr>
        <tr>
          <td width="90px" align="left" class="ForumLabel">
            Titel:</td>
          <td align="left">
            <asp:TextBox ID="txtsubject" runat="server" CssClass="ForumInput" Width="370px"></asp:TextBox>
            <SSL:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Font-Size="10pt" ControlToValidate="txtsubject" ErrorMessage="*" ValidationGroup="ForumEntry"></SSL:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td valign="top" align="left" class="ForumLabel">
            Svar:</td>
          <td align="left" style="vertical-align:top">
            <asp:TextBox ID="txtcomment" runat="server" CssClass="ForumInput" Height="104px"  Width="370px" TextMode="MultiLine"></asp:TextBox>
            <SSL:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtcomment" Font-Size="10pt" ValidationGroup="ForumEntry"></SSL:RequiredFieldValidator></td>
        </tr>
        <tr>
          <td align="left" class="ForumLabel">
            Navn:</td>
          <td align="left">
            <asp:TextBox ID="txtname" runat="server" CssClass="ForumInput" Width="370px"></asp:TextBox>
            <SSL:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
              ControlToValidate="txtname" Font-Size="10pt" ValidationGroup="ForumEntry"></SSL:RequiredFieldValidator>
          </td>
        </tr>
        <tr>
          <td align="left" class="ForumLabel">
            Email:</td>
          <td align="left">
            <asp:TextBox ID="txtemail" CssClass="ForumInput" runat="server" Width="370px"></asp:TextBox>
            <SSL:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
              ControlToValidate="txtemail" Font-Size="10pt" ValidationGroup="ForumEntry"></SSL:RequiredFieldValidator>
          </td>
        </tr>
        <tr style="display: none">
          <td align="left" class="ForumLabel">
            Bruger profil:</td>
          <td align="left">
            <asp:TextBox ID="txtProfile" runat="server" CssClass="ForumInput" Width="350px">http://codeproject.com</asp:TextBox>
          </td>
        </tr>
        <tr>
          <td align="left">
            &nbsp;</td>
          <td>
            &nbsp;</td>
        </tr>
        <tr>
          <td align="left">
            &nbsp;</td>
          <td>
            <asp:Button ID="btnPost" runat="server" CssClass="ForumButton" Width="90px" Text="Tilf�j" OnClick="btnPost_Click" ValidationGroup="ForumEntry"></asp:Button>&nbsp;&nbsp;
            <asp:Button ID="btnCancel" runat="server" CssClass="ForumButton" Width="90px" Text="Tilbage" CausesValidation="False" OnClick="btnCancel_Click" ValidationGroup="ForumEntry"></asp:Button></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
  <td><asp:Label runat="server" ID="lblStatus"></asp:Label></td>
  </tr>

</table>

<script language="JavaScript1.2" type="text/javascript">
///	editor_generate('ctl00$ContentPlaceHolder1$ForumEntry$txtcomment');
</script>

