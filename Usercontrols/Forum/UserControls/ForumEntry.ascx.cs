using System;
using System.Drawing;

namespace SSL.Forum.UserControls
{
  using SSL.Forum.ViewModel;

  public partial class ForumEntry : System.Web.UI.UserControl, IForumDetail
  {
    #region Variables

    private ForumDetailPresenter presenter;
    #endregion

    #region IForumDetail Implementation


    public ViewDetail AnswerTo
    {
      set
      {
        lbltitle.Text = value.Title;
        lblname.Text = value.Name;
        lblComment.Text = value.Comment;
        if (lblemail != null)
          lblemail.Text = value.EMail;

        lblDate.Text = value.DateAdded.ToString();

      }
    }

    public ViewDetail NewEntry
    {
      set
      {
        txtname.Text = value.Name;
        txtcomment.Text = value.Comment;
        txtsubject.Text = value.Title;
        txtemail.Text = value.EMail;
      }

      get
      {
        return new ViewDetail(txtsubject.Text, txtname.Text, txtcomment.Text, txtemail != null ? txtemail.Text : String.Empty, DateTime.Now);
      }
    }


    public string DateAdded
    {
      get
      {
        return lblDate.Text;
      }

      set
      {
        lblDate.Text = value;
      }

    }

    public string Title
    {
      get
      {
        return lbltitle.Text;

      }

      set
      {
        lbltitle.Text = value;
      }
    }

    public bool Valid
    {
      get
      {
        return Page.IsValid;
      }
    }

    public int ArticleId
    {
      get
      {
        return Convert.ToInt32(ViewState["ArticleId"]);
      }

      set
      {
        ViewState["ArticleId"] = value;
      }
    }

    public int CommentId
    {
      get
      {
        return Convert.ToInt32(ViewState["CommentId"]);
      }

      set
      {
        ViewState["CommentId"] = value;
      }
    }

    public int Indent
    {
      get
      {
        return Convert.ToInt32(ViewState["Indent"]);
      }

      set
      {
        ViewState["Indent"] = value;
      }
    }

    public string Subject
    {
      get
      {
        return txtsubject.Text;
      }
      set
      {
        txtsubject.Text = value;
      }
    }


    public string Profile
    {
      get
      {
        return txtProfile != null ? txtProfile.Text : String.Empty;
      }
      set
      {
        if (txtProfile != null)
        txtProfile.Text = value;
      }
    }

    public int CommentType
    {
      get
      {
        if (MsgType_1.Checked)
          return 1;
        else if (MsgType_2.Checked)
          return 2;
        else if (MsgType_3.Checked)
          return 3;
        else if (MsgType_4.Checked)
          return 4;
        else
          return 5;

      }
      set
      {
        if (value == 1) MsgType_1.Checked = true;
        else if (value==2) MsgType_2.Checked = true;
        else if (value==3) MsgType_3.Checked = true;
        else if (value==4) MsgType_4.Checked = true;
      }
    }

    public void ShowError(string message)
    {
      lblStatus.ForeColor = Color.Red;
      lblStatus.Text = message;
    }

    public void ShowMessage(string message)
    {
      lblStatus.ForeColor = Color.Green;
      lblStatus.Text = message;
    }





    public string UserName
    {
      set
      {
        txtname.Text = value;
        txtname.Enabled = false; /// If username is set from external source disable it.
      }
    }

    public int UserId
    {
      set
      {
        ViewState["UserId"] = value;
      }

      get
      {
        return ViewState["UserId"] != null ? Convert.ToInt32(ViewState["UserId"]) : 0;
      }
    }

    #endregion

    #region Events
    public event EventHandler OnNewEntry;
    #endregion

    #region Protected

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      presenter = new ForumDetailPresenter(this);

      if (txtcomment.MaxLength > 0)
      {
        txtcomment.Attributes.Add("maxlength", txtcomment.MaxLength.ToString());
      }
    }

    protected override void OnLoad(EventArgs e)
    {
 	    base.OnLoad(e);

      if (!Page.IsPostBack)
      {
        if (Request.QueryString["id"] != null)
          ArticleId = Convert.ToInt32(Request.QueryString["id"]);
        else
          ArticleId = 1;

        if (Request.QueryString["Cid"] != null)
        {
          pnlComment.Visible = true;
          CommentId = Convert.ToInt32(Request.QueryString["CID"]);          
          this.presenter.LoadComment();
        }
        else
        {
          pnlComment.Visible = false;
        }

        CommentType = 1;

        if (OnNewEntry != null)
        {
          OnNewEntry(this, e);
        }
      }
    }

    protected void btnPost_Click(object sender, EventArgs e)
    {
      if (presenter.Post())
      {
        Response.Redirect("Forum.aspx?id=" + ArticleId);        
      }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
      Response.Redirect("Forum.aspx?id=" + ArticleId);
    }



    #endregion
  }
}