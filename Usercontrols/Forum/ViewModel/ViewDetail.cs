﻿using System;

namespace SSL.Forum.ViewModel
{
  public class ViewDetail
  {
    #region Properties
    public string Title
    {
      get;
      set;
    }

    public string Comment
    {
      get;
      set;
    }

    public string Name
    {
      get;
      set;
    }

    public string EMail
    {
      get;
      set;
    }

    public DateTime DateAdded
    {
      get;
      set;
    }

    #endregion

    #region Constructors
    public ViewDetail(string title, string name, string comment, string eMail, DateTime dateAdded)
    {
      Title = title;
      Name = name;
      Comment = comment;
      EMail = eMail;
      DateAdded = dateAdded;
    }
    #endregion


  }
}
