using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SSL.UserControls.Directory
{

  [DefaultProperty("Href"), DefaultEvent("Click")] 
  public class DirectoryItem : WebControl, IPostBackEventHandler
  {

    #region Properties

  	public string Href { get; set; }

  	public string Text { get; set; }

  	public string PostBackEventReference { get; set; }

  	public bool UpDir { get; set; }

  	#endregion

    #region Constructors
    public DirectoryItem(string IsText,
      string IsToolTip,
      string IsHRef)
    {
      Text = IsText;
      ToolTip = IsToolTip;
      Href = IsHRef;
    }

    public DirectoryItem()
    {
    }

    #endregion

    #region Events
    public event EventHandler Click;
    #endregion

    #region Event-handlers

    /// <summary>
    /// 
    /// </summary>
    /// <param name="writer"></param>
    protected override void Render(HtmlTextWriter writer)
    {
      string sRef = PostBackEventReference ?? Page.ClientScript.GetPostBackEventReference(this, Href);

      HtmlTable tab = HtmlFunc.CreateTable(sRef,
        UpDir ? "/Images/Doc/FolderUp.gif" : "/Images/Doc/Folder50x50.gif",
        ToolTip,
        Text,
        CssClass);

      tab.RenderControl(writer);
    }
    #endregion

    #region IPostBackEventHandler

    /// <summary>
    /// Kaldes n�r script genereret fra GetPostBackEventReference kaldes.
    /// </summary>
    /// <param name="eventArgument"></param>
    public void RaisePostBackEvent(string eventArgument)
    {
      /// Kaldes hvis der er abonneret p� click event.
      if (Click != null)
      {
        Click(this, EventArgs.Empty);
      }

    }
    #endregion

  }
}
