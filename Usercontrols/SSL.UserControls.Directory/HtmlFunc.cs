using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace SSL.UserControls.Directory
{

  /// <summary>
  /// Contains function to generate various html snippets.
  /// </summary>
  public static class HtmlFunc
  {
    #region Methods
    public static void AddTableRow(Control Ictrl,
      HtmlTable DTab)
    {
      var cell = new HtmlTableCell();
      var row = new HtmlTableRow();

      cell.Style["text-align"] = "center";

      row.Cells.Add(cell);
      DTab.Rows.Add(row);

      cell.Controls.Add(Ictrl);
    }

    public static HtmlTable CreateTable(string IsHRef,
      string IsImgSrc,
      string IsImgAlt,
      string IsDivInnerTxt,
      string IsCssClass)
    {
      var tab = new HtmlTable();
      tab.Attributes["class"] = "DirectoryItem";

      var anchor = new HtmlAnchor {HRef = IsHRef};

    	var img = new HtmlImage {Src = IsImgSrc, Alt = IsImgAlt, Border = 0};

    	var div = new HtmlGenericControl {TagName = "div", InnerText = IsDivInnerTxt};

    	if (! String.IsNullOrEmpty(IsCssClass))
        div.Attributes.Add("class", IsCssClass);
    
      anchor.Controls.Add(img);

      AddTableRow(anchor, tab);
      AddTableRow(div, tab);

      return tab;
    }

    public static HtmlTable CreateTableNew(string IsHRef,
      string IsImgSrc,
      string IsImgAlt,
      string IsDivInnerTxt)
    {
      var tab = new HtmlTable();
      tab.Attributes["class"] = "DirectoryItem";

      //HtmlAnchor anchor = new HtmlAnchor();
      //anchor.HRef = IsHRef;

      var imgbtn = new ImageButton {ImageUrl = IsImgSrc, ToolTip = IsImgAlt, CommandName = IsHRef};

    	var div = new HtmlGenericControl {TagName = "div"};
    	div.Style["Width"] = "50px";
      div.InnerText = IsDivInnerTxt;


      AddTableRow(imgbtn, tab);
      AddTableRow(div, tab);

      return tab;
    }


    #endregion

  }
}
