// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ShowDirectory.ascx.cs" company="">
//   
// </copyright>
// <summary>
//   Show content of a library as windows explorer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SSL.UserControls.Directory
{
  using System;
  using System.Configuration;
  using System.IO;
  using System.Web;
  using System.Web.UI;
  using System.Web.UI.HtmlControls;
  using System.Web.UI.WebControls;

  /// <summary>
  /// Show content of a library as windows explorer.
  /// </summary>
  public partial class ShowDirectory : UserControl, IPostBackEventHandler
  {
    #region Constants and Fields

    /// <summary>
    /// The pnl directory.
    /// </summary>
    protected Panel pnlDirectory;

    /// <summary>
    /// The _ dir click text.
    /// </summary>
    private string dirClickText = "Click to open folder";

    /// <summary>
    /// The _ dir up click text.
    /// </summary>
    private string dirUpClickText = "Click to go one level up";

    /// <summary>
    /// The _ document viewer.
    /// </summary>
    private string documentViewer = "ShowDocument.aspx";

    /// <summary>
    /// The _ file click text.
    /// </summary>
    private string fileClickText = "Click to open. Filesize {0} kb";

    /// <summary>
    /// The _ level up.
    /// </summary>
    private string levelUp = "Up";

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets ColCount.
    /// </summary>
    public int ColCount { get; set; }

    /// <summary>
    /// Gets or sets CssClass.
    /// </summary>
    public string CssClass { get; set; }

    /// <summary>
    /// Gets or sets DirClickText.
    /// </summary>
    public string DirClickText
    {
      get
      {
        return this.dirClickText;
      }

      set
      {
        this.dirClickText = value;
      }
    }

    /// <summary>
    /// Gets or sets DirUpClickText.
    /// </summary>
    public string DirUpClickText
    {
      get
      {
        return this.dirUpClickText;
      }

      set
      {
        this.dirUpClickText = value;
      }
    }

    /// <summary>
    /// Gets or sets DocumentViewer.
    /// </summary>
    public string DocumentViewer
    {
      get
      {
        return this.documentViewer;
      }

      set
      {
        this.documentViewer = value;
      }
    }

    /// <summary>
    /// Gets or sets FileClickText.
    /// </summary>
    public string FileClickText
    {
      get
      {
        return this.fileClickText;
      }

      set
      {
        this.fileClickText = value;
      }
    }

    /// <summary>
    /// Gets or sets LevelUp.
    /// </summary>
    public string LevelUp
    {
      get
      {
        return this.levelUp;
      }

      set
      {
        this.levelUp = value;
      }
    }

    /// <summary>
    /// Gets or sets a value indicating whether UploadEnabled.
    /// </summary>
    public bool UploadEnabled { get; set; }

    /// <summary>
    /// Gets or sets CurrentDirectory.
    /// </summary>
    private string CurrentDirectory
    {
      get
      {
        return Convert.ToString(this.ViewState["CurrentDirectory"]);
      }

      set
      {
        this.ViewState["CurrentDirectory"] = value;
      }
    }

    #endregion

    #region Implemented Interfaces

    #region IPostBackEventHandler

    /// <summary>
    /// Kaldes n�r script genereret fra GetPostBackEventReference kaldes.
    /// </summary>
    /// <param name="eventArgument">
    /// </param>
    public void RaisePostBackEvent(string eventArgument)
    {
      this.GetDirectory(eventArgument);
    }

    #endregion

    #endregion

    #region Methods

    /// <summary>
    /// The on init.
    /// </summary>
    /// <param name="e">
    /// The e.
    /// </param>
    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      this.UCUpload.OnAfterUpload += this.UCUpload_OnAfterUpload;

      if (! this.Page.IsPostBack && this.Request.QueryString["Path"] != null)
      {
        this.GetDirectory(this.Request.QueryString["Path"]);
      }
    }

    /// <summary>
    /// The get directory.
    /// </summary>
    /// <param name="IsPath">
    /// The is path.
    /// </param>
    /// <exception cref="Exception">
    /// Kastes hvis inddata er null.
    /// </exception>
    private void GetDirectory(string IsPath)
    {
      if (IsPath == null)
      {
        throw new Exception("Path is null");
      }

      string documentPath = ConfigurationManager.AppSettings["DocumentPath"];

      if (documentPath.StartsWith("/"))
      {
        documentPath = HttpContext.Current.Server.MapPath(documentPath);
      }

      this.CurrentDirectory = IsPath;

      documentPath += IsPath.Replace('/', '\\');
      if (Directory.Exists(documentPath))
      {
        this.UCUpload.Visible = this.CurrentDirectory != String.Empty &&
                                ConfigurationManager.AppSettings["UploadEnabled"].Contains(this.CurrentDirectory + ";");

        if (this.UCUpload.Visible)
        {
          this.UCUpload.UploadFolder = documentPath;
        }

        if (IsPath != String.Empty)
        {
          string sParentDir = IsPath.IndexOf("/") > 0 ? IsPath.Substring(0, IsPath.LastIndexOf("/")) : String.Empty;

          var item = new DirectoryItem(this.LevelUp, this.DirUpClickText, sParentDir)
            {
              UpDir = true, 
              CssClass = "ContentText", 
              PostBackEventReference =
                "javascript: " + this.Page.ClientScript.GetPostBackEventReference(this, sParentDir)
            };
          this.pnlDirectory.Controls.Add(item);
          this.pnlDirectory.Controls.Add(new LiteralControl("<br />"));
        }

        // Get items in directory.
        foreach (DirectoryInfo di in new DirectoryInfo(documentPath).GetDirectories())
        {
          var item = new DirectoryItem(
            di.Name, this.DirClickText, IsPath != String.Empty ? String.Format("{0}/{1}", IsPath, di.Name) : di.Name);

          item.PostBackEventReference = "javascript: " +
                                        this.Page.ClientScript.GetPostBackEventReference(this, item.Href);
          this.pnlDirectory.Controls.Add(item);
        }

        // Create HtmlTables and add them to panel.
        HtmlTable tabMain = null;

        if (this.ColCount > 0)
        {
          tabMain = new HtmlTable();
        }

        int iItemIdx = 0;

        FileInfo[] files = new DirectoryInfo(documentPath).GetFiles();

        Array.Sort(files, delegate(FileInfo a, FileInfo b) { return b.Name.CompareTo(a.Name); }); 



        foreach (FileInfo fi in files)
        {
          HtmlTable tab = HtmlFunc.CreateTable(
            String.Format("{0}?Path={1}/{2}", this.DocumentViewer, IsPath, fi.Name), 
            String.Format("/images/doc/{0}.gif", fi.Extension.Substring(1)), 
            String.Format(this.FileClickText, Decimal.Round(Convert.ToDecimal(fi.Length) / 1024, 1)), 
            fi.Name, 
            this.CssClass);

          // TableLayout
          if (tabMain != null)
          {
            if (iItemIdx % this.ColCount == 0)
            {
              tabMain.Rows.Add(new HtmlTableRow());
            }

            var htc = new HtmlTableCell();
            htc.VAlign = "top";
            htc.Controls.Add(tab);
            tabMain.Rows[tabMain.Rows.Count - 1].Controls.Add(htc);
          }
          else
          {
            // FlowLayout
            this.pnlDirectory.Controls.Add(tab);
          }

          iItemIdx++;
        }

        if (tabMain != null)
        {
          this.pnlDirectory.Controls.Add(tabMain);
        }
      }
    }

    /// <summary>
    /// Udf�res efter dokument er uploaded.
    /// </summary>
    /// <param name="sender">
    /// </param>
    /// <param name="e">
    /// </param>
    private void UCUpload_OnAfterUpload(object sender, EventArgs e)
    {
      this.GetDirectory(this.CurrentDirectory);
    }

    #endregion
  }
}