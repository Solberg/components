<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Upload.ascx.cs" Inherits="SSL.UserControls.Directory.Upload" %>
<%@ Register Assembly="SSL.Web" Namespace="SSL.Web.UI.WebControls" TagPrefix="SSL" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<script language="javascript" type="text/javascript">

function CheckForm()
{
  alert("checkform");
  var RegularExpression = new RegExpr("(.*?)\.(pdf|jpeg|png|gif)$");
  
  if (document.getElementById("<%=FileUpload1.ClientID%>").value.search(RegularExpression) == -1)
  {
    alert("Ulovlig filtype");
    return false;
  }
  else
  {
    return true;
  }
  
}
</script>

<asp:Button CssClass="Button" ID="btnUpload" runat="server" Text="Upload" causesvalidation="false" ToolTip="Send dokument til mappe" /> 
<asp:Panel runat="server" ID="pnlUpload" CssClass="modalPopup" style="position:absolute;display:none;">
<table ID="Upload" style="width:400px">
  <tr class="HeaderLine">
    <td colspan="2" class="HeaderText">Upload dokument</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td class="FieldLabel">Dokument:</td>
    <td class="FieldCell">
      <asp:FileUpload ID="FileUpload1" runat="server" CssClass="InputField" Width="300px"  />
      <SSL:RequiredFieldValidator ID="rfvFileUpload" runat="server" EnableClientScript="true" ValidationGroup="Upload" ControlToValidate="FileUpload1" />
      <SSL:RegularExpressionValidator ID="revFileUpload" runat="server" EnableClientScript="false" ValidationExpression="(.*?)\.(pdf)$" ValidationGroup="Upload" ControlToValidate="FileUpload1" />
    </td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
      <asp:Button CssClass="Button" ID="btnPost" runat="server" Text="Upload" ToolTip="Upload dokument" OnClick="btnPost_Click" ValidationGroup="Upload" />
      <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Afbryd" CausesValidation="False" ToolTip="Afbryd og returner til dokumentmappe" />
    </td>
  </tr>
</table>
</asp:Panel>

<ajax:ModalPopupExtender ID="ModalPopupExtender1" 
           runat="server" 
           PopupControlID="pnlUpload" 
           BackgroundCssClass="modalBackground"
           DropShadow="true"
           RepositionMode="RepositionOnWindowResizeAndScroll"
           TargetControlID="btnUpload"
           CancelControlId="btnCancel">
            
</ajax:ModalPopupExtender>
