using System;
using System.Text.RegularExpressions;

namespace SSL.UserControls.Directory
{
  public partial class Upload : System.Web.UI.UserControl
  {
    public event EventHandler OnAfterUpload;

    #region Properties
    public string UploadFolder
    {
      get { return ViewState["UploadFolder"] != null ? Convert.ToString(ViewState["UploadFolder"]) : String.Empty ; }
      set { ViewState["UploadFolder"] = value; }
    }

    protected string FileUploadField
    {
      get { return FileUpload1.ClientID; }
    }

    protected bool IsValid
    {
      get
      {
        if (!FileUpload1.HasFile)
        {
          cvFileUpload.IsValid = false;
          cvFileUpload.ErrorMessage = "Dokument skal v�lges";
        }
        else if (new System.IO.FileInfo(FileUpload1.FileName).Extension != ".pdf")
        {
          cvFileUpload.IsValid = false;
          cvFileUpload.ErrorMessage = "Der kan kun uploades pdf filer";
        }

        return Page.IsValid;
      }
    }


    #endregion

    #region Event-handlers

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      btnUpload.OnClientClick = String.Format("ajaxSetFocus('{0}')", FileUpload1.ClientID);
    }


    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      rfvFileUpload.Visible = revFileUpload.IsValid;

    }

      protected void btnPost_Click(object sender, EventArgs e)
      {
        if (Page.IsValid && FileUpload1.HasFile)
        {
          string filepath = FileUpload1.PostedFile.FileName;
          string pat = @"\\(?:.+)\\(.+)\.(.+)";
          Regex r = new Regex(pat);
          //run
          Match m = r.Match(filepath);
          string file_ext = m.Groups[2].Captures[0].ToString();
          string filename = m.Groups[1].Captures[0].ToString();
          string file = filename + "." + file_ext;

          //save the file to the server 
          FileUpload1.PostedFile.SaveAs(UploadFolder + "\\" + file);
        }
        else
        {
          ModalPopupExtender1.Show();
        }

        if (OnAfterUpload != null)
        {
          OnAfterUpload(this, EventArgs.Empty);
        }

      }
    #endregion

  }
}