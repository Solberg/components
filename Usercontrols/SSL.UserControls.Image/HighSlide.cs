using System;
namespace SSL.UserControls.Image
{
  using System.Web.UI.HtmlControls;
  public class HighSlide
  {

    public static HtmlGenericControl GetHSAlt(int imageIndex,
      string alt)
    {
      var div = new HtmlGenericControl("div") {InnerHtml = alt, ID = String.Format("caption{0}", imageIndex)};
      div.Attributes["class"] = "highslide-caption";
      return div;
    }

    public static HtmlAnchor GetHSAnchor(int imageIndex,
      string fullImagePath,
      string thumbnailPath,
      int thumbnailWidth,
      int thumbnailHeight,
      string className,
      string captionPrefix)
    {
      var anchor = new HtmlAnchor {ID = String.Format("thumb{0}", imageIndex)};
      anchor.Attributes["class"] = "highslide";
      anchor.Attributes["onclick"] = "return hs.expand(this, { captionId: '" + captionPrefix + "caption" + Convert.ToString(imageIndex) + "' } )";
      anchor.HRef = fullImagePath;

      var img = new HtmlImage
                  {
                    Src = thumbnailPath,
                    Alt = "Click to enlarge",
                    Width = thumbnailWidth,
                    Height = thumbnailHeight
                  };

      img.Attributes["class"] = className;
      anchor.Controls.Add(img);
      return anchor;
    }
  }
}
