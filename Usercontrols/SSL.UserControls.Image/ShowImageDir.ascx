<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowImageDir.ascx.cs" Inherits="SSL.UserControls.Image.ShowImageDir" %>
<style>
 .Thumbnail{border:2px solid green}
</style>
<script type="text/javascript" src="/highslide/highslide.js"></script>
<script type="text/javascript">
	hs.registerOverlay(
    	{
    		thumbnailId: null,
    		overlayId: 'controlbar',
    		position: 'top right',
    		hideOnMouseOut: false
		}
	);
	
    hs.graphicsDir = '/highslide/graphics/';
    hs.outlineType = 'rounded-white';
    hs.targetX = 'HSPlacer';
    hs.targetY = 'HSPlacer';
    hs.showCredits = false;
    hs.allowMultipleInstances = false;
</script>
<div style="vertical-align:top;Text-align:center;height:480px;width:100%;overflow-y:auto">
<asp:Panel ID="PnlImages" runat="server"></asp:Panel>
<div id="controlbar" class="highslide-overlay controlbar">
	<a href="#" class="previous" onclick="return hs.previous(this)" title="Previous (left arrow key)"></a>
	<a href="#" class="next" onclick="return hs.next(this)" title="Next (right arrow key)"></a>
    <a href="#" class="highslide-move" onclick="return false" title="Click and drag to move"></a>
    <a href="#" class="close" onclick="return hs.close(this)" title="Close"></a>
</div>
</div>


