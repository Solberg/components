namespace SSL.UserControls.Image
{
  using System;
  using System.Collections.Specialized;
  using System.IO;
  using System.Web.UI.HtmlControls;
  using System.Web.UI.WebControls;

  using IO.Domain;

    public partial class ShowImageDir : ShowImagesControl
	{
		#region Variables

		private string _path;

		#endregion

    protected Panel PnlImages;


		#region Methods
		

  	private void GetImages()
		{
			var id = new IO.ImageDirectory(_path);

			AlbumInfo ai = id.GetAlbumInfo(Server.MapPath(Request.QueryString["ImagePath"]), "dk");

			Page.Title = ai.Title;

			StringCollection sc = id.ListImages(Server.MapPath(Request.QueryString["ImagePath"]));

			for (int idx = 0; idx < sc.Count; idx++ )
			{
				sc[idx] = Request.QueryString["ImagePath"] + "/" + sc[idx];
			}

  		bool bGetInfo = id.ShowImageInfo(Server.MapPath(Request.QueryString["ImagePath"]));

  		HtmlTable tab = GetImageTable(sc, bGetInfo);

			PnlImages.Controls.Add(tab);

		}

  	#endregion


		#region Event handlers
		override protected void OnInit(EventArgs e)
    {
      base.OnInit(e);

      if (!String.IsNullOrEmpty(Request.QueryString["ImagePath"]))
      {
        bool bOk = true;

        try
        {
          _path = Server.MapPath(Request.QueryString["ImagePath"]);
        }
        catch
        {
          bOk = false;
        }

        if (!Directory.Exists(_path))
        {
          bOk = false;
        }

        if (! bOk)
        {
          Response.Redirect("/PageNotFound.aspx");
        }
      }
      else
      {
        Response.Redirect("/PageNotFound.aspx");
      }
    }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (! Page.IsPostBack)
			{
				if (! String.IsNullOrEmpty(_path))
				{
					GetImages();
				}

			}
		}


    #endregion


  }
}