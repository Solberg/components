<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowImages.ascx.cs" Inherits="SSL.UserControls.Image.ShowImages" %>
<style>
 .Thumbnail{border:2px solid green}
 .Container{vertical-align:top;Text-align:center;overflow-y:auto}
</style>

<asp:panel runat="server" ID="PnlContainer" CssClass="Container" height="480px" Width="100%">
<asp:Panel ID="PnlImages" runat="server"></asp:Panel>
<div id="controlbar" class="highslide-overlay controlbar">
	<a href="#" class="previous" onclick="return hs.previous(this)" title="Previous (left arrow key)"></a>
	<a href="#" class="next" onclick="return hs.next(this)" title="Next (right arrow key)"></a>
    <a href="#" class="highslide-move" onclick="return false" title="Click and drag to move"></a>
    <a href="#" class="close" onclick="return hs.close(this)" title="Close"></a>
</div>
</asp:panel>


