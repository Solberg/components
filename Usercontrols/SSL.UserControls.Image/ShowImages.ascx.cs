namespace SSL.UserControls.Image
{
  using System;
  using System.Collections.Specialized;
  using System.Web.UI.WebControls;

  public partial class ShowImages : ShowImagesControl
	{
		#region Variables

    #endregion

		#region Properties

    public StringCollection ImageFiles { get; set; }

    public Unit Height
  	{
  		set { PnlContainer.Height = value; } get { return PnlContainer.Height; } 
  	}

		public Unit Width
		{
			set { PnlContainer.Width = value; }
			get { return PnlContainer.Width; }
		}
		#endregion

    protected Panel PnlImages;
  	protected Panel PnlContainer;
		#region Methods
		
  	private void GetImages()
		{
  		var tab = GetImageTable(ImageFiles, false);

			PnlImages.Controls.Add(tab);
		}

  	#endregion


		#region Event handlers


		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			GetImages();
		}


    #endregion


  }
}