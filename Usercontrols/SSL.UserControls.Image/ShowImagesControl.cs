﻿namespace SSL.UserControls.Image
{
  using System;
  using System.Collections.Specialized;
  using System.Web.UI;
  using System.Web.UI.HtmlControls;

	public class ShowImagesControl : UserControl
	{

    #region Variables
		private int _imagesOnRow = 6;

		private int _thumbnailHeight = 90;

		private int _thumbnailWidth = 120;

		#endregion

		#region Properties


		public int ThumbnailWidth
		{
			get { return _thumbnailWidth; }
			set { _thumbnailWidth = value; }
		}


		public int ThumbnailHeight
		{
			get { return _thumbnailHeight; }
			set { _thumbnailHeight = value; }
		}


		public int ImagesOnRow
		{
			get { return _imagesOnRow; }
			set { _imagesOnRow = value; }
		}



		#endregion

    #region Delegates

    public delegate string OnAddImage(string filePath);

    #endregion

    #region Event handlers
    public OnAddImage AddImage;
    #endregion

    #region Methods

    /// <summary>
		/// Build a html table containing the pictures from IscFiles.
		/// </summary>
		/// <param name="files"></param>
		/// <param name="getInfo"></param>
		/// <returns></returns>
		protected HtmlTable GetImageTable(StringCollection files,
			bool getInfo)
		{
			int i = 0;

			var tab = new HtmlTable();
			var row = new HtmlTableRow();

			foreach (string sImagePath in files)
			{
				string sIndexPath = sImagePath.Substring(0, sImagePath.LastIndexOf("/") + 1) + "Index/" +
														sImagePath.Substring(sImagePath.LastIndexOf("/") + 1);

				if (i % ImagesOnRow == 0)
				{
					tab.Rows.Add(row);
					row = new HtmlTableRow();
				}

				var cell = new HtmlTableCell();

				string alt;


				if (AddImage != null)
				{
					alt = AddImage(sImagePath);
				}
				else
				{
				  alt = getInfo
				          ? new IO.ImageInfo(
				              Server.MapPath(sImagePath)).Comments
				          : String.Empty;
				}

				HtmlAnchor anchor = HighSlide.GetHSAnchor(i,
																									sImagePath,
																									sIndexPath,
																									ThumbnailWidth,
																									ThumbnailHeight,
																									"HSBorder",
																									ClientID + "_");

				cell.Controls.Add(anchor);

				if (alt != String.Empty)
				{
					HtmlGenericControl div = HighSlide.GetHSAlt(i, alt);
					cell.Controls.Add(div);
				}

				row.Cells.Add(cell);
				i++;
			}

			tab.Rows.Add(row);
			return tab;
		}

		#endregion

	}
}
