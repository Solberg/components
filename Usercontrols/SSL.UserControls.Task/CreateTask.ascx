<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateTask.ascx.cs" Inherits="SSL.UserControls.Task.CreateTask" %>
<%@ Register Assembly="SSL.Web" Namespace="SSL.Web.UI.WebControls" TagPrefix="SSL" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Button CssClass="Button" ID="btnCreate" runat="server" Text="Opret" causesvalidation="false" ToolTip="Opret ny opgave" /> 
<asp:Panel runat="server" ID="pnlCreate" CssClass="modalPopup" style="position:absolute;display:none;">
<table id="CreateTask" style="width:400px">
<tr class="HeaderLine">
  <td colspan="2" class="HeaderText">Opret opgave</td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td class="FieldLabel">Tekst:</td>
<td class="FieldCell">
  <SSL:RequiredFieldValidator ID="rfvText" ControlToValidate="tbText" runat="server" ErrorMessage="Felt skal udfyldes" ValidationGroup="TaskCreate" />
  <asp:TextBox CssClass="InputField" ID="tbText" runat="server" Width="300px" MaxLength="100"></asp:TextBox>
</td>
</tr>
<tr>
<td class="FieldLabel">Dato:</td>
<td class="FieldCell">
  <SSL:RequiredFieldValidator ID="rfvDate" ControlToValidate="tbDate" runat="server" ErrorMessage="Felt skal udfyldes" ValidationGroup="TaskCreate" />
  <asp:TextBox CssClass="InputField DateField" ID="tbDate" runat="server"></asp:TextBox>
  <ajax:MaskedEditExtender ID="meeDate" runat="server" TargetControlID="tbDate" MaskType="Date" Mask="99/99/9999"></ajax:MaskedEditExtender>
  <ajax:CalendarExtender ID="ceDate" runat="server" TargetControlID="tbDate" Format="dd-MM-yyyy" EnabledOnClient="true" ></ajax:CalendarExtender>
</td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
  <asp:Button CssClass="Button" ID="btnPost" runat="server" Text="Opret" ToolTip="Opret opgave" OnClick="btnPost_Click" ValidationGroup="TaskCreate" />
  <asp:Button ID="btnCancel" runat="server" CssClass="Button" Text="Afbryd" CausesValidation="False" ToolTip="Afbryd og returner til opgaveoversigt" />
</td>
</tr>
</table>
</asp:Panel>

<ajax:ModalPopupExtender ID="ModalPopupExtender1" 
           runat="server" 
           PopupControlID="pnlCreate" 
           BackgroundCssClass="modalBackground"
           DropShadow="true"
           RepositionMode="RepositionOnWindowResizeAndScroll"
           TargetControlID="btnCreate"
           CancelControlId="btnCancel">
            
</ajax:ModalPopupExtender>
