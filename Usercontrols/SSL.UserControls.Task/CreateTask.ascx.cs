using System;
using System.Web.UI.WebControls;
using SSL.Data.SqlServer.Dbo;

namespace SSL.UserControls.Task
{
  public partial class CreateTask : Web.UI.UserControl
  {
    public event EventHandler OnAfterPost;

    #region Properties
    public string ConnectionString { set; get; }

    /// <summary>
    /// Id for Member logged in. 
    /// </summary>
    public int MemberId { set; get;}
    #endregion

    protected Button btnCancel;

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      btnCreate.OnClientClick = String.Format("ajaxSetFocus('{0}')", tbText.ClientID);

    }

    /// <summary>
    /// Post data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPost_Click(object sender, EventArgs e)
    {
      if (! IsRefresh
          && Page.IsValid)
      {
        var dbo = new dboTask(ConnectionString);

        dbo.Create(tbText.Text, Convert.ToDateTime(tbDate.Text), MemberId);

        if (OnAfterPost != null)
        {
          OnAfterPost(sender, e);
        }

        tbText.Text = String.Empty;
        tbDate.Text = String.Empty;
      }
 
    }
  }
}