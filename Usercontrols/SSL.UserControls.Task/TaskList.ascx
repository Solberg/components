<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaskList.ascx.cs" Inherits="SSL.UserControls.Task.TaskList" %>
<%@ Register Src="CreateTask.ascx" TagName="CreateTask" TagPrefix="uc1" %>
<table id="TaskList" class="ContentArea" cellspacing="0">
  <tr>
    <td class="DataDivFrameContainer">
      <div class="DataDiv Frame ScrollTableContainer">
        <div class="innerframe">
          <asp:Repeater ID="rptTask" runat="server" OnItemDataBound="rptJobs_ItemDataBound" OnItemCommand="rptJobs_ItemCommand"  >
          <HeaderTemplate>
          <table cellspacing="0">
            <thead>
            <tr class="HeaderLine">
              <td>&nbsp;</td>
              <td class="FieldLabel">Opgave</td>
              <td class="FieldLabel">Dato</td>
              <td class="FieldLabel">Medlem</td>
             </tr>
             </thead>         
             <tbody>
          </HeaderTemplate>
          <ItemTemplate>
            <tr>
              <td style="vertical-align:middle;text-align:left"><asp:ImageButton ID="ibTakeIt" Cssclass="ColumnButton" ImageUrl="/images/btn/edit.gif" runat="server" CausesValidation="false" CommandName="Link" /><asp:ImageButton ID="ibDone" Cssclass="ColumnButton" ImageUrl="/images/btn/check.gif" runat="server" CausesValidation="false" CommandName="DONE" ToolTip="Opgaven er udf�rt" />&nbsp;</td>
              <td class="ColumnText"><asp:Label ID="lblText" Cssclass="ContentText" runat="server" />&nbsp;</td>
              <td class="ColumnDate"><asp:Label ID="lblDate" Cssclass="ContentText" runat="server" />&nbsp;</td>
              <td class="ColumnPerson"><asp:Label ID="lblMember" Cssclass="ContentText" runat="server" />&nbsp;</td>
            </tr>
          </ItemTemplate>
          <FooterTemplate>
          </tbody>
          </table>
          </FooterTemplate>
          </asp:Repeater>
        </div>
      </div>
    </td>
  </tr>
	<tr class="ButtonRow">  
	<td>
    <table>
      <tr>
        <td>
          <uc1:CreateTask ID="CreateTask1" runat="server" />
        </td>
      </tr>
    </table>
	</td>
	</tr>
</table>
