using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using SSL.Data.SqlServer;
using SSL.Data.SqlServer.Dbo;

namespace SSL.UserControls.Task
{
  public partial class TaskList : UserControl
  {
    #region Variables

    private string _ConnectionString;
    private int _MemberId;

    #endregion

    #region Properties
    /// <summary>
    /// Connectionstring to the database.
    /// </summary>
    public string ConnectionString
    {
      get { return _ConnectionString; }
      set
      {
        _ConnectionString = value;
        CreateTask1.ConnectionString = value;
      }
    }

    public int MemberId
    {
      get { return _MemberId; }
      set 
      { 
        _MemberId = value;
        CreateTask1.MemberId = value;
      }
    }

    public bool IsAdministrator { get; set; }

    #endregion

    #region Methods

    public override void DataBind()
    {
      base.DataBind();

      var dbo = new dboTask(ConnectionString);
      rptTask.DataSource = dbo.List();
      rptTask.DataBind();

    }


    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);

      if (!Page.IsPostBack)
        DataBind();
    }



    #endregion

    #region Event handlers

    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      CreateTask1.OnAfterPost += CreateTask_OnAfterPost;
    }

    protected void CreateTask_OnAfterPost(object sender, EventArgs e)
    {
      DataBind();
    }

    #endregion



    protected void rptJobs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.DataItem != null)
      {
        var aFields = ((DataRowView)e.Item.DataItem).Row.ItemArray;

        var ibTakeIt = ((ImageButton)e.Item.FindControl("ibTakeIt"));
        var ibDone = ((ImageButton)e.Item.FindControl("ibDone"));

        if (DbConvert.ToInt32(aFields[3].ToString()) == 0)
        {
          ibTakeIt.CommandArgument = Convert.ToString(aFields[0]);
          ibTakeIt.ImageUrl = "/images/btn/connect.gif";
          ibTakeIt.CommandName = "CONNECT";
          ibTakeIt.AlternateText = "Tilknyt mig til denne opgave";

          ibDone.Visible = false;
        }
        else
        {
          /// If Assigned to task, creator or administrator
          if (DbConvert.ToInt32(aFields[3]) == MemberId
            || DbConvert.ToInt32(aFields[6]) == MemberId
            || IsAdministrator)
          {
            
            if ( DbConvert.ToInt32(aFields[3]) == MemberId
              || DbConvert.ToInt32(aFields[6]) == MemberId)
            {
              ibTakeIt.ImageUrl = "/images/btn/disconnect.gif";
              ibTakeIt.CommandName = "DISCONNECT";
              ibTakeIt.CommandArgument = Convert.ToString(aFields[0]);
              ibTakeIt.AlternateText = "Fjern mig fra denne opgave";
            }
            else
            {
              ibTakeIt.Visible = false;
            }

            ibDone.CommandArgument = Convert.ToString(aFields[0]);
            ibDone.Visible = true;
          }
          else
          {
            ibTakeIt.Visible = false;
            ibDone.Visible = false;
          }
        }

        ((Label)e.Item.FindControl("lblText")).Text = Convert.ToString(aFields[1]);
        ((Label)e.Item.FindControl("lblDate")).Text = String.Format("{0:d}", aFields[2]);
        ((Label)e.Item.FindControl("lblMember")).Text = Convert.IsDBNull(aFields[4]) ? String.Empty : Convert.ToString(aFields[4]);

        if (e.Item.ItemType == ListItemType.AlternatingItem)
          ((LiteralControl)e.Item.Controls[0]).Text = ((LiteralControl)e.Item.Controls[0]).Text.Replace("<tr>", "<tr class=\"AlternateRow\" >");

      }
    }

    /// <summary>
    /// Tilknyt nuv�rende medlem til job.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void rptJobs_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      switch(e.CommandName)
      {
        case "CONNECT"    : new dboTask(ConnectionString).UpdateMember(Convert.ToInt32(e.CommandArgument), MemberId);
          break;
        case "DISCONNECT" : new dboTask(ConnectionString).UpdateMember(Convert.ToInt32(e.CommandArgument), MemberId);
          break;
        case "DONE": new dboTask(ConnectionString).UpdateActive(Convert.ToInt32(e.CommandArgument), 0);
          break;
      }
      DataBind();
    }

  }

}